﻿using tedhiyye_portal_shared.Domains.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.Archive
{
    public class QuestionAnswerVM
    {
        public bool IsQuarter { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public BankQuestionTitle BankQuestionTitle { get; set; }
    }
}