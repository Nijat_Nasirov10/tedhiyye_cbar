﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class ReportType
    {
        public int ID { get; set; }
        public string REPORTTYPE { get; set; }
        public string REPORT_FILE_TYPE_NAME { get; set; }
        public string PERIOD { get; set; }
        public int SENDTIME { get; set; }
        public int NOTIFY { get; set; }
        public int TIMEOVER { get; set; }
        public int ALLOW_CHECK_SIMILAR { get; set; }
        public int? SENDTIME_NOBANKS { get; set; }
        public int? IS_ACTIVE { get; set; }
        public int? EMPLOYEE_ID { get; set; }
        public int? REPORT_FILE_TYPE_ID { get; set; }
        public string PERIOD_NEW { get; set; }

    }

}
