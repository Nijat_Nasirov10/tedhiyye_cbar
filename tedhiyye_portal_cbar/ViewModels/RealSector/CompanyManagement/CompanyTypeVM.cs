﻿using tedhiyye_portal_shared.Domains;
using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.CompanyManagement
{
    public class CompanyTypeVM
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }

    public class CreateEditCompanyTypeVM
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [Display(Name = "Fəaliyyət növü")]
        public string Type { get; set; }
    }
}