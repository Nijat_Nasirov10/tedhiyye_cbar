﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class ResponsiblePerson
    {
        public string Name { get; set; } = " ";
        public string Surname { get; set; } = " ";
        public string FatherName { get; set; } = " ";
        public string Department { get; set; } = " ";
        public string Position { get; set; } = " ";
        public string Email { get; set; } = " ";
        public string Phone { get; set; } = " ";
        public string OfficePhone { get; set; } = " ";
        public string MobilePhone { get; set; } = " ";
        public string EmployeeId { get; set; } = " ";
    }

    public class ResponsiblesInfo
    {
        public int UserId { get; set; }
        public int BankId { get; set; } = 0;
        public string BankName { get; set; } = "";
        public int ReportFileTypeId { get; set; }
        public string ReportFileTypeName { get; set; }
        public List<int> moderatorReportFileTypeIDs { get; set; }
        public bool IsAdmin { get; set; }
        public string[] SubstituteNames { get; set; }
        public string[] SubstituteUserIds { get; set; }
        public string[] SubstituteEmails { get; set; }
        public string SubstituteDepartment { get; set; }
        public string[] SubstitutePosition { get; set; }
        public string[] SubstitutePhone { get; set; }
        public string[] SubstituteOfficePhone { get; set; }
        public string[] SubstituteMobilePhone { get; set; }


        public ResponsiblePerson Executor { get; set; }
        public ResponsiblePerson Curator { get; set; }

        public ResponsiblesInfo(int arraySize)
        {
            SubstituteNames = new string[arraySize];
            SubstituteUserIds = new string[arraySize];
            SubstituteEmails = new string[arraySize];
            SubstitutePosition = new string[arraySize];
            SubstitutePhone = new string[arraySize];
            SubstituteOfficePhone = new string[arraySize];
            SubstituteMobilePhone = new string[arraySize];
        }
        public ResponsiblesInfo()
        {
            SubstituteNames = new string[1] { " " };
            SubstituteUserIds = new string[1] { " " };
            SubstituteEmails = new string[1] { " " };
            SubstitutePosition = new string[1] { " " };
            SubstitutePhone = new string[1] { " " };
            SubstituteOfficePhone = new string[1] { " " };
            SubstituteMobilePhone = new string[1] { " " };
        }
    }
}
