﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class NotificationMessage
    {
        public int ID { get; set; }
        public DateTime SENDDATE { get; set; }
        public int BANKID { get; set; }
        public string BankName { get; set; }
        public string BANKNAME { get; set; }
        public int? DEPARTMENTID { get; set; }
        public string DEPARTMENT { get; set; }
        public string SUBJECT { get; set; }
        public string MESSAGE { get; set; }
        public int? USERID { get; set; }
        public bool HASREAD { get; set; }
        public DateTime READDATE { get; set; }
        public bool HASDELETE { get; set; }
        public DateTime DELETEDATE { get; set; }
    }
}
