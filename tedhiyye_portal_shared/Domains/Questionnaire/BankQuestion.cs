﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankQuestion
    {
        public BankQuestion()
        {
            BankQuestionDetails = new List<BankQuestionDetail>();
        }
        public int ID { get; set; }
        public string QUESTION_TITLE { get; set; }
        public string QUESTION_GROUP_ID { get; set; }

        public List<BankQuestionDetail> BankQuestionDetails { get; set; }

    }
}
