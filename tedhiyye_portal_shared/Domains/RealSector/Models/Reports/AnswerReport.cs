﻿using tedhiyye_portal_shared.Domains.RealSector.Models.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.RealSector.Models.Reports
{
    public class AnswerReport
    {
        public IEnumerable<AnswerReportItem> Data { get; set; }
        public int Count { get; set; }
    }
}
