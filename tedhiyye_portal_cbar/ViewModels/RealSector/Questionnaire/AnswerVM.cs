﻿using tedhiyye_portal_shared.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace insurance_system_cbar.ViewModels.RealSector.Questionnaire
{
    public class AnswerVM
    {
        public int Id { get; set; }
        public string Answer { get; set; }
        public int? Point { get; set; }
        public int? OrderId { get; set; }
    }

    public class CreateEditAnswerVM
    {
        public int Id { get; set; }

        [Required]
        public int QuestionId { get; set; }

        public string QuestionText { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [DisplayName("Cavab")]
        public string Answer { get; set; }

        [Display(Name = "Xal")]
        public int? Point { get; set; }

        [DisplayName("Sıralanma")]
        public int? OrderId { get; set; }

    }
}