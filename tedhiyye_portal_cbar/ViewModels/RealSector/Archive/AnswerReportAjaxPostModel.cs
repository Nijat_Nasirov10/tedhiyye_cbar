﻿
namespace tedhiyye_portal_cbar.ViewModels.RealSector.Archive
{
    public class AnswerReportAjaxPostModel
    {
        public int draw { get; set; } = 1;
        public int start { get; set; } = 1;
        public int length { get; set; } = 10;
        public MonthOfYearSearch MonthOfYearSearch { get; set; }

    }

    public struct MonthOfYearSearch
    {
        public int Year { get; set; }
        public int Month { get; set; }
    }
}