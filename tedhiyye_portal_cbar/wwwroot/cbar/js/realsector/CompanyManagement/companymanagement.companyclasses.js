﻿function OnLoadCompanyClasses() {
    var url_companyclass_table = $("#url-companyClass").data('table');
    var url_companyclass_edit = $("#url-companyClass").data('edit');
    var url_companyclass_delete = $("#url-companyClass").data('delete');

    var $companyclass_table = $("#companyclass_datatable");
    var companyTypeId = $companyclass_table.data('companytypeid');
    var companyclasstable = $companyclass_table.DataTable({
        "ajax": {
            "url": url_companyclass_table + '?companyTypeId=' + companyTypeId,
            "type": "get",
            "datatype": "json"
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [{ "orderable": false, "targets": [2, 3] }],

        "columns": [
            {
                "data": "Name",
                "width": "70%"
            },

            {
                "data": "Parent",
                "width": "70%"
            },

            {
                "data": "Id", "render": function (data) {
                    var edit = '<a class="edit btn btn-primary btn-block" href="' + url_companyclass_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>';
                    return edit;
                },
                "width": "30px"
            },

            {
                "data": "Id", "render": function (data) {
                    return '<a class="delete btn btn-danger btn-block" data-id="' + data + '" href="' + url_companyclass_delete + '"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                "width": "30px"
            }
        ]
    })

    $('.companyclass-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpenCompanyClassPopup($(this).attr('href'));
    })

    $('.companyclass-table-container').on('click', 'a.delete', function (e) {
        e.preventDefault();
        if (confirm("Məlumat ləğv edilsin?")) {
            e.preventDefault();

            $.post($(this).attr('href'), { companyClassId: $(this).attr('data-id') }, function (data) {
                companyclasstable.ajax.reload(null, false);
                
            });
        }
        else {
            return false;
        }
    })

    function ClearChildContent() {
        $("#companyclass-holder").html('');
    }

    var companyTypeId = '#companyclass-content';
    function OpenCompanyClassPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $(companyTypeId, $pageContent).removeData('validator');
            $(companyTypeId, $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });

        $(".dialog").dialog("close");
        $dialog = $('<div class="dialog-companyclass dialog"></div>')
            .html($pageContent)
            .dialog({
                draggable: dialog_options.draggable,
                autoOpen: dialog_options.autoOpen,
                resizable: dialog_options.resizable,
                modal: dialog_options.modal,
                height: dialog_options.height,
                width: dialog_options.width,
                position: dialog_options.position,
                title: 'Bölmələr',
                buttons: {
                    "Yadda saxla": function () {
                        if (!$(companyTypeId).valid()) {
                            return false;
                        }
                        var url = $(companyTypeId)[0].action;
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $(companyTypeId).serialize(),
                            success: function (data) {
                                if (data !== null) {
                                    $dialog.dialog('destroy').remove();
                                    companyclasstable.ajax.reload(null, false);
                                }
                            }
                        })
                    },
                    "Cancel": function () {
                        $dialog.dialog('destroy').remove();
                    }
                }
            })

        $dialog.dialog('open');
    }
}