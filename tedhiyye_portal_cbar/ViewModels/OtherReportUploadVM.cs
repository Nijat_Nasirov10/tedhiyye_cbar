﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_cbar.ViewModels
{
    public class OtherReportUploadVM
    {
        [Required(ErrorMessage = "Hesabat tipi seçilməyib")]
        public int? report_file_type_id { get; set; }

        [Required(ErrorMessage = "Hesabat dövrü başlama seçilməyib")]
        public string periodstart { get; set; }

        [Required(ErrorMessage = "Hesabat dövrü bitmə seçilməyib")]
        public string periodend { get; set; }
    }
}