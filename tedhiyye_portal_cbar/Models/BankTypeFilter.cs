﻿namespace tedhiyye_portal_cbar.Models
{
    public class BankTypeFilter
    {
        public int Id { get; set; }
        public bool IsClosed { get; set; }
    }
}