﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
	[Table("DOWNLOADS")]
	public class Download
	{
		public Download()
		{
			CompanyTypes = new HashSet<CompanyType>();
		}

		[Key]
		[Column("ID")]
		public int Id { get; set; }

		[Column("HEAD")]
		public string Head { get; set; }

		[Column("LINK")]
		public string Link { get; set; }

		[Column("ROW_NO")]
		public int? RowNo { get; set; }

		[Column("GROUP_ID")]
		public int GroupId { get; set; }

		[Column("DESCRIPTION")]
		public string Description { get; set; }

		[Column("DEPARTMENT_ID")]
		public int? DepartmentId { get; set; }

		[Column("SECTOR_ID")]
		public int? SectorId { get; set; }

		[Column("PROJECT_NAME")]
		public string ProjectName { get; set; }
		public ICollection<CompanyType> CompanyTypes { get; set; }

	}
}