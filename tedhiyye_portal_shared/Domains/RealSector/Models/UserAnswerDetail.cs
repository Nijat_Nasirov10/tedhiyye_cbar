﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("USERANSWERDETAIL")]
    public class UserAnswerDetail
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("USERANSWERID")]
        [ForeignKey("UserAnswer")]
        public int UserAnswerId { get; set; }

        [Column("QUESTIONID")]
        [ForeignKey("Question")]
        public int QuestionId { get; set; }

        [Column("ANSWERID")]
        public int? QuestionAnswerId { get; set; }

        [Column("PRIVATEANSWER")]
        public string PrivateAnswer { get; set; }

        public virtual UserAnswer UserAnswer { get; set; }
        public virtual Question Question { get; set; }
        public virtual QuestionAnswer QuestionAnswer { get; set; }
    }
}