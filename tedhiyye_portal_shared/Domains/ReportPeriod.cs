﻿using tedhiyye_portal_shared;
using System;

namespace tedhiyye_portal_shared.Domains
{
	public class ReportPeriod
	{
		public PeriodType ReportPeriodType
		{
			get
			{
				PeriodType pt = PeriodType.Unknown;
				return Enum.TryParse(Code, true, out pt) ? pt : PeriodType.Unknown;
			}
		}

		public string Code { get; private set; }
		public string Name { get; private set; }
		public ReportPeriod()
		{

		}
		public ReportPeriod(string code, string name)
		{
			Code = code;
			Name = name;
		}
	}

}
