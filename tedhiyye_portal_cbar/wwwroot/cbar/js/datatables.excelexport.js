﻿        $(function () {
            var table = $('.esahs-table table').DataTable(
                {
                    "paging": false,
                    "ordering": false,
                    "info": false,
                    dom: 'lBfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            text: 'Excel export',
                            orientation: 'landscape',
                        }
                        /*{
                            extend: 'print',
                            text: 'Çap',
                            orientation: 'landscape',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                            }
                        },
                        {
                            extend: 'collection',
                            text: 'Eksport',
                            buttons: [
                                {
                                    extend: 'excelHtml5',
                                },
                                {
                                    extend: 'csv',
                                },
                                {
                                    extend: 'pdf',
                                    orientation: 'landscape',
                                },
                            ]
                        }*/
                    ],
                    language: {
                        "sEmptyTable": "Cədvəldə heç bir məlumat yoxdur",
                        "sInfo": " _TOTAL_ Nəticədən _START_ - _END_ Arası Nəticələr",
                        "sInfoEmpty": "Nəticə Yoxdur",
                        "sInfoFiltered": "( _MAX_ Nəticə İçindən Tapılanlar)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "Səhifədə _MENU_ Nəticə Göstər",
                        "sLoadingRecords": "Yüklənir...",
                        "sProcessing": "Gözləyin...",
                        "sSearch": "Axtarış:",
                        "sZeroRecords": "Nəticə Tapılmadı.",
                        "oPaginate": {
                            "sFirst": "İlk",
                            "sLast": "Son",
                            "sNext": "»",
                            "sPrevious": "«"
                        },

                        "oAria": {
                            "sSortAscending": ": sütunu artma sırası üzərə aktiv etmək",
                            "sSortDescending": ": sütunu azalma sırası üzərə aktiv etmək"
                        }

                    }
                });
        });