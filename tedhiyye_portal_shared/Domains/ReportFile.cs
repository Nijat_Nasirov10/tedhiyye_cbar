﻿using tedhiyye_portal_shared;
using System;

namespace tedhiyye_portal_shared.Domains
{
    public class ReportFile
    {
        public int ID { get; set; }
        public string UPLOAD_FILE_NAME { get; set; }
        public string UPLOAD_FILE_CONTENT_TYPE { get; set; }
        public DateTime UPLOAD_TIME { get; set; }
        public DateTime? CONVERT_TIME { get; set; }
        public int UPLOAD_USER_ID { get; set; }
        public string UPLOAD_USER_IP { get; set; }
        public int BANKID { get; set; }
        public string BANK { get; set; }
        //public string Upload_User_Name { get; set; }
        //public int Report_Id { get; set; }
        public ReportUploadFileStatus REPORT_FILE_STATUS_ID { get; set; }
        public string STATUS { get; set; }

        public DateTime? PERIODSTART { get; set; }
        public DateTime? PERIODEND { get; set; }
        public int? REPORT_FILE_TYPE_ID { get; set; }
        public string REPORT_FILE_TYPE_NAME { get; set; }
        //public string Version { get; set; }
        public string Period { get; set; }
        public string SendTime { get; set; }
        public byte[] Report_Template { get; set; }
        public bool NotFound { get; set; }
    }
}
