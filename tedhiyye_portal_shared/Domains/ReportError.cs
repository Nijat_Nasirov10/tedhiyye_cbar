﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class BaseReportError
    {
        public BaseReportError(string description)
        {
            DESCRIPTION = description;
            //Report = report;
        }

        public string DESCRIPTION { get;  set; }
    //    public string Report { get;  set; }
    }

    public class ReportError : BaseReportError
    {
        public ReportError(string report, string description) : base(description) { }
        public int RuleId { get; set; }
        public int CheckId { get; set; }
        public int ReportFileId { get; set; }
        public int ReportId { get; set; }
        public bool IsMultiRowResult { get; set; }
    }

}
