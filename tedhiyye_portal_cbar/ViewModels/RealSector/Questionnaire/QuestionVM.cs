﻿using tedhiyye_portal_shared.Domains;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.Questionnaire
{
	public class QuestionVM
	{
		public int Id { get; set; }
		public string QuestionText { get; set; }
		public bool IsPrivate { get; set; }
		public bool IsRequired { get; set; }
		public bool IsActive { get; set; }
		public int? OrderId { get; set; }
		public int? BiiCoefficient { get; set; }
	}

	public class CreateEditQuestionVM
	{
		public int Id { get; set; }

		[Required]
		public int? FormId { get; set; }

		[Required(ErrorMessage = ValidationMessage.Required)]
		[DisplayName("Sual")]
		public string QuestionText { get; set; }

		public string FormName { get; set; }

		[Required(ErrorMessage = ValidationMessage.Required)]
		[Display(Name = "Fərdi cavab")]
		public bool IsPrivate { get; set; }

		[Required(ErrorMessage = ValidationMessage.Required)]
		[Display(Name = "Mütləq cavabi olan")]
		public bool IsRequired { get; set; }

		public bool IsActive { get; set; }

		[DisplayName("Sıralanma")]
		public int? OrderId { get; set; }

		[DisplayName("Əmsal")]
		public CoefficientTypes? BiiCoefficient { get; set; }

		[DisplayName("Ədəd tipi")]
		public bool IsNumber { get; set; }

	}

	public enum CoefficientTypes
	{
		Müsbət = 1,
		Mənfi = -1
	}

}