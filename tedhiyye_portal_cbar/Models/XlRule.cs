﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar.Models
{
    public class XlRule
    {
        public int Id { get; set; }
        public string Formula { get; set; }
        public string CheckValue { get; set; }
        public string Status { get; set; }
        public string State { get; set; }
        public int IsMixed { get; set; }
        public int IsMultiRowResult { get; set; }
        public string RoundSize { get; set; }
        public string ErrorMessageSuffix { get; set; }
        public int HideDataValue { get; set; }
        public List<int> ReportTypeIds { get; set; }
        public List<int> PreviousMonthList { get; set; }
    }
}
