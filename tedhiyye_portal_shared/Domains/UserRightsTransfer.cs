﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class UserRightsTransfer
    {
        public int Id { get; set; }
        public BankUser FromUser { get; set; }
        public BankUser ToUser { get; set; }
    }
}
