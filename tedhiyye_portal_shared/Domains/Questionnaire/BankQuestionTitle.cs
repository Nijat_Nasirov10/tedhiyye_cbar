﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankQuestionTitle
    {
        public BankQuestionTitle()
        {
            BankQuestionGroups = new List<BankQuestionGroup>();
            BankTimePeriods = new List<BankTimePeriod>();
        }

        //public int ID { get; set; }
        //public int QUESTION_TYPE_ID { get; set; }
        //public string MAIN_QUESTION { get; set; }
        //public string QUESTION_COMMENT { get; set; }
        //public string QUESTION_DESCRIPTION { get; set; }

        public string question_type { get; set; }
        public string main_question { get; set; }
        public string question_comment { get; set; }
        public string question_description { get; set; }
        public int question_title_id { get; set; }
        public int bank_question_title_answer_id { get; set; }



        public bool IsEdit { get; set; }





        public List<BankQuestionGroup> BankQuestionGroups { get; set; }
        public List<BankTimePeriod> BankTimePeriods { get; set; }
    }
}
