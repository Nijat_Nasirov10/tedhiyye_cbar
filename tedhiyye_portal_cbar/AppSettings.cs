﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar
{
    public static class AppSettings
    {
        static IConfiguration configuration;
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        static AppSettings()
        {
            configuration = new ConfigurationBuilder().SetBasePath(AssemblyDirectory)
                .AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true)
                .Build();
        }

        public static string ConnectionString(string connectionName)
        {
            return configuration.GetConnectionString(connectionName);
        }

        public static T GetValue<T>(string configPath, char separator = '.')
        {
            try
            {
                var configHierarchy = configPath.Split(separator).Reverse();
                Stack<string> stack = new Stack<string>(configHierarchy);

                if (stack.Count < 1)
                {
                    throw new OverflowException();
                }

                if (stack.Count == 1)
                {
                    return configuration.GetValue<T>(stack.Pop());
                }

                var section = configuration.GetSection(stack.Pop());
                while (stack.Count > 1)
                {
                    section = section.GetSection(stack.Pop());
                }

                return section.GetValue<T>(stack.Pop());
            }
            catch
            {
                throw new ArgumentException(string.Format("Bad argument: '{0}'", nameof(configPath)));
            }
        }
    }
}
