﻿//using System.Data.Entity;

using tedhiyye_portal_shared.Domains.RealSector.Models;
using System.Data.SqlClient;
using tedhiyye_portal_shared.Domains.RealSector.Models.Reports;
using Microsoft.EntityFrameworkCore;

namespace tedhiyye_portal_shared.Domains.RealSector.DatabaseContext
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Form> Forms { get; set; }
        public DbSet<UserAnswer> UserAnswers { get; set; }
        public DbSet<UserAnswerDetail> UserAnswerDetails { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionAnswer> QuestionAnswers { get; set; }
        public DbSet<CompanyType> CompanyTypes { get; set; }
        public DbSet<CompanyClass> CompanyClasses { get; set; }
        public DbSet<Download> Downloads { get; set; }
        public DbSet<VwBiiForCompanyClass> VwBiisForCompanyClass { get; set; }
        public DbSet<VwBiiForCompanyType> VwBiisForCompanyType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //ConfigureBaseSettings(modelBuilder);
            ConfigureEntities(modelBuilder);
            ConfigureRelations(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        //private void ConfigureBaseSettings(ModelBuilder modelBuilder)
        //{
        //    var connBuilder = new SqlConnectionStringBuilder(Database.Connection.ConnectionString);
        //    modelBuilder.HasDefaultSchema(connBuilder.UserID.ToUpper());

        //    Database.SetInitializer<ApplicationContext>(null);
        //    Configuration.LazyLoadingEnabled = false;
        //}

        private void ConfigureEntities(ModelBuilder modelBuilder)
        {
            var userEntity = modelBuilder.Entity<User>();
            ////userEntity.ToTable("USERSCOMPANIES");
            ////userEntity.HasKey(src => src.Id);
            ////userEntity.Property(src => src.Id).HasColumnName("ID");
            ////userEntity.Property(src => src.Username).HasColumnName("USERNAME");
            ////userEntity.Property(src => src.Password).HasColumnName("PASSWORD");
            ////userEntity.Property(src => src.CompanyId).HasColumnName("COMPANYID");
            ////userEntity.Property(src => src.Email).HasColumnName("EMAIL");
            ////userEntity.Property(src => src.Phone).HasColumnName("PHONE");
            //userEntity.Property(src => src.BankId).HasColumnName("BANKID");

            var companyEntity = modelBuilder.Entity<Company>();
            ////companyEntity.ToTable("COMPANIES");
            //companyEntity.HasKey(src => src.Id);
            ////companyEntity.Property(src => src.Id).HasColumnName("ID");
            ////companyEntity.Property(src => src.Name).HasColumnName("COMPANY");
            ////companyEntity.Property(src => src.CompanyTypeId).HasColumnName("COMPANYTYPEID");
            ////companyEntity.Property(src => src.CompanyClassId).HasColumnName("COMPANYCLASSID");
            ////companyEntity.Property(src => src.Code).HasColumnName("CODE");
            ////companyEntity.Property(src => src.Address).HasColumnName("ADDRESS");
            ////companyEntity.Property(src => src.Phone).HasColumnName("PHONE");
            ////companyEntity.Property(src => src.Fax).HasColumnName("FAX");
            ////companyEntity.Property(src => src.Email).HasColumnName("EMAIL");
            ////companyEntity.Property(src => src.Description).HasColumnName("DESCRIPTION");
            ////companyEntity.Property(src => src.Boss).HasColumnName("BOSS");
            ////companyEntity.Property(src => src.WebSite).HasColumnName("WEBSITE");

            var companyClassEntity = modelBuilder.Entity<CompanyClass>();
            ////companyClassEntity.ToTable("COMPANYCLASS");
            companyClassEntity.HasKey(src => src.Id);
            ////companyClassEntity.Property(src => src.Id).HasColumnName("ID");
            //companyClassEntity.Property(src => src.CompanyTypeId).HasColumnName("COMPANYTYPEID");
            ////companyClassEntity.Property(src => src.Name).HasColumnName("COMPANYCLASS");
            //companyClassEntity.Property(src => src.ParentId).HasColumnName("PARENT_ID");

            var companyTypeEntity = modelBuilder.Entity<CompanyType>();
            ////companyTypeEntity.ToTable("COMPANYTYPE");
            ////companyTypeEntity.HasKey(src => src.Id);
            ////companyTypeEntity.Property(src => src.Id).HasColumnName("ID");
            ////companyTypeEntity.Property(src => src.Type).HasColumnName("COMPANYTYPE");

            var formEntity = modelBuilder.Entity<Form>();
            ////formEntity.ToTable("FORMS");
            ////formEntity.HasKey(src => src.Id);
            ////formEntity.Property(src => src.Id).HasColumnName("ID");
            ////formEntity.Property(src => src.Name).HasColumnName("FORM");
            ////formEntity.Property(src => src.CompanyTypeId).HasColumnName("COMPANYTYPEID");
            //formEntity.Property(src => src.OrderId).HasColumnName("ORDERID");
            formEntity.Ignore(src => src.IsComplete);

            var userAnswerEntity = modelBuilder.Entity<UserAnswer>();
            ////userAnswerEntity.ToTable("USERANSWER");
            ////userAnswerEntity.HasKey(src => src.Id);
            ////userAnswerEntity.Property(src => src.Id).HasColumnName("ID");
            ////userAnswerEntity.Property(src => src.UserId).HasColumnName("USERID");
            ////userAnswerEntity.Property(src => src.FormId).HasColumnName("FORMID");
            ////userAnswerEntity.Property(src => src.AnswerDate).HasColumnName("ANSWERDATE");

            var questionEntity = modelBuilder.Entity<Question>();
            ////questionEntity.ToTable("QUESTION");
            ////questionEntity.HasKey(src => src.Id);
            ////questionEntity.Property(src => src.Id).HasColumnName("ID");
            ////questionEntity.Property(src => src.QuestionText).HasColumnName("QUESTION");
            ////questionEntity.Property(src => src.FormId).HasColumnName("FORMID");
            ////questionEntity.Property(src => src.IsPrivate).HasColumnName("ISPRIVATE");
            ////questionEntity.Property(src => src.IsRequired).HasColumnName("ISREQUIRED");
            ////questionEntity.Property(src => src.OrderId).HasColumnName("ORDERID");
            ////questionEntity.Property(src => src.IsActive).HasColumnName("ISACTIVE");
            ////questionEntity.Property(src => src.BiiCoefficient).HasColumnName("BIICOEFFICIENT");
            ////         questionEntity.Property(src => src.IsNumber).HasColumnName("ISNUMBER");


            var questionAnswerEntity = modelBuilder.Entity<QuestionAnswer>();
            ////questionAnswerEntity.ToTable("ANSWER");
            ////questionAnswerEntity.HasKey(src => src.Id);
            ////questionAnswerEntity.Property(src => src.Id).HasColumnName("ID");
            ////questionAnswerEntity.Property(src => src.Answer).HasColumnName("ANSWER");
            ////questionAnswerEntity.Property(src => src.Point).HasColumnName("POINT");
            ////questionAnswerEntity.Property(src => src.QuestionId).HasColumnName("QUESTIONID");
            //questionAnswerEntity.Property(src => src.OrderId).HasColumnName("ORDERID");

            var userAnswerDetailEntity = modelBuilder.Entity<UserAnswerDetail>();
            ////userAnswerDetailEntity.ToTable("USERANSWERDETAIL");
            ////userAnswerDetailEntity.HasKey(src => src.Id);
            ////userAnswerDetailEntity.Property(src => src.Id).HasColumnName("ID");
            //userAnswerDetailEntity.Property(src => src.UserAnswerId).HasColumnName("USERANSWERID");
            //userAnswerDetailEntity.Property(src => src.QuestionId).HasColumnName("QUESTIONID");
            //userAnswerDetailEntity.Property(src => src.PrivateAnswer).HasColumnName("PRIVATEANSWER");
            //userAnswerDetailEntity.Property(src => src.QuestionAnswerId).HasColumnName("ANSWERID");

            var downloadEntity = modelBuilder.Entity<Download>();
            ////downloadEntity.ToTable("DOWNLOADS");
            ////downloadEntity.HasKey(src => src.Id);
            ////downloadEntity.Property(src => src.Id).HasColumnName("ID");
            ////downloadEntity.Property(src => src.Head).HasColumnName("HEAD");
            ////downloadEntity.Property(src => src.Link).HasColumnName("LINK");
            ////downloadEntity.Property(src => src.RowNo).HasColumnName("ROW_NO");
            //downloadEntity.Property(src => src.GroupId).HasColumnName("GROUP_ID");
            ////downloadEntity.Property(src => src.Description).HasColumnName("DESCRIPTION");
            ////downloadEntity.Property(src => src.DepartmentId).HasColumnName("DEPARTMENT_ID");
            ////downloadEntity.Property(src => src.SectorId).HasColumnName("SECTOR_ID");
            ////downloadEntity.Property(src => src.ProjectName).HasColumnName("PROJECT_NAME");

            var vwBiiForClassEntity = modelBuilder.Entity<VwBiiForCompanyClass>();
            ////vwBiiForClassEntity.ToTable("VW_BII_FOR_COMPANYCLASS");
            ////vwBiiForClassEntity.HasKey(x => new { x.CompanyClassId, x.Year, x.Month });
            ////vwBiiForClassEntity.Property(src => src.CompanyClassId).HasColumnName("COMPANYCLASSID");
            ////vwBiiForClassEntity.Property(src => src.Year).HasColumnName("YEAR");
            ////vwBiiForClassEntity.Property(src => src.Month).HasColumnName("MONTH");
            ////vwBiiForClassEntity.Property(src => src.FirstDateOfMonth).HasColumnName("FIRSTDATEOFMONTH");
            ////vwBiiForClassEntity.Property(src => src.TotalPoints).HasColumnName("TOTALPOINT");

            var vwBiiForTypeEntity = modelBuilder.Entity<VwBiiForCompanyType>();
            ////vwBiiForTypeEntity.ToTable("VW_BII_FOR_COMPANYTYPE");
            ////vwBiiForTypeEntity.HasKey(x => new { x.CompanyTypeId, x.Year, x.Month });
            ////vwBiiForTypeEntity.Property(src => src.CompanyTypeId).HasColumnName("COMPANYTYPEID");
            ////vwBiiForTypeEntity.Property(src => src.Year).HasColumnName("YEAR");
            ////vwBiiForTypeEntity.Property(src => src.Month).HasColumnName("MONTH");
            ////vwBiiForTypeEntity.Property(src => src.FirstDateOfMonth).HasColumnName("FIRSTDATEOFMONTH");
            ////vwBiiForTypeEntity.Property(src => src.TotalPoints).HasColumnName("TOTALPOINT");
        }

        private void ConfigureRelations(ModelBuilder modelBuilder)
        {
            ////modelBuilder.Entity<Company>()
            ////	.HasMany(c => c.Users)
            ////	.WithRequired(u => u.Company)
            ////	.HasForeignKey(u => u.CompanyId);

            ////modelBuilder.Entity<Form>()
            ////	.HasMany(f => f.Questions)
            ////	.WithRequired(q => q.Form)
            ////	.HasForeignKey(q => q.FormId);

            ////modelBuilder.Entity<Question>()
            ////	.HasMany(q => q.QuestionAnswers)
            ////	.WithRequired(qa => qa.Question)
            ////	.HasForeignKey(qa => qa.QuestionId);

            ////modelBuilder.Entity<Question>()
            ////	.HasMany(q => q.UserAnswerDetails)
            ////	.WithRequired(qa => qa.Question)
            ////	.HasForeignKey(qa => qa.QuestionId);

            ////modelBuilder.Entity<UserAnswer>()
            ////	.HasMany(ua => ua.UserAnswersDetail)
            ////	.WithRequired(uad => uad.UserAnswer)
            ////	.HasForeignKey(uad => uad.UserAnswerId);

            ////modelBuilder.Entity<QuestionAnswer>()
            ////	.HasMany(q => q.UserAnswerDetails)
            ////	.WithOptional(uad => uad.QuestionAnswer)
            ////	.HasForeignKey(qa => qa.QuestionAnswerId);

            modelBuilder.Entity<Form>()
                .Property(f => f.CompanyType)
                .IsRequired();

            modelBuilder.Entity<Company>()
               .HasOne(c => c.CompanyType)
               .WithMany()
               .HasForeignKey(c => c.CompanyTypeId);

            modelBuilder.Entity<Company>()
              .HasOne(c => c.CompanyClass)
               .WithMany()
               .HasForeignKey(c => c.CompanyClassId);

            ////modelBuilder.Entity<CompanyType>()
            ////   .HasMany(c => c.CompanyClasses)
            ////   .WithRequired(cc => cc.CompanyType)
            ////   .HasForeignKey(qa => qa.CompanyTypeId);


            modelBuilder.Entity<CompanyType>()
            .HasMany<Download>(ct => ct.Downloads)
            .WithMany(d => d.CompanyTypes);
            ////.Map(cd =>
            ////{
            ////    cd.MapLeftKey("COMPANYTYPE_ID");
            ////    cd.MapRightKey("DOWNLOAD_ID");
            ////    cd.ToTable("COMPANYTYPE_DOWNLOAD");
            ////});

            modelBuilder.Entity<CompanyClass>()
                .HasOne(c => c.Parent)
               .WithMany()
               .HasForeignKey(c => c.ParentId);
        }
    }
}