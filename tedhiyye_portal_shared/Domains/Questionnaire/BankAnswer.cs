﻿using tedhiyye_portal_shared.Domains.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankAnswer
    {
        public int ID { get; set; }
        public string TITLE { get; set; }
        public string ANSWER_GROUP_ID { get; set; }
        public decimal POINT { get; set; }

        public List<BankQuestionDetail> BankQuestionAnswers { get; set; }

    }
}
