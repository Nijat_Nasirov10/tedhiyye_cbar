﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
//using System.Web.Mvc;

namespace tedhiyye_portal_cbar.Models
{
    public class QuestionnaireHistoryItem
    {
        public string CompanyClass { get; set; }
        public string Company { get; set; }
        public string CompanyCode { get; set; }
        public string SurvayType { get; set; }
        public DateTime? SurvayDate { get; set; }
        public int CompanyId { get; set; }
        public int Quarter { get; set; }
        public int Year { get; set; }
        public string questionTypeName { get; set; }
        public string questionTypeId { get; set; }
        public bool IsActive { get; set; }
        public string MonthName
        {
            get
            {
                return SurvayDate.HasValue ? SurvayDate.Value.ToString("MMMM", new CultureInfo("AZ")) : string.Empty;
            }
        }
    }
}