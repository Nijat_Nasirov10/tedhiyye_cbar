﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }

        public int PageSize { get; private set; }
        public int TotalRows { get; private set; }

        const int PageLinkCount = 10;

        public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            PageSize = PageSize;
            TotalRows = count;

            this.AddRange(items);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public int FirstPageToShow
        {
            get
            {
                return ((PageIndex-1) / PageLinkCount) * PageLinkCount + 1;
            }
        }

        public int LastPageToShow
        {
            get
            {
                int n = FirstPageToShow + PageLinkCount-1;
                if (n > TotalPages)
                    n = TotalPages;
                return n;
            }
        }
    }
}
