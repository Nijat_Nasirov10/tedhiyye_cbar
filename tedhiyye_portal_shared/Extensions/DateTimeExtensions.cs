﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace insurance_system_shared.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsBetween(this DateTime dateTime, DateTime start, DateTime end)
        {
            return dateTime.Date >= start.Date && dateTime.Date <= end.Date;
        }

        public static int GetQuarter(this DateTime date)
        {
            if (date.Month >= 2 && date.Month <= 4)
                return 1;
            else if (date.Month >= 5 && date.Month <= 7)
                return 2;
            else if (date.Month >= 8 && date.Month <= 10)
                return 3;
            else
                return 4;
        }

    }
}
