﻿namespace tedhiyye_portal_shared
{
    public enum StatUnitUserType
    {
        Bank = 1,
        StatKom = 2,
        Customs = 3
    }

    public enum AuthType
    {
        UsernamePassword = 1,
        ESignature = 2
    }

    public enum AllowedAuthType
    {
        UsernamePassword = 0,
        ESignature = 1,
        UsernamePasswordESignature = 2,
        Deactivated = 3
    }

    public enum ReportStatus
    {
        Yuklenib = 0,
        Tesdiq_edilib = 1,
        Imtina_edilib = 2,
        Yoxlanilir = 3,
        Sehvdir_Qebul_olunmadi = 4,
        Duzdur = 5,
        Yoxlama_yoxdur = 6,
        Serti_duzdur = 7,
        Gecikdirilib = 8,
        Sehv_dovr_secilmisdir = 9,
    }

    public enum ReportStatusCrossCheck
    {
        cross_yoxlama_gozlenilir = 1,
        cross_yoxlama_duzdur = 2,
        cross_yoxlama_sehvdir = 3,
    }

    public enum ReportUploadFileStatus
    {
        Uploaded = 0,
        ConvertingToXML = 1,
        ErrorConvertingToXML = 2,
        ConvertedToXML = 3,
        ErrorInsertingToDB = 4,
        InsertedToDB = 5,
        ErrorValidating = 6,
        ConfirmAsCorrect = 7
    }

    public enum BankUserRole
    {
        Operator = 1,
        Admin = 2
    }

    public enum CBARUserRole
    {
        Bank_prudential = 100,
        Real_sector = 200,
        Foregn_sector = 300
    }

    public enum PeriodType
    {
        Unknown,
        /// <summary>
        /// Günlük
        /// </summary>
        D,
        /// <summary>
        /// Ay
        /// </summary>
        M,
        /// <summary>
        /// Yarım Ay
        /// </summary>
        HM,
        /// <summary>
        /// Rüb
        /// </summary>
        Q,
        /// <summary>
        /// Həftə
        /// </summary>
        W,
        /// <summary>
        /// Yarım günlük
        /// </summary>
        HD,
    }
}