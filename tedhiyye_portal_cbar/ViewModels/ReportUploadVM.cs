﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_cbar.ViewModels
{
    public class ReportUploadVM
    {
        [Required(ErrorMessage = "Statistik vahid seçilməyib")]
        public int? BankId { get; set; }

        [Required(ErrorMessage = "Hesabat tipi seçilməyib")]
        [Range(0,5000,ErrorMessage = "Hesabat tipi seçilməyib")]
        public int? report_file_type_id { get; set; }

        [Required(ErrorMessage = "Hesabat dövrü başlama seçilməyib")]
        public string periodstart { get; set; }

        [Required(ErrorMessage = "Hesabat dövrü bitmə seçilməyib")]
        public string periodend { get; set; }
        public string[] bank_types { get; set; }
        public bool ShowPhqe { get; set; }
        public int ReportVersionId { get; set; }
    }
}