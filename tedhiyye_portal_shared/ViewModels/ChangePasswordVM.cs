﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.ViewModels
{
    public class ChangePasswordVM
    {
        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [DataType(DataType.Password)]
        [Display(Name = "İndiki parol")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [StringLength(100, ErrorMessage = "Parol ən az 6 simvol olmalıdır", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni parol")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Yeni parol (təkrar)")]
        [Compare("NewPassword", ErrorMessage = "Yeni parol ilə təkrarı uyğun gəlmir")]
        public string ConfirmPassword { get; set; }
    }
}
