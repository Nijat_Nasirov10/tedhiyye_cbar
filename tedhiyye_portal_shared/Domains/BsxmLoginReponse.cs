﻿using System;

namespace tedhiyye_portal_shared.Domains
{
    public class BsxmLoginReponse
    {
        public string pin { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string middlename { get; set; }
        public string email { get; set; }
        public string role { get; set; }
        public int roleId { get; set; }
        public string cert { get; set; }
        public string orgCode { get; set; }
        public string tin { get; set; }
        public bool isMbUser { get; set; }
        public string orgName { get; set; }
        public string orgId { get; set; }
        public int hasQC { get; set; }
        public DateTime validDate { get; set; }
        public string userId { get; set; }
        public string regId { get; set; }
        public int authType { get; set; }
        public string userName { get; set; }
        public string did { get; set; }
        public string participantId { get; set; }
        public string certSerialNo { get; set; }
    }
}
