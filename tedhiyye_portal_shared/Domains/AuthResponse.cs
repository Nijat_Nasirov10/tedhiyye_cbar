﻿using tedhiyye_portal_shared;
using System;
using System.Collections.Generic;

namespace tedhiyye_portal_shared.Domains
{
    public class AuthResponse
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public AuthType AuthenticationType { get; set; }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }

        public int BankId { get; set; }
        public string BankName { get; set; }
        public StatUnitUserType? UserType { get; set; }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string PIN { get; set; }

        public int CompanyId { get; set; }

        public int RoleId { get; set; }

        public int HasQCStatements { get; set; } // 0 or 1

        public DateTime ValidToDate { get; set; }

        public List<int> Roles { get; set; }

        public List<int> RealSectorCompanyTypes { get; set; }
    }
}