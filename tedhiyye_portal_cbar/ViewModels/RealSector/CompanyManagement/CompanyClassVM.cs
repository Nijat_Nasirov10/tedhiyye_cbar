﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using tedhiyye_portal_shared.Domains;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
//using System.Web.Mvc;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.CompanyManagement
{
    public class CompanyClassVM
    {
        public int Id { get; set; }
        public int CompanyTypeId { get; set; }
        public string Name { get; set; }
        public string Parent { get; set; }
    }

    public class CreateEditCompanyClassVM
    {
        public int? Id { get; set; }
        public int? CompanyTypeId { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [Display(Name = "Bölmə")]
        public string Name { get; set; }

        [DisplayName("Üst Bölmə")]
        public int? ParentId { get; set; }

        public IEnumerable<SelectListItem> CompanyClasses { get; set; }
    }
}