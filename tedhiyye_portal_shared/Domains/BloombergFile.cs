﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
	public class BloombergFile
	{
		public string REGDATE { get; set; }
		public string FILENAME { get; set; }
		public string REGUSER { get; set; }
		public string PERIOD { get; set; }
		public string Version { get; set; }
        public DateTime UPLOAD_TIME { get; set; }
    }

	public class BaseBloombergData
	{
		public int Id { get; set; }
		public string SHDATE { get; set; }
		public string BUYER { get; set; }
		public string BUY { get; set; }
		public string SELLER { get; set; }
		public string SELL { get; set; }
		public string PAIR { get; set; }
		public string CURRENCY { get; set; }
		public string AZN { get; set; }
		public string RATE { get; set; }
	}

	public class NewBloombergData : BaseBloombergData
	{
		public int REPORTID { get; set; }
	}

	public class OldBloombergData : BaseBloombergData
	{
		public string BREGDATE { get; set; }
	}

}
