﻿namespace tedhiyye_portal_shared.Domains
{
    public class ReportCheckError
    {
        public int REPORTTYPE { get; set; }
        public string DESCRIPTION { get; set; }
        public int RULEID { get; set; }
        public int CHECKID { get; set; }
        public int REPORTS_FILE_ID { get; set; }
        public int REPORTID { get; set; }
        public int IS_MULTIROW_RESULT { get; set; }
    }
}
