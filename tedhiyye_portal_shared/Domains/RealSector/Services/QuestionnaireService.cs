﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using tedhiyye_portal_shared.Domains.RealSector.DatabaseContext;
using tedhiyye_portal_shared.Domains.RealSector.Models;
using tedhiyye_portal_shared.Helpers;

namespace tedhiyye_portal_shared.Domains.RealSector.Services
{
    public class QuestionnaireService : BaseService
    {
        public QuestionnaireService(ApplicationContext context)
            : base(context)
        {
        }

        #region Forms

        public Form GetForm(int? formId, IReadOnlyCollection<int> accessedCompanyTypes)
        {
            if (!formId.HasValue)
                throw new ArgumentNullException();

            return _context.Forms
                .Where(src => accessedCompanyTypes.Contains(src.CompanyTypeId))
                .SingleOrDefault(src => src.Id == formId.Value);
        }

        public IEnumerable<Form> GetForms(IReadOnlyCollection<int> accessedCompanyTypes)
        {
            return _context.Forms
                .Where(src => accessedCompanyTypes.Contains(src.CompanyTypeId))
                .Include(src => src.CompanyType)
                .ToList();
        }

        public IEnumerable<Form> GetForms(int? companyTypeId, bool onlyActive)
        {
            if (!companyTypeId.HasValue)
                throw new ArgumentNullException();

            var currentDate = DateTime.Now;

            var forms = _context.Forms
                .Include(src => src.CompanyType)
                .Where(src => src.CompanyTypeId == companyTypeId)
                .AsEnumerable();

            if (onlyActive)
                forms = forms.Where(src => IsActive(src, currentDate));

            return forms
                .Select(src => new Form
                {
                    Id = src.Id,
                    Name = GetAdvancedName(src.Name, currentDate),
                    CompanyType = src.CompanyType,
                    CompanyTypeId = src.CompanyTypeId,
                    OrderId = src.OrderId
                });
        }

        public IEnumerable<UserAnswer> GetUserAnswers(int? userId)
        {
            if (!userId.HasValue)
                throw new ArgumentNullException();

            return _context.UserAnswers
                .Include(src => src.Form)
                .Where(src => src.UserId == userId)
                .AsEnumerable()
                .Select(src =>
                {
                    return new UserAnswer
                    {
                        Id = src.Id,
                        AnswerDate = src.AnswerDate,
                        FormId = src.FormId,
                        UserId = src.UserId,
                        Form = new Form { Name = GetAdvancedName(src.Form.Name, src.AnswerDate) }
                    };
                });
        }

        public bool CreateForm(Form form)
        {
            if (form == null)
                throw new ArgumentNullException();

            var forms = _context.Forms;
            if (forms.Any(src => src.Name == form.Name))
                return false;

            forms.Add(form);

            return _context.SaveChanges() > 0;
        }

        public bool EditForm(Form form)
        {
            if (form == null)
                throw new ArgumentNullException();

            var forms = _context.Forms;
            if (forms.Any(src => src.Id == form.Id))
                if (!forms.Any(src => src.Id != form.Id && src.Name == form.Name))
                {
                    forms.Attach(form);
                    var formEntry = _context.Entry(form);
                    formEntry.State = EntityState.Modified;

                    return _context.SaveChanges() > 0;
                }

            return false;
        }

        public bool RemoveForm(int? formId)
        {
            if (!formId.HasValue)
                throw new ArgumentNullException();

            var form = _context.Forms.Find(formId);
            if (form == null)
                return false;

            _context.Forms.Remove(form);
            return _context.SaveChanges() > 0;
        }

        public IEnumerable<CompanyType> GetCompanyTypes(IReadOnlyCollection<int> accessedCompanyTypes)
        {
            return _context.CompanyTypes
                .Where(src => accessedCompanyTypes.Contains(src.Id))
                .AsEnumerable();
        }

        private bool IsActive(Form form, DateTime date)
        {
            if (form.Name.Contains("aylıq"))
            {
                if (/*date.Day < 16 ||*/ GetActiveFormQuarter(date.Month) != null || date.Month == 1)
                    return false;
            }
            else if (form.Name.Contains("rüblük"))
            {
                if (GetActiveFormQuarter(date.Month) == null || date.Month == 1)
                    return false;
            }
            else if (form.Name.Contains("illik"))
            {
                if (date.Month != 1)
                    return false;
            }

            return true;
        }

        private string GetAdvancedName(string formName, DateTime date)
        {
            string advancedName = formName;
            if (formName.Contains("aylıq"))
            {
                string currentMonth = DateHelper.GetMonthName(date.Month, "az-Latn");
                advancedName = $"{advancedName} - {currentMonth}";
            }
            else if (formName.Contains("rüblük"))
            {
                int? activeQuarter = GetActiveFormQuarter(date.Month);
                if (activeQuarter.HasValue)
                {
                    string currentQuarter = DateHelper.ToRoman(activeQuarter.Value);
                    int quarterYear = GetQuarterYear(activeQuarter.Value, date);

                    advancedName = $"{advancedName} - {quarterYear} - {currentQuarter}";
                }

            }
            else if (formName.Contains("illik"))
            {
                int formYear = date.Year - 1;
                advancedName = $"{advancedName} - {formYear}";
            }

            return advancedName;
        }

        private int? GetActiveFormQuarter(int month)
        {
            int formQuarter;
            Dictionary<int, int> requeriedQuarterMonths = new Dictionary<int, int>
            {
                {1, 4},
                {4, 1},
                {7, 2},
                {10, 3},
            };

            bool isContains = requeriedQuarterMonths.TryGetValue(month, out formQuarter);
            return isContains ? (int?)formQuarter : null;
        }

        private int GetQuarterYear(int quarter, DateTime date)
        {
            if (quarter < 4) return date.Year;
            else if (quarter == 4) return date.Year - 1;

            throw new ArgumentOutOfRangeException();
        }

        #endregion

        #region Questions

        public Question GetQuestion(int? questionId)
        {
            if (!questionId.HasValue)
                throw new ArgumentNullException();

            return _context.Questions
                .Include(src => src.Form)
                .SingleOrDefault(src => src.Id == questionId);
        }

        public IEnumerable<Question> GetQuestions(int? formId)
        {
            if (!formId.HasValue)
                throw new ArgumentNullException();

            return _context.Questions
                .Where(src => src.FormId == formId)
                .AsEnumerable();
        }

        public bool CreateQuestion(Question question)
        {
            if (question == null)
                throw new ArgumentNullException();

            var questions = _context.Questions;
            if (HasQuestionInForm(question))
                return false;

            questions.Add(question);

            return _context.SaveChanges() > 0;
        }

        public bool EditQuestion(Question question)
        {
            if (question == null)
                throw new ArgumentNullException();

            var questions = _context.Questions;
            var dbQuestion = questions.Find(question.Id);

            if (dbQuestion != null && dbQuestion.FormId == question.FormId)
            {
                if (!HasQuestionInForm(question))
                {
                    _context.Entry(dbQuestion).CurrentValues.SetValues(question);
                    return _context.SaveChanges() > 0;
                }
            }

            return false;
        }

        public bool ChangeQuestionStatus(int? questionId)
        {
            if (!questionId.HasValue)
                throw new ArgumentNullException();

            var question = _context.Questions.Find(questionId);

            if (question != null)
                question.IsActive = !question.IsActive;

            return _context.SaveChanges() > 0;
        }

        public bool RemoveQuestion(int? questionId)
        {
            if (!questionId.HasValue)
                throw new ArgumentNullException();

            var question = _context.Questions.Find(questionId);
            if (question == null)
                return false;

            _context.Questions.Remove(question);
            return _context.SaveChanges() > 0;
        }

        #region Helpers

        private bool HasQuestionInForm(Question question)
        {
            var questions = _context.Questions;
            return questions.Any(src => src.Id != question.Id &&
                                        src.FormId == question.FormId &&
                                        src.QuestionText == question.QuestionText);
        }

        #endregion
        #endregion

        #region Answers

        public QuestionAnswer GetAnswer(int? answerId)
        {
            if (!answerId.HasValue)
                throw new ArgumentNullException();

            return _context.QuestionAnswers
                .Include(src => src.Question)
                .SingleOrDefault(src => src.Id == answerId);
        }

        public IEnumerable<QuestionAnswer> GetAnswers(int? questionId)
        {
            if (!questionId.HasValue)
                throw new ArgumentNullException();

            return _context.QuestionAnswers
                .Where(src => src.QuestionId == questionId)
                .AsEnumerable();
        }

        public bool CreateAnswer(QuestionAnswer answer)
        {
            if (answer == null)
                throw new ArgumentNullException();

            var answers = _context.QuestionAnswers;
            if (HasAnswerInQuestion(answer))
                return false;

            answers.Add(answer);

            return _context.SaveChanges() > 0;
        }

        public bool EditAnswer(QuestionAnswer answer)
        {
            if (answer == null)
                throw new ArgumentNullException();

            var answers = _context.QuestionAnswers;
            var dbAnswer = answers.Find(answer.Id);

            if (dbAnswer != null && dbAnswer.QuestionId == answer.QuestionId)
                if (!HasAnswerInQuestion(answer))
                {
                    _context.Entry(dbAnswer).CurrentValues.SetValues(answer);
                    return _context.SaveChanges() > 0;
                }

            return false;
        }

        public bool RemoveAnswer(int? answerId)
        {
            if (!answerId.HasValue)
                throw new ArgumentNullException();

            var answer = _context.QuestionAnswers.Find(answerId);
            if (answer == null)
                return false;

            _context.QuestionAnswers.Remove(answer);
            return _context.SaveChanges() > 0;
        }

        #region Helpers

        private bool HasAnswerInQuestion(QuestionAnswer answer)
        {
            var answers = _context.QuestionAnswers;
            return answers.Any(src => src.Id != answer.Id &&
                                        src.QuestionId == answer.QuestionId &&
                                        src.Answer == answer.Answer);
        }

        #endregion
        #endregion
    }
}
