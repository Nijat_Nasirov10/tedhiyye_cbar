﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tedhiyye_portal_shared.ViewModels
{
    public class ReportActionMessageVM
    {
        public string Success { get; set; }
        public string Error { get; set; }
    }
}
