﻿
namespace tedhiyye_portal_shared.Domains.RealSector.Models.Reports
{
    public class AnswerReportItem
    {
        public string CompanyClass { get; set; }
        public int? CId { get; set; }
        public string Company { get; set; }
        public string Area { get; set; }
        public string SequenceNo { get; set; }
        public string Ifnt { get; set; }
        public int? CompanyTypeId { get; set; }
        public int? CompanyClassId { get; set; }
        public string PrivateAnswer { get; set; }
        public int? QuestionNumber { get; set; }
        public string Answer { get; set; }
        public int? Point { get; set; }
    }
}
