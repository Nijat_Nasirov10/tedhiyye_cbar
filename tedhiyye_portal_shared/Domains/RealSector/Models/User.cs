﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("USERSCOMPANIES")]
    public class User
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("USERNAME")]
        public string Username { get; set; }
        [Column("PASSWORD")]
        public string Password { get; set; }
        
        [ForeignKey("Company")]
        [Column("COMPANYID")]
        public int CompanyId { get; set; }
        [Column("EMAIL")]
        public string Email { get; set; }
        [Column("PHONE")]
        public string Phone { get; set; }

        public virtual Company Company { get; set; }

        //public int? BankId { get; set; }
    }
}