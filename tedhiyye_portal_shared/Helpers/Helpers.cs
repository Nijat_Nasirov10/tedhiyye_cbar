﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Helpers
{
    public static class DateHelper
    {
        public static int GetQuarter(DateTime date)
        {
            if (date.Month >= 2 && date.Month <= 4)
                return 1;
            else if (date.Month >= 5 && date.Month <= 7)
                return 2;
            else if (date.Month >= 8 && date.Month <= 10)
                return 3;
            else
                return 4;
        }

        public static string GetMonthName(int month, string culture)
        {
            return new CultureInfo(culture).DateTimeFormat.MonthNames[month - 1];
        }

        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999))
                throw new ArgumentOutOfRangeException();

            if (number < 1) return string.Empty;
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);

            throw new ArgumentOutOfRangeException();
        }


        public static DateTime[] GetMinAndMAxDatesFromQuarterAndYear(int? year, int? quarter)
        {
            string minMonth;
            string maxMonth;

            switch (quarter)
            {
                case 1:
                    minMonth = "02";
                    maxMonth = "04";
                    break;
                case 2:
                    minMonth = "05";
                    maxMonth = "07";
                    break;
                case 3:
                    minMonth = "08";
                    maxMonth = "10";
                    break;
                case 4:
                    minMonth = "11";
                    maxMonth = "01";
                    break;
                default:
                    minMonth = "";
                    maxMonth = "";
                    break;
            }

            var lastDayOfMonth = DateTime.DaysInMonth(year.Value, int.Parse(maxMonth));


            string minDate = $"01.{minMonth}.{year}";
            if (quarter == 4)
            {
                year++;
            }
            string maxDate = $"{lastDayOfMonth}.{maxMonth}.{year}";

            DateTime minDateTime = DateTime.ParseExact(minDate, "dd.MM.yyyy", null);
            DateTime maxDateTime = DateTime.ParseExact(maxDate, "dd.MM.yyyy", null);

            DateTime[] minMaxDates = new[] { minDateTime, maxDateTime };
            return minMaxDates;

        }
    }
}
