﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tedhiyye_portal_cbar.Models
{
    public class ComposeMessageVM
    {
        [Required(ErrorMessage = "Kimə göndərilməsi seçilməyib")]
        [Display(Name = "Kimə")]
        public int? DepartmentId { get; set; }

        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [Display(Name = "Mövzu")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [Display(Name = "Mətn")]
        [DataType(DataType.MultilineText)]
        public string Body { get; set; }
    }
}