﻿using tedhiyye_portal_shared;
using System;

namespace tedhiyye_portal_shared.Domains
{
    public class UploadedReportFile
    {
        public int ID { get; set; }
        public string UPLOAD_FILE_NAME { get; set; }
        public string UPLOAD_FILE_CONTENT_TYPE { get; set; }
        public DateTime UPLOAD_TIME { get; set; }
        public DateTime? CONVERT_TIME { get; set; }
        public int UPLOAD_USER_ID { get; set; }
        public string UPLOAD_USER_IP { get; set; }
        public int BANKID { get; set; }
        public string BankName { get; set; }
        public string BANK { get; set; }
        public string Upload_User_Name { get; set; }
        public int Report_Id { get; set; }
        public ReportUploadFileStatus Status { get; set; }
        public ReportUploadFileStatus REPORT_FILE_STATUS_ID { get; set; }
        public string StatusText { get; set; }

        public DateTime? PERIODSTART { get; set; }
        public DateTime? PERIODEND { get; set; }
        public int? REPORT_FILE_TYPE_ID { get; set; }
        public string REPORT_FILE_TYPE_NAME { get; set; }
        public string Version { get; set; }

        public string STATUS { get; set; }
        public int CONFIRMSTATUS { get; set; }
        public byte[] UPLOAD_FILE_DATA { get; set; }
    }
}
