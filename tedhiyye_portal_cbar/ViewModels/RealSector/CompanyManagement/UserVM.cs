﻿using tedhiyye_portal_shared.Domains;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.CompanyManagement
{
    public class UserVM
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Company { get; set; }
    }

    public class CreateUserVM
    {
        public int? Id { get; set; }
        public int? CompanyId { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [DisplayName("İstifadəçi")]
        public string Username { get; set; }

        [EmailAddress(ErrorMessage = ValidationMessage.Email)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [StringLength(100, ErrorMessage = "Parol ən az 6 simvol olmalıdır", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [DisplayName("Yeni parol")]
        public string Password { get; set; }
       
        [DataType(DataType.Password)]
        [DisplayName("Yeni parol (təkrar)")]
        [Compare("Password", ErrorMessage = "Yeni parol ilə təkrarı uyğun gəlmir")]
        public string ConfirmPassword { get; set; }
    }

    public class EditUserVM
    {
        public int? Id { get; set; }
        public int? CompanyId { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [DisplayName("İstifadəçi")]
        public string Username { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = ValidationMessage.Email)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = "Parol ən az 6 simvol olmalıdır", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni parol")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Yeni parol (təkrar)")]
        [Compare("Password", ErrorMessage = "Yeni parol ilə təkrarı uyğun gəlmir")]
        public string ConfirmPassword { get; set; }
    }
}