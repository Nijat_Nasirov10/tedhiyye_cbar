﻿using tedhiyye_portal_shared.Domains.RealSector.DatabaseContext;

namespace tedhiyye_portal_shared.Domains.RealSector.Services
{
    public abstract class BaseService
    {
        protected readonly ApplicationContext _context;
        protected BaseService(ApplicationContext context)
        {
            _context = context;
        }
     }
}
