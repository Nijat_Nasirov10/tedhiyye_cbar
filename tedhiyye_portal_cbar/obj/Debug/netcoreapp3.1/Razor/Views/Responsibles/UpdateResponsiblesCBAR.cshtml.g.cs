#pragma checksum "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1fc46b83175ecf87c2684e8cfffe723977d98f1a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Responsibles_UpdateResponsiblesCBAR), @"mvc.1.0.view", @"/Views/Responsibles/UpdateResponsiblesCBAR.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared.Domains;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1fc46b83175ecf87c2684e8cfffe723977d98f1a", @"/Views/Responsibles/UpdateResponsiblesCBAR.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7403705723c95d1543623efd76ed381e059f9cb9", @"/Views/_ViewImports.cshtml")]
    public class Views_Responsibles_UpdateResponsiblesCBAR : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ResponsiblesInfo>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/Content/DataTables/css/dataTables.bootstrap.min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/DataTables/jquery.dataTables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/DataTables/dataTables.bootstrap.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/DataTables/dataTables.buttons.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/jszip.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/DataTables/buttons.html5.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Scripts/DataTables/buttons.bootstrap.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
  
    ViewBag.Title = "Məsul şəxslər idarəetmə paneli";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            DefineSection("Currentpagelink", async() => {
                WriteLiteral("\r\n    <li><a href=\"javascript:void(0)\">Məlumat</a></li>\r\n    <li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>\r\n    <li>\r\n        ");
#nullable restore
#line 11 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
   Write(Html.ActionLink("Məsul şəxslər (MB)", "ResponsiblesCBAR", "Responsibles"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </li>\r\n    <li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>\r\n    <li>\r\n        ");
#nullable restore
#line 15 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
   Write(Html.ActionLink("Məsul şəxslər idarəetmə paneli", "UpdateResponsiblesCBAR", "Responsibles"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </li>\r\n");
            }
            );
            WriteLiteral("\r\n<div class=\"col-md-12 esahs-container-padd\" style=\"padding-top:20px\">\r\n");
#nullable restore
#line 20 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
     if (TempData["SuccessMessage"] != null)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"alert alert-success\" role=\"alert\" style=\"padding-top:10px\">\r\n            ");
#nullable restore
#line 23 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
       Write(TempData["SuccessMessage"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(" <br />\r\n        </div>\r\n");
#nullable restore
#line 25 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n    <h1>");
#nullable restore
#line 29 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
   Write(Model.ReportFileTypeName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n    <br />\r\n");
#nullable restore
#line 31 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
     using (Html.BeginForm("UpdateResponsiblesCBAR", "Responsibles", FormMethod.Post, new { id = "foFilter" }))
    {
        var BankUsersForExecutor = (List<SelectListItem>)ViewBag.CbarUsersList;
        var BankUsersForCurator = (List<SelectListItem>)ViewBag.CbarUsersList;

        

#line default
#line hidden
#nullable disable
#nullable restore
#line 36 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
   Write(Html.Hidden("reportFileTypeId", Model.ReportFileTypeId));

#line default
#line hidden
#nullable disable
            WriteLiteral("        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a10933", async() => {
                WriteLiteral("\r\n            <div class=\"form-group\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-2\" style=\"margin-top:5px\">\r\n                        ");
#nullable restore
#line 41 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                   Write(Html.Label("executor", "İcraçı:"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </div>\r\n");
#nullable restore
#line 43 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                      
                        BankUsersForExecutor.Remove(BankUsersForExecutor.FirstOrDefault(x => x.Value == Model.Executor.EmployeeId.ToString()));

#line default
#line hidden
#nullable disable
                WriteLiteral("                        <div class=\"col-md-2\">\r\n                            ");
#nullable restore
#line 46 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                       Write(Html.DropDownList("executorId", BankUsersForExecutor, Model.Executor.Name, new { @class = "form-control select2" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                        </div>\r\n");
#nullable restore
#line 48 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                        BankUsersForExecutor.Add(new SelectListItem { Text = Model.Executor.Name, Value = Model.Executor.EmployeeId });
                    

#line default
#line hidden
#nullable disable
                WriteLiteral("                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-2\" style=\"margin-top:5px\">\r\n                        ");
#nullable restore
#line 55 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                   Write(Html.Label("curator", "Rəhbər şəxs:"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </div>\r\n");
#nullable restore
#line 57 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                      

                        BankUsersForCurator.Remove(BankUsersForCurator.FirstOrDefault(x => x.Value == Model.Curator.EmployeeId.ToString()));

#line default
#line hidden
#nullable disable
                WriteLiteral("                        <div class=\"col-md-2\">\r\n                            ");
#nullable restore
#line 61 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                       Write(Html.DropDownList("curatorId", BankUsersForCurator, Model.Curator.Name, new { @class = "form-control select2" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                        </div>\r\n");
#nullable restore
#line 63 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                        BankUsersForCurator.Add(new SelectListItem { Text = Model.Curator.Name, Value = Model.Curator.EmployeeId });
                    

#line default
#line hidden
#nullable disable
                WriteLiteral("                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-2\" style=\"margin-top:5px\">\r\n                        ");
#nullable restore
#line 70 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                   Write(Html.Label("Yeni əvəz edən şəxs"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </div>\r\n                    <div class=\"col-md-2\">\r\n                        ");
#nullable restore
#line 73 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                   Write(Html.DropDownList("SubstituteId", (List<SelectListItem>)ViewBag.CbarUsersList, "---", new { @class = "form-control select2" }));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                    </div>
                </div>
                <br />
                <div class=""col-md-4"">
                    <input type=""submit"" value=""Təsdiq et"" class=""btn btn-primary"" style=""float:right;margin-right:5px"" />
                </div>
            </div>
            <div class=""form-group"">
                <br />
                ");
#nullable restore
#line 83 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
           Write(Html.Label("Əvəz edən şəxslər"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                <div class=""table table-responsive esahs-table table-hover dataTable no-footer"">
                    <table class=""table table-bordered dataTable no-footer"" role=""grid"" style=""width:60%"">
                        <thead>
                            <tr>
                                <th>
                                    Adı soyadı
                                </th>
                                <th>
                                    Struktur bölmə
                                </th>
                                <th>
                                    Vəzifə
                                </th>
                                <th>
                                    Sil
                                </th>
                        </thead>
                        <tbody>
");
#nullable restore
#line 102 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                             if (Model.SubstituteUserIds.Length > 0)
                            {
                                for (int i = 0; i < Model.SubstituteUserIds.Length; i++)
                                {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                    <tr>\r\n\r\n                                        <td>\r\n                                            ");
#nullable restore
#line 109 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                                       Write(Model.SubstituteNames[i]);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
#nullable restore
#line 112 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                                       Write(Model.SubstituteDepartment);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
#nullable restore
#line 115 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                                       Write(Model.SubstitutePosition[i]);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                        </td>\r\n");
#nullable restore
#line 117 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                                         if (!String.IsNullOrWhiteSpace(Model.SubstituteUserIds[i]))
                                        {


#line default
#line hidden
#nullable disable
                WriteLiteral("                                            <td>\r\n                                                <a");
                BeginWriteAttribute("href", " href=\"", 5558, "\"", 5724, 1);
#nullable restore
#line 121 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
WriteAttributeValue("", 5565, Url.Action("DeleteUserJob", "Responsibles" , new { reportFileTypeId=Model.ReportFileTypeId, userId=Model.SubstituteUserIds[i], jobId=2, roleId=100,bankId=0 }), 5565, 159, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"font-size:20px\"></i></a>\r\n                                            </td>\r\n");
#nullable restore
#line 123 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"


                                        }
                                        else
                                        {

#line default
#line hidden
#nullable disable
                WriteLiteral("                                            <td></td>\r\n");
#nullable restore
#line 129 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                                        }

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                                    </tr>\r\n");
#nullable restore
#line 132 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
                                }
                            }

#line default
#line hidden
#nullable disable
                WriteLiteral("                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n\r\n        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
#nullable restore
#line 140 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Responsibles\UpdateResponsiblesCBAR.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "1fc46b83175ecf87c2684e8cfffe723977d98f1a22970", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a24149", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a25249", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a26349", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a27449", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a28549", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fc46b83175ecf87c2684e8cfffe723977d98f1a29649", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
            }
            );
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ResponsiblesInfo> Html { get; private set; }
    }
}
#pragma warning restore 1591
