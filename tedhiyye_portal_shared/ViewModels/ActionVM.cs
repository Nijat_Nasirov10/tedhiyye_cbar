﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.ViewModels
{
    public class ActionVM
    {
        [Required(ErrorMessage = "Seçilməyib")]
        public int Id { get; set; }
        public string Action { get; set; }
        //tahmin
        [Required(ErrorMessage = "Hesabat tipi seçilməyib")]
        public int report_file_type_id { get; set; }
    }
}
