﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class CertUser
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        //public string SurName { get; set; }
        public string Position { get; set; }
        public string Phone { get; set; }
        public string Cert { get; set; }
    }

    public class CertUserReportFileTypeDetail
    {
        public int ReportFileTypeId { get; set; }
        public string ReportFileTypeName { get; set; }
        public CertUser User { get; set; }
    }
}
