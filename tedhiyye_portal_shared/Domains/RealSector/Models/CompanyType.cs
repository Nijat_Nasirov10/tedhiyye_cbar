﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
	[Table("COMPANYTYPE")]
	public class CompanyType
	{
		public CompanyType()
		{
			Downloads = new HashSet<Download>();
		}

		[Key]
		[Column("ID")]
		public int Id { get; set; }

		[Column("COMPANYTYPE")]
		public string Type { get; set; }

		public ICollection<CompanyClass> CompanyClasses { get; set; }
		public ICollection<Download> Downloads { get; set; }
	}
}
