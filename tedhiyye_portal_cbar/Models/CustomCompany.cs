﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tedhiyye_portal_cbar.Models
{
    public class CustomCompany
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsBank { get; set; }
    }
}