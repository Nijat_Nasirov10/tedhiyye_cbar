﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
	public class DownloadFileInfo
	{
		public int Id { get; set; }
		public string Head { get; set; }
		public string Link { get; set; }
		public int GroupId { get; set; }
		public string Description { get; set; }
		public int? DepartmentId { get; set; }
		public string DepartmentName { get; set; }
		public string SectionName { get; set; }
		public string ProjectName { get; set; }
		public byte[] UPLOAD_FILE_DATA { get; set; }
	}
}
