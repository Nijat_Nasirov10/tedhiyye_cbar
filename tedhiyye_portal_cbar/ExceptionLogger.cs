﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar
{
    public class ExceptionLogger : ILogger
    {
        private static readonly object _locker = new object();

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel == LogLevel.Error;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (logLevel == LogLevel.Error)
            {
                while (exception.InnerException != null)
                    exception = exception.InnerException;

                LogException(exception);
            }
        }

        public static void LogException(Exception error)
        {
            lock (_locker)
            {
                string path = Path.Combine(Directory.GetCurrentDirectory(), "App_Data", "ErrorLog");

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string filePath = Path.Combine(path, string.Format("{0}.log", DateTime.Today.ToString("yyyy-MM-dd")));

                using (StreamWriter w = File.AppendText(filePath))
                {
                    w.WriteLine("TIME: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff"));
                    w.WriteLine("EXCEPTION STACKTRACE BELOW:\n " + error.StackTrace);
                    w.WriteLine();
                    w.WriteLine();
                    w.WriteLine();
                    w.Flush();
                    w.Close();
                }
            }
        }


        public sealed class ExceptionLoggerProvider : ILoggerProvider
        {
            public ILogger CreateLogger(string categoryName)
            {
                return new ExceptionLogger();
            }

            public void Dispose()
            {
            }
        }
    }
}
