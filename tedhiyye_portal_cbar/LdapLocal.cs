﻿using Novell.Directory.Ldap;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Threading.Tasks;
using LdapConnection = Novell.Directory.Ldap.LdapConnection;
using SearchScope = System.DirectoryServices.SearchScope;

namespace tedhiyye_portal_cbar
{
    public class LdapLocal
    {
        public LdapLocal()
        {
        }

        public class LDAPUser
        {
            public string Firstname { get; set; }
            public string LastName { get; set; }
            public string Mail { get; set; }
            public string Username { get; set; }
            public string Department { get; set; }
            public string Cn { get; internal set; }
            public string UserPrincipalName { get; internal set; }
            public string St { get; internal set; }
            public string GivenName { get; internal set; }
            public string Samaccountname { get; internal set; }
            public string Description { get; internal set; }
            public string Telephonenumber { get; internal set; }
            public string Displayname { get; internal set; }
        }

        private string path = AppSettings.GetValue<string>("Ldap.path");
        //string user = AppSettings.GetValue<string>("Ldap.userName");
        //string password = AppSettings.GetValue<string>("Ldap.password");
        private string baseDn = AppSettings.GetValue<string>("Ldap.baseDn");
        private string filter = AppSettings.GetValue<string>("Ldap.filter");

        public bool NovellLogin(string username, string password, out LDAPUser ldapuser)
        {
            string ldapHost = path;
            int ldapPort = LdapConnection.DEFAULT_PORT;

            string[] attributes = new string[] { "cn", "userPrincipalName", "st", "givenname", "samaccountname",
                "description", "telephonenumber", "department", "displayname", "mail", "givenName", "sn" };

            try
            {
                using (var con = new LdapConnection { SecureSocketLayer = false })
                {
                    con.Connect(ldapHost, ldapPort);
                    con.Bind(string.Format("{0}@cbar.az", username), password);

                    ldapuser = new LDAPUser();
                    ldapuser.Username = username;

                    LdapSearchQueue queue = con.Search(
                        baseDn,
                        LdapConnection.SCOPE_SUB,
                        filter,
                        attributes,
                        false,
                        (LdapSearchQueue)null,
                        (LdapSearchConstraints)null);

                    LdapMessage message;

                    if ((message = queue.getResponse()) != null && message is LdapSearchResult)
                    {
                        LdapEntry entry = ((LdapSearchResult)message).Entry;
                        LdapAttributeSet attributeSet = entry.getAttributeSet();

                        //ldapuser = new LDAPUser
                        //{
                        ldapuser.Cn = attributeSet.getAttribute("cn")?.StringValue ?? "";
                        ldapuser.Firstname = attributeSet.getAttribute("givenname")?.StringValue ?? "";
                        ldapuser.LastName = attributeSet.getAttribute("sn")?.StringValue ?? "";
                        ldapuser.UserPrincipalName = attributeSet.getAttribute("userPrincipalName")?.StringValue ?? "";
                        ldapuser.St = attributeSet.getAttribute("st")?.StringValue ?? "";
                        ldapuser.Samaccountname = attributeSet.getAttribute("samaccountname")?.StringValue ?? "";
                        ldapuser.Description = attributeSet.getAttribute("description")?.StringValue ?? "";
                        ldapuser.Telephonenumber = attributeSet.getAttribute("telephonenumber")?.StringValue ?? "";
                        ldapuser.Department = attributeSet.getAttribute("department")?.StringValue ?? "";
                        ldapuser.Displayname = attributeSet.getAttribute("displayname")?.StringValue ?? "";
                        ldapuser.Mail = attributeSet.getAttribute("mail")?.StringValue ?? "";
                        ldapuser.GivenName = attributeSet.getAttribute("givenName")?.StringValue ?? "";
                        //};

                        return true;
                    }
                }

                ldapuser = null;
                return false;
            }
            catch
            {
                ldapuser = null;
                return false;
            }
        }
    }
}
