﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankQuestionType
    {
        public int ID { get; set; }
        public string QUESTION_TYPE { get; set; }

        public List<BankQuestionTitle> QuestionTitles { get; set; }

        public bool IsActive { get; set; }
    }
}
