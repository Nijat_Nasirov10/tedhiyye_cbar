﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class ExchangeRate
    {
        public string CurrencyName { get; set; }
        public int CurrencyId { get; set; }
        public decimal Rate { get; set; }
    }
}
