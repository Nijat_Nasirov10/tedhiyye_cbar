﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class DbResult<T>
    {
        public bool Success { get; set; }
        public T Value { get; set; }
        public string ErrorMessage { get; set; }
    }
}
