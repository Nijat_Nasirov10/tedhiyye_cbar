﻿using Microsoft.AspNetCore.Mvc.Rendering;
using tedhiyye_portal_shared.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
//using System.Web.Mvc;

namespace insurance_system_cbar.ViewModels.RealSector.Questionnaire
{
    public class FormVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CompanyType { get; set; }
        public int OrderId { get; set; }
    }

    public class CreateEditFormVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [DisplayName("Sorğu")]
        public string Name { get; set; }

        [DisplayName("Sıralanma")]
        public int? OrderId { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [DisplayName("Fəaliyyət növü")]
        public int? CompanyTypeId { get; set; }

        public IEnumerable<SelectListItem> CompanyTypes { get; set; }
    }
}