#pragma checksum "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "54d718c61cfc4d2271574f4ae10edd0f4c058aeb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Messages_Compose), @"mvc.1.0.view", @"/Views/Messages/Compose.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared.Domains;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
using insurance_system_shared;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"54d718c61cfc4d2271574f4ae10edd0f4c058aeb", @"/Views/Messages/Compose.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7403705723c95d1543623efd76ed381e059f9cb9", @"/Views/_ViewImports.cshtml")]
    public class Views_Messages_Compose : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ComposeMessageVM>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
  
    ViewBag.Title = "Bildiriş yaz";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n\r\n");
            DefineSection("Currentpagelink", async() => {
                WriteLiteral("\r\n    <li><a href=\"javascript:void(0)\">Bildirişlər</a></li>\r\n    <li><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></li>\r\n    <li>\r\n        ");
#nullable restore
#line 15 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
   Write(Html.ActionLink("Bildiriş yaz", "Compose", "Messages"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </li>\r\n");
            }
            );
            WriteLiteral("\r\n<style>\r\n    input, select, textarea {\r\n        max-width: 100%;\r\n    }\r\n</style>\r\n<div class=\"col-md-12 esahs-container-padd\">\r\n    <h1>");
#nullable restore
#line 25 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
   Write(ViewBag.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n");
#nullable restore
#line 26 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
      
        if (TempData["SuccessMessage"] != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-success\" role=\"alert\">\r\n                ");
#nullable restore
#line 30 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(TempData["SuccessMessage"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n");
#nullable restore
#line 32 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
        }
        if (TempData["ErrorMessage"] != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-danger\" role=\"alert\">\r\n                ");
#nullable restore
#line 36 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(TempData["ErrorMessage"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n");
#nullable restore
#line 38 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
        }
        if (ViewBag.SuccessMessage != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-success\" role=\"alert\">\r\n                ");
#nullable restore
#line 42 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(ViewBag.SuccessMessage);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n");
#nullable restore
#line 44 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
        }
        if (ViewBag.ErrorMessage != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-danger\" role=\"alert\">\r\n                ");
#nullable restore
#line 48 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(ViewBag.ErrorMessage);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n");
#nullable restore
#line 50 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
        }
    

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 53 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
     using (Html.BeginForm())
    {
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 55 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
   Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"form-horizontal\">\r\n            ");
#nullable restore
#line 58 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
       Write(Html.ValidationSummary(true, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            <div class=\"form-group\">\r\n                ");
#nullable restore
#line 60 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(Html.LabelFor(model => model.DepartmentId, htmlAttributes: new { @class = "control-label col-md-2" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-md-4\">\r\n                    ");
#nullable restore
#line 62 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
               Write(Html.DropDownListFor(model => model.DepartmentId, (IEnumerable<SelectListItem>)ViewBag.DepartmentList, "---", new { @class = "form-control select2" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 63 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
               Write(Html.ValidationMessageFor(model => model.DepartmentId, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                ");
#nullable restore
#line 68 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(Html.LabelFor(model => model.Subject, htmlAttributes: new { @class = "control-label col-md-2" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-md-6\">\r\n                    ");
#nullable restore
#line 70 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
               Write(Html.EditorFor(model => model.Subject, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 71 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
               Write(Html.ValidationMessageFor(model => model.Subject, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                ");
#nullable restore
#line 76 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
           Write(Html.LabelFor(model => model.Body, htmlAttributes: new { @class = "control-label col-md-2" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-md-6\">\r\n                    ");
#nullable restore
#line 78 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
               Write(Html.EditorFor(model => model.Body, new { htmlAttributes = new { @class = "form-control", @rows = "5" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 79 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
               Write(Html.ValidationMessageFor(model => model.Body, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </div>
            </div>

            <div class=""form-group"">
                <div class=""col-md-offset-2 col-md-6"">
                    <input type=""submit"" value=""Göndər"" class=""btn btn-default"" />
                </div>
            </div>
        </div>
");
#nullable restore
#line 89 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Messages\Compose.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ComposeMessageVM> Html { get; private set; }
    }
}
#pragma warning restore 1591
