﻿namespace tedhiyye_portal_shared.Domains
{
    public class BankType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
