#pragma checksum "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "afb11369e7a630ad2133f4010bad1fe2e47a94bd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Error), @"mvc.1.0.view", @"/Views/Home/Error.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_cbar.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared.Domains;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\_ViewImports.cshtml"
using tedhiyye_portal_shared.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"afb11369e7a630ad2133f4010bad1fe2e47a94bd", @"/Views/Home/Error.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7403705723c95d1543623efd76ed381e059f9cb9", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Error : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n﻿");
#nullable restore
#line 2 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml"
   
    ViewBag.Title = "Xəta";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            DefineSection("Currentpagelink", async() => {
                WriteLiteral("\r\n    <li>\r\n        ");
#nullable restore
#line 9 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml"
   Write(Html.ActionLink("Xəta", "Error"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </li>\r\n");
            }
            );
            WriteLiteral("\r\n<style>\r\n    ");
            WriteLiteral("@media (min-width: 1250px) {\r\n        #divActionLogs .modal-dialog {\r\n            width: 1200px;\r\n        }\r\n    }\r\n\r\n    ");
            WriteLiteral(@"@media (min-width: 1350px) {
        #divActionLogs .modal-dialog {
            width: 1300px;
        }
    }

    .selected-row {
        background-color: #efefef;
    }

    .unread {
        font-weight: bold;
    }
</style>

<div class=""col-md-12 esahs-container-padd"">
    <h1>");
#nullable restore
#line 36 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml"
   Write(ViewBag.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n");
            WriteLiteral("        <h2><b>Sistem xətası baş verdi:</b></h2>\r\n        <h3><b>Zəhmət olmasa daha sonra təkrar yoxlayın.</b></h3>\r\n        <br />\r\n");
#nullable restore
#line 41 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml"

        if (ViewBag.ErrorMessage != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-danger\" role=\"alert\">\r\n                ");
#nullable restore
#line 45 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml"
           Write(ViewBag.ErrorMessage);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n");
#nullable restore
#line 47 "C:\Users\User\source\repos\Tedhiyye_Portal_System\tedhiyye_portal_cbar\Views\Home\Error.cshtml"
        }
    

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n    <script>\r\n    </script>\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
