﻿$(document).ready(function () {
    var url_company_table = $("#url-company").data('table');
    var url_company_detail = $("#url-company").data('detail');
    var url_company_edit = $("#url-company").data('edit');
    var url_company_delete = $("#url-company").data('delete');
    var url_company_users = $("#url-company").data('users');

    var companyOTable = $('#companies_datatable').DataTable({
        "ajax": {
            "url": url_company_table,
            "type": "get",
            "datatype": "json"
        },
        "initComplete": function (settings, json) {
            InitSearch();
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [{ "orderable": false, "targets": [2, 3, 4] }],

        "columns": [
            { "data": "Name" },
            { "data": "Code", "width": "100px" },
            { "data": "CompanyClass", "width": "250px" },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="edit btn btn-primary btn-block" href="' + url_company_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>';
                },
                "width": "30px"
            },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="delete btn btn-danger btn-block" data-id="' + data + '" href="' + url_company_delete + '"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                "width": "30px"
            }
        ]
    });

    $('#companies_datatable tbody').on('click', 'td', function (e) {
        if (!$(this).has('a, input, button').length) {
            var tr = $(this).closest('tr');

            if (tr.hasClass('selected')) {
                tr.removeClass('selected');
                ClearChildContent();
            }
            else {
                companyOTable.$('tr.selected').removeClass('selected');
                tr.addClass('selected');

                var detail_url = url_company_detail + '/' + companyOTable.row(tr).data().Id;
                var users_url = url_company_users + '?companyId=' + companyOTable.row(tr).data().Id;
                LoadChilds(detail_url, users_url)
            }
        }
    });

    function LoadChilds(detail_url,users_url) {
        $.when($.get(detail_url), $.get(users_url)).done(function (detail, users) {
            $("#company-detail").html(detail[0]);
            $("#users-holder").html(users[0]);
            $(document).ready(OnLoadUsers);
        });
    }

    $('.company-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpenCompanyPopup($(this).attr('href'));    
    })

    $('.company-table-container').on('click', 'a.delete', function (e) {
        e.preventDefault();
        if (confirm("Məlumat ləğv edilsin?")) {
            e.preventDefault();

            $.post($(this).attr('href'), { companyId: $(this).attr('data-id') }, function (data) {
                companyOTable.ajax.reload(null, false);
                ClearChildContent();
            });
        }
        else {
            return false;
        }
    })

    function ClearChildContent() {
        $("#company-detail").html('');
        $("#users-holder").html('');
    }
    var companyFormId = '#company-content';
    function OpenCompanyPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $(companyFormId, $pageContent).removeData('validator');
            $(companyFormId, $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });

        $(".dialog").dialog("close");
        $dialog_company = $('<div class="dialog-company dialog"></div>')
                  .html($pageContent)
                  .dialog({
                      draggable: dialog_options.draggable,
                      autoOpen: dialog_options.autoOpen,
                      resizable: dialog_options.resizable,
                      modal: dialog_options.modal,
                      height: dialog_options.height,
                      width: 700,
                      position: ['top', 30],
                      title: 'Şirkət',
                      buttons: {
                          "Yadda saxla": function () {
                              if (!$(companyFormId).valid()) {
                                  return false;
                              }
                              var url = $(companyFormId)[0].action;
                              $.ajax({
                                  type: "POST",
                                  url: url,
                                  data: $(companyFormId).serialize(),
                                  success: function (data) {
                                      if (data.status) {
                                          $dialog_company.dialog('destroy').remove();
                                          companyOTable.ajax.reload(null, false);
                                          ClearChildContent();
                                      }
                                  }
                              })
                          },
                          "Cancel": function () {
                              $dialog_company.dialog('destroy').remove();
                          }
                      }
                  })

        $dialog_company.dialog('open');
    }

    function InitSearch() {


        $('#companies_datatable thead tr#filterrow th#filter-input').each(function () {
            $(this).html('<input type="text" class="form-control" style="width:100%" onclick="stopPropagation(event);" placeholder="Axtar..." />');
        });

        $('#companies_datatable thead tr#filterrow th#filter-select').each(function () {
            var select = $('<select id="company_type_select" class="form-control select2" style="width:100%!important" onclick="stopPropagation(event);"><option value="">Hamısı</option></select>')
                .appendTo($(this).empty())
                .on('change', function (event) {                    
                    companyOTable.column(2)
                        .search($(this).val())
                        .draw();                   
                    ClearChildContent();
                });

            $(".select2").select2({
                theme: "bootstrap"
            });

            companyOTable.column(2).data().each(function (value, index) {
                if ($("#company_type_select option[value='" + value + "']").length === 0) {
                    select.append('<option value="' + value + '">' + value + '</option>')
                }
            });
        });

        
        // Инициализация самой таблицы с default сортировкой
        $("#companies_datatable thead input").on('keyup change', function () {
            companyOTable
                .column($(this).parent().index() + ':visible')
                .search(this.value)
                .draw();
            ClearChildContent();
        });

        

    }
})
