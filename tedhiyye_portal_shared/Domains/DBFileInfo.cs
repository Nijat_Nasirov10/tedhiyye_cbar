﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class DBFileInfo
    {
        public string UPLOAD_FILE_NAME { get; set; }
        public byte[] UPLOAD_FILE_DATA { get; set; }
        public string UPLOAD_FILE_CONTENT_TYPE { get; set; }
        public int UPLOAD_USER_ID { get; set; }
        public bool NotFound { get; set; }
    }
}
