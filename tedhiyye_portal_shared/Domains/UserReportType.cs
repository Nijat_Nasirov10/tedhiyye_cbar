﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class UserReportType
    {
        public int ReportTypeId { get; set; }
        public int SubstituteUserId { get; set; }
        public int CuratorUserId { get; set; }
    }
}
