﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("FORMS")]
    public class Form
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("FORM")]
        public string Name { get; set; }

        [Column("COMPANYTYPEID")]
        public int CompanyTypeId { get; set; }

        [Column("ORDERID")]
        public int OrderId { get; set; }

        public bool IsComplete { get; set; }

        public CompanyType CompanyType { get; set; }
        public List<Question> Questions { get; set; }
    }
}