﻿function OnLoadAnswers() {
    var url_answer_table = $("#url-answer").data('table');
    var url_answer_edit = $("#url-answer").data('edit');
    var url_answer_delete = $("#url-answer").data('delete');

    var $answers_table = $("#answers_datatable")
    var questionId = $answers_table.data('questionid');
    var answerOTable = $answers_table.DataTable({
        "ajax": {
            "url": url_answer_table + '?questionId=' + questionId,
            "type": "get",
            "datatype": "json"
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [{ "orderable": false, "targets": [3, 4] }],
        "columns": [
            { "data": "Answer" },
            { "data": "Point", "width": "30px" },
            { "data": "OrderId", "width": "30px" },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="edit btn btn-primary btn-block" href="' + url_answer_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>';
                },
                "width": "30px"
            },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="delete btn btn-danger btn-block" data-id="' + data + '" href="' + url_answer_delete + '"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                "width": "30px"
            }
        ]
    })

    $('.answer-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpenAnswerPopup($(this).attr('href'));
    })

    $('.answer-table-container').on('click', 'a.delete', function (e) {
        e.preventDefault();
        if (confirm("Məlumat ləğv edilsin?")) {
            e.preventDefault();

            $.post($(this).attr('href'), { answerId: $(this).attr('data-id') }, function (data) {
                answerOTable.ajax.reload(null, false);
            });
        }
        else {
            return false;
        }
    })

    var answerFormId = '#answer-content';
    function OpenAnswerPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $(answerFormId, $pageContent).removeData('validator');
            $(answerFormId, $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });

        $(".dialog").dialog("close");
        $dialog = $('<div class="dialog-answer dialog"></div>')
                  .html($pageContent)
                  .dialog({
                      draggable: dialog_options.draggable,
                      autoOpen: dialog_options.autoOpen,
                      resizable: dialog_options.resizable,
                      modal: dialog_options.modal,
                      height: dialog_options.height,
                      width: dialog_options.width,
                      position: dialog_options.position,
                      title: 'Cavab',
                      buttons: {
                          "Yadda saxla": function () {
                              if (!$(answerFormId).valid()) {
                                  return false;
                              }
                              var url = $(answerFormId)[0].action;
                              $.ajax({
                                  type: "POST",
                                  url: url,
                                  data: $(answerFormId).serialize(),
                                  success: function (data) {
                                      if (data !== null) {
                                          $dialog.dialog('destroy').remove();
                                          answerOTable.ajax.reload(null, false);
                                      }
                                  }
                              })
                          },
                          "Cancel": function () {
                              $dialog.dialog('destroy').remove();
                          }
                      }
                  })

        $dialog.dialog('open');
    }
}
