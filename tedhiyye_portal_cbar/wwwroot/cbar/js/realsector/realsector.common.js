﻿
var datatables_options =
    {
        "processing": true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Hamısı"]],
        "language": {
            "paginate": {
                "first": "İlk",
                "last": "Son",
                "next": "»",
                "previous": "«"
            },
            "processing": "Yüklənir...",
            "lengthMenu": 'Səhifədə <select class= "form-control" style="text-align-last:center;">' +
                '<option value="10">10</option>' +
                '<option value="25">25</option>' +
                '<option value="50">50</option>' +
                '<option value="-1">Hamısı</option>' +
                '</select> yazını göstərmək',
            "zeroRecords": "Məlumat yoxdur",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "Məlumat yoxdur",
            "infoFiltered": "(filtered from _MAX_ total records)"
        },
        "dom": "<'row'<'col-sm-12'tr>> <'row'<'col-sm-4'l><'col-sm-8'p>>",
    };

var dialog_options =
    {
        "draggable": true,
        "autoOpen": false,
        "resizable": false,
        "modal": true,
        "height": 'auto',
        "width": 600,
        "position": ['center', 150]
    };

function goToByScroll(id) {

    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    },
        400);
}

function stopPropagation(event) {
    if (event.stopPropagation !== undefined) {
        event.stopPropagation();
    } else {
        event.cancelBubble = true;
    }
}

