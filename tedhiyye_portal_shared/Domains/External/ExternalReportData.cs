﻿using tedhiyye_portal_shared.Domains;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace tedhiyye_portal_shared.Domains.External
{

    public class ExternalReportData
    {
        public int ReportFileTypeId { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public DataSet DataSet { get; set; }
        //public List<ExternalReportTypeData> ReportTypeData { get; set; }
    }

    //public class ExternalReportTypeData
    //{
    //    public string TableName { get; set; }
    //    public List<dynamic> Rows { get; set; }
    //}

    public class ExternalReportStructure
    {
        public int ReprortFileTypeId { get; set; }
        public List<ExternalReportTable> ReportTypeTables { get; set; }
    }

    public class ExternalReportTable
    {
        public string TableName { get; set; }
        public int? StartRowIndex { get; set; }
        public int? EndRowIndex { get; set; }
        public Dictionary<string, string> TableColumnWithType { get; set; }
    }

}