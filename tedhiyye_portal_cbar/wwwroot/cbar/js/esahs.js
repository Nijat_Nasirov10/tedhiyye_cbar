$(function () {
    $("input:file[id=uploadBtn]").change(function () {

        $("#uploadFile").val($(this).val());

        $("span.uploadBtn").text("upload");

    });

    var loc = location.pathname;
    var ar = location.pathname.split('/');
    if (ar.length > 2) {
        loc = '/' + ar[1] + '/' + ar[2];
    }
    // subfolder deploy
    if (ar.length > 3) {
        loc += '/' + ar[3];
    }
    // subfolder deploy param (/cbar/Info/Downloads/2)
    if (ar.length > 4) {
        loc += '/' + ar[4];
    }
    if (loc == '/Home/Index')
        loc = '/';
    loc = loc.replace('RatesDaily', 'RatesMonthly');
    //console.log(loc);
    $('a[href="' + loc + '"]').children().css('color', '#71cde8');
    var esahs_li = $('a[href="' + loc + '"]').closest('.esahs-li')
    esahs_li.find('.esahs-a').addClass('esahs-active');
    esahs_li.find('.esahs-dis').css('display', 'block');

    /*var table = $('#wrapper').DataTable({
        scrollY: 400,
        scrollCollapse: true,
        scrollX: true,
        paging: false,
        fixedColumns: true
    });*/


    $('.Show').click(function () {
        if ($(".wrapp_div").css('display') == 'none') {
            $(".wrapp_div").css('display', 'block');
            table.draw()
        }

    })

    /*$('.datepicker').datepicker({
        format: "dd-mm-yyyy",
    });*/



});

$('.esahs-active').next().slideDown(0, function () {
});
$('.esah-active').children('span').children('.fa-angle-left').css('transform', 'rotate(-90deg)')


$('.esahs-a').click(function (e) {
    if ($(this).next().css('display') == 'none') {
        $('.esahs-a').next().slideUp(function () {
        });
        $('.esahs-a').removeClass('esahs-active')
        $(this).next().slideDown(function () {
        });
        $(this).addClass('esahs-active')
    }
    else {
        $(this).next().slideUp(function () {
        });
        $(this).removeClass('esahs-active')
    }
});

/* Tabs */

$('#1').css('display', 'block');
$('.tab').children('li').first().children('a').addClass('active-tab');

$('.tablinks').click(function () {
    var tabName = $(this)[0].name
    $('.tablinks').removeClass('active-tab')
    $('.tabcontent').css('display', 'none')
    $(this).addClass('active-tab')
    $('#' + tabName).css('display', 'block')
});

var minHeight = (document.documentElement.clientHeight);
$('.admin-item').css('min-height', minHeight - 115)




var geocoder;
var map;
function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var mapOptions = {
        zoom: 8,
        center: latlng
    }
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
}

function codeAddress() {
    var address = document.getElementById('address').value;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
//map
function initMap() {
    // Create a map object and specify the DOM element for display.
    var opts = { 'center': new google.maps.LatLng(40.3784410644675, 49.8448982834816), 'zoom': 15, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
    map = new google.maps.Map(document.getElementById('map'), opts);

    google.maps.event.addListener(map, 'click', function (event) {
        document.getElementById('latlongclicked').value = event.latLng.lat()
        document.getElementById('lotlongclicked').value = event.latLng.lng()
    });
}
$(".clickable-row").click(function () {
    window.document.location = $(this).data("href");
});
$(".select2").select2({
    theme: "bootstrap"
});

$(".js-programmatic-multi-set-val").on("click", function () { $(".js-example-basic-multiple").val(["multi"]).trigger("change"); });
$(".js-programmatic-multi-clear").on("click", function () { $(".js-example-basic-multiple").val(null).trigger("change"); });

$(".js-programmatic-multi-set-val_bank").on("click", function () { $(".js-example-basic-multiple_bank").val(["bank"]).trigger("change"); });
$(".js-programmatic-multi-clear_bank").on("click", function () { $(".js-example-basic-multiple_bank").val(null).trigger("change"); });

//info
$(".info-h1").click(function () {
    if ($(this).next().css('display') == 'none') {
        $(this).next().slideDown(function () {

        });
    }
    else {
        $(this).next().slideUp(function () {
        });
    }
});


$('div.info-item-list').click(function () {
    if ($(this).next().css('display') == 'none') {
        $(this).next().slideDown(function () {
        });
    }
    else {
        $(this).next().slideUp(function () {
        });
    }
});

$(".bt-grey").click(function () {

    var el = $(this).next();
    $(".ellipsis").not(el).css('display', 'none')
    if (el.css('display') == 'none') {
        el.css('display', 'block')
    }
    else {
        el.css('display', 'none')
    }

})

$(".bt-red").click(function () {
    $(".history-row").css("display", "block")
})
$(".clos").click(function () {
    $(".history-row").css("display", "none")
})
