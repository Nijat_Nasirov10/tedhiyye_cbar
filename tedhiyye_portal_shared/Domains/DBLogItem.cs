﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class DBLogItem
    {
        public int ID { get; set; }
        public string ORGANIZATION { get; set; }
        public string USERNAME { get; set; }
        public string REPORT_ID { get; set; }
        public string ACTION { get; set; }
        public string DATA { get; set; }
        public DateTime LOG_TIME { get; set; }
        public string LOG_TYPE { get; set; }
        public string IP { get; set; }

        public string ReportBankName { get; set; }
        public string BANK { get; set; }
        public string ReportTypeName { get; set; }
        public string REPORT_FILE_TYPE_NAME { get; set; }
        public DateTime ReportPeriodStart { get; set; }
        public DateTime PERIODSTART { get; set; }
        public DateTime PERIODEND { get; set; }
        public DateTime ReportPeriodEnd { get; set; }
    }
}
