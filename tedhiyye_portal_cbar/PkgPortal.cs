﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar
{
    public static class PkgPortal
    {
        private static readonly string _schema = "statport";
        private static readonly string _package = "pkg_portal_reports";

        private static string FullMethodName(string methodName, string package = null, string schema = null)
        {
            return $"{schema ?? _schema}.{package ?? _package}.{methodName}";
        }
    }
}
