﻿
using tedhiyye_portal_shared.Domains.RealSector.DatabaseContext;
using tedhiyye_portal_shared.Domains.RealSector.Models;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;

namespace tedhiyye_portal_shared.Domains.RealSector.Services
{
	public class DownloadFileService : BaseService
	{
		public DownloadFileService(ApplicationContext context)
			: base(context)
		{
		}

		public IEnumerable<Download> GetDownloads(int companyTypeId, int groupId)
		{
			var reuslt = from download in _context.Downloads
						 where download.CompanyTypes.Any(x => x.Id == companyTypeId) && download.GroupId == groupId
						 select download;
			return reuslt;
		}
	}
}
