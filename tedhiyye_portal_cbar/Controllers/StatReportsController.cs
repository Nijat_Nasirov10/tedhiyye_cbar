﻿using tedhiyye_portal_cbar.Models;
using tedhiyye_portal_cbar.ViewModels;
using tedhiyye_portal_shared;
using tedhiyye_portal_shared.Domains;
using tedhiyye_portal_shared.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using tedhiyye_portal_cbar;

namespace tedhiyye_portal_cbar.Controllers
{
    [DisableRequestSizeLimit]
    public class StatReportsController : Controller
    {

        [HttpGet]
        public ActionResult Index(string[] bankType, int[] bank_ids, string yearmonth = "", string days = "", string status = "", string report_file_type_id = "")
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            int? userId = HttpContext.Session.GetInt32("UserId") as int?;

            ViewBag.ReportFileTypeList = DBCBAR.GetReportFileTypeList(userId);

            SetFilterViewBags(bankType, bank_ids);

            if (!IfParamsIsEmpty(ref yearmonth, bank_ids, days))
                return Redirect(String.Format("~/StatReports?yearmonth={1}&days=", bankType, yearmonth));

            ViewBag.YearMonthFilterValidation = "";
            if (HttpContext.Request.Query["yearmonth"].Count != 0)
            {
                if (HttpContext.Request.Query["yearmonth"].ToString() == "")
                    ViewBag.YearMonthFilterValidation = "İl-Ay seçilməlidir";
            }

            var Reports = new List<Report>();
            ViewBag.ShowTable = true;
            if (yearmonth != "")
            {
                string[] parts = yearmonth.Split('-');
                int year = Convert.ToInt32(parts[0]);
                int month = Convert.ToInt32(parts[1]);
                int daysCount = Convert.ToInt32(days);

                var DaysListinCurrentMonth = new List<SelectListItem>();

                for (int i = 1; i <= DateTime.DaysInMonth(year, month); i++)
                {
                    DaysListinCurrentMonth.Add(new SelectListItem { Text = "" + i, Value = "" + i });
                }

                ViewBag.DaysListinCurrentMonth = DaysListinCurrentMonth;

                Reports = DBCBAR.GetReports(ConvertToBankTypeFilter(bankType), bank_ids, userId.Value, year, month, days, status, report_file_type_id);
                ViewBag.CheckAllVisible = true;
            }
            else
            {
                ViewBag.ShowTable = false;
                ViewBag.CheckAllVisible = false;
            }
            ViewBag.Reports = Reports;

            if (bank_ids == null || bank_ids.Length == 0)
                ViewBag.CheckAllDisabled = true;
            else
            {
                var PeriodEndDates = Tools.GetYearMonthEndDates(yearmonth, days);
                ViewBag.PeriodEndDates = String.Join(",", PeriodEndDates.Select(d => d.ToString("yyyy-MM-dd")));
                ViewBag.CheckAllDisabled = false;
            }

            ActionVM actionVM = new ActionVM();
            return View(actionVM);
        }

        public ReportActionMessageVM DoReportOperation(string tbId, string tbAction)
        {
            ReportActionMessageVM message = new ReportActionMessageVM();
            string success = "";
            string error = "";
            List<int> idsWithError = new List<int>();
            List<int> idsWithSimilar = new List<int>();

            if (tbAction == "confirmascorrect")
            {
                //var report = reportsInGroup.FirstOrDefault(x => x.ID == actionVM.Id);
                var similarReportExist = DBCBAR.CheckConfirmedAsCorrectSimilarReports(Convert.ToInt32(tbId));
                if (similarReportExist)
                {
                    error = "Səhv! Hesabat düz kimi qəbul edilmədi";
                    error += ". ID " + tbId + " hesabatı ilə eyni tip, period və statistik vahidə aid başqa düzdür olan hesabat var.";
                    DBLog.AddError("ConfirmAsCorrectReport", HttpContext, tbId, "has similar other correct report");
                }
                else
                {
                    bool p_result;
                    string p_result_message;
                    p_result = DBCBAR.ConfirmAsCorrect(Convert.ToInt32(tbId), out p_result, out p_result_message);
                    if (p_result)
                    {
                        success = p_result_message;
                        DBLog.AddInfo("ConfirmAsCorrectReport", HttpContext, tbId);
                    }
                    else
                    {
                        error = "Səhv! hesabat düz kimi qəbul edilmədi";
                        DBLog.AddError("ConfirmAsCorrectReport", HttpContext, tbId);
                    }
                }

                message.Error = error;
                message.Success = success;
                return message;

            }
            else if (tbAction == "confirm")
            {
                //var report = reportsInGroup.FirstOrDefault(x => x.ID == actionVM.Id);
                var similarReportExist = DBCBAR.CheckConfirmedAsCorrectSimilarReports(Convert.ToInt32(tbId));
                if (similarReportExist)
                {
                    error = "Səhv! Hesabat düz kimi qəbul edilmədi";
                    error += ". ID " + tbId + " hesabatı ilə eyni tip, period və statistik vahidə aid başqa düzdür olan hesabat var.";
                    DBLog.AddError("ConfirmAsCorrectReport", HttpContext, tbId, "has similar other correct report");
                }
                else
                {
                    bool p_result;
                    string p_result_message;
                    p_result = DBCBAR.ConfirmCorrect(Convert.ToInt32(tbId), out p_result, out p_result_message);
                    if (p_result)
                    {
                        success = p_result_message;
                        DBLog.AddInfo("ConfirmAsCorrectReport", HttpContext, tbId);
                    }
                    else
                    {
                        error = "Səhv! hesabat düz kimi qəbul edilmədi";
                        DBLog.AddError("ConfirmAsCorrectReport", HttpContext, tbId);
                    }
                }

                message.Error = error;
                message.Success = success;
                return message;
            }
            else if (tbAction == "cancel")
            {

                if (idsWithError.Count > 0)
                {
                    error = "Səhv! ID " + String.Join(", ", idsWithError) + " hesabat(lar)ından imtina alınmadı";
                }
                else
                {
                    bool p_result;
                    string p_result_message;
                    p_result = DBCBAR.CancelReport(Convert.ToInt32(tbId), out p_result, out p_result_message);
                    if (p_result)
                    {
                        success = p_result_message;
                    }
                    else
                    {
                        error = "Səhv! hesabat düz kimi qəbul edilmədi";
                    }
                }
                message.Error = error;
                message.Success = success;
                return message;
            }
            else
            {
                message.Error = "";
                message.Success = "";
                return message;
            }
        }

        public ActionResult PerformActionReport(string tbId, string tbAction)
        {
            var resultMessage = DoReportOperation(tbId, tbAction);
            TempData["ErrorMessage"] = resultMessage.Error;
            TempData["SuccessMessage"] = resultMessage.Success;
            return View();
        }

        [HttpPost]
        public ActionResult Index(ActionVM actionVM, string[] bankType, int[] bank_ids, string yearmonth = "", string days = "", string status = "")
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            SetFilterViewBags(bankType, bank_ids);

            if (Tools.IsLunchBreak(DateTime.Now))
            {
                DBLog.AddError("ChangeReportStatus", HttpContext, actionVM.Id.ToString(), "cannot change report status in lunch break");
                TempData["ErrorMessage"] = "Səhv! Nahar fasiləsində (13:00-14:00) status dəyişikliyi etmək olmaz.";

                return Redirect(HttpContext.Request.Path + HttpContext.Request.QueryString);
            }

            List<int> idsWithError = new List<int>();
            List<int> idsWithSimilar = new List<int>();


            if (actionVM.Action == "confirmascorrect")
            {
                var resultMessage = DoReportOperation(actionVM.Id.ToString(), actionVM.Action);
                TempData["ErrorMessage"] = resultMessage.Error;
                TempData["SuccessMessage"] = resultMessage.Success;

                return Redirect(HttpContext.Request.Path + HttpContext.Request.QueryString);
            }
            else if (actionVM.Action == "cancel")
            {

                var resultMessage = DoReportOperation(actionVM.Id.ToString(), actionVM.Action);
                TempData["ErrorMessage"] = resultMessage.Error;
                TempData["SuccessMessage"] = resultMessage.Success;

                return Redirect(HttpContext.Request.Path + HttpContext.Request.QueryString);
            }
            else
            {
                return Redirect(HttpContext.Request.Path + HttpContext.Request.QueryString);
            }
        }

        [HttpGet]
        public ActionResult UploadReport(string[] bank_types, string BankId)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new
                {
                    ReturnUrl = HttpContext.Request.Path + HttpContext.Request.QueryString
                });

            int? userId = HttpContext.Session.GetInt32("UserId") as int?;

            int bankId = String.IsNullOrEmpty(BankId) ? -1 : Convert.ToInt32(BankId);

            int[] bank_ids = { bankId };
            ViewBag.ReportFileTypeList = DBCBAR.GetReportFileTypeList(userId);
            ViewBag.RecentUploadedReportFiles = DBCBAR.GetRecentUploadedReportFiles(userId.Value, bankId);
            ViewBag.ReportVersions = DBCBAR.GetReportVersions();

            SetFilterViewBags(bank_types);

            var bankVM = new ReportUploadVM();
            return View(bankVM);
        }

        [HttpPost]
        public ActionResult UploadReport(ReportUploadVM reportUploadVM, IFormFile file)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = HttpContext.Request.Path + HttpContext.Request.QueryString });

            int userId = (int)HttpContext.Session.GetInt32("UserId");
            bool HasError = false;
            List<string> errors = new List<string>();


            DateTime periodstartdate = DateTime.Now;

            if (!reportUploadVM.BankId.HasValue)
            {
                errors.Add("Statistik vahid seçilməyib");
                HasError = true;
            }
            try
            {
                periodstartdate = DateTime.ParseExact(reportUploadVM.periodstart, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                errors.Add("Hesabat başlama dövrü düzgün deyil");
                HasError = true;
            }
            DateTime periodenddate = DateTime.Now;
            try
            {
                periodenddate = DateTime.ParseExact(reportUploadVM.periodend, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                errors.Add("Hesabat bitmə dövrü düzgün deyil");
                HasError = true;
            }

            if (periodstartdate > periodenddate)
            {
                errors.Add("Hesabat dövrü düzgün deyil, başlama tarixi bitmə tarixindən sonradır.");
                HasError = true;
            }
            bool p_result;
            string p_result_message;

            int bankID = reportUploadVM.BankId.HasValue ? reportUploadVM.BankId.Value : -1;
            //int isPhqe = reportUploadVM.ShowPhqe ? 1 : 0;
            int isPhqe = reportUploadVM.ReportVersionId;

            p_result = DBCBAR.PeriodDateFileExists(bankID, (int)reportUploadVM.report_file_type_id.Value, isPhqe, periodenddate, out p_result, out p_result_message);
            if (p_result)
            {
                errors.Add(p_result_message);
                HasError = true;
            }

            if (reportUploadVM.BankId.HasValue)
            {
                if (!DBCBAR.IsBankHaveAccessForReportFileType(reportUploadVM.BankId, reportUploadVM.report_file_type_id))
                {
                    errors.Add("Bu hesabat seçilmiş statistik vahid üçün yüklənə bilməz!");
                    HasError = true;
                }
            }


            if (reportUploadVM.report_file_type_id == -1)
            {
                errors.Add("Hesabat tipi seçilməyib");
                HasError = true;
            }
            if (reportUploadVM.report_file_type_id != -1)
            {
                var pt = DBCBAR.GetPeriodTypeByReportFileType(reportUploadVM.report_file_type_id.Value);
                var dateDiff = (periodenddate - periodstartdate).TotalDays;

                switch (pt.ReportPeriodType)
                {
                    case PeriodType.Unknown:
                        errors.Add("Bu hesabat tipinə uyğun period mövcud deyil!");
                        HasError = true;
                        break;
                    case PeriodType.M:
                        var lastDayOfMonth = DateTime.DaysInMonth(periodenddate.Year, periodenddate.Month);
                        if (!(periodstartdate.Day == 1 && periodenddate.Month == periodenddate.Month && periodenddate.Day == lastDayOfMonth))
                        {
                            errors.Add("Hesabat dövrü hesabat tipinə uyğun deyil!");
                            HasError = true;
                        }
                        break;
                    case PeriodType.HM:
                        if (!(dateDiff <= 15 && dateDiff >= 12))
                        {
                            errors.Add("Hesabat dövrü hesabat tipinə uyğun deyil!");
                            HasError = true;
                        }
                        break;
                    case PeriodType.Q:
                        var quat = (periodstartdate.AddMonths(3).AddDays(-1)).DayOfYear;
                        if (!(periodstartdate.Day == 1 && periodenddate.DayOfYear == quat))
                        {
                            errors.Add("Hesabat dövrü hesabat tipinə uyğun deyil!");
                            HasError = true;
                        }
                        break;
                    case PeriodType.W:
                        if (!(dateDiff <= 6 && dateDiff >= 4))
                        {
                            errors.Add("Hesabat dövrü hesabat tipinə uyğun deyil!");
                            HasError = true;
                        }
                        break;
                    case PeriodType.HD:
                        if (!(dateDiff == 0))
                        {
                            errors.Add("Hesabat dövrü hesabat tipinə uyğun deyil!");
                            HasError = true;
                        }
                        break;
                    case PeriodType.D:
                        if (!(dateDiff == 0))
                        {
                            errors.Add("Hesabat dövrü hesabat tipinə uyğun deyil!");
                            HasError = true;
                        }
                        break;
                }
            }
            if (file == null)
            {
                errors.Add("Fayl seçilməyib");
                HasError = true;
            }
            else
            if (file.Length == 0)
            {
                errors.Add("Fayl boşdur");
                HasError = true;
            }
            else
            if (!file.FileName.ToLower().EndsWith(".xlsx") && !file.FileName.ToLower().EndsWith(".xls") && !file.FileName.ToLower().EndsWith(".xlsm") && !file.FileName.ToLower().EndsWith(".xml"))
            {
                errors.Add("Yalnız .xlsx, .xlsm və ya .xls genişləməli Excel faylını və yaxud XML faylı yükləmək olar");
                HasError = true;
            }
            else
            {
                int MaxUploadFileSizeMB = Convert.ToInt32("30");
                int MaxUploadFileSizeB = MaxUploadFileSizeMB * 1024 * 1024;

                if (file.Length > MaxUploadFileSizeB)
                {
                    errors.Add(String.Format("Faylın ölçüsü {0} MB-dan çox ola bilməz", MaxUploadFileSizeMB));
                    HasError = true;
                }
            }
            if (HasError)
            {
                ViewBag.ErrorMessages = errors;
            }
            if (!HasError && ModelState.IsValid)
            {
                bool uploaded = false;

                string filename = Path.GetFileName(file.FileName);
                string contenttype = file.ContentType;

                using (Stream fs = file.OpenReadStream())
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        uploaded = DBCBAR.UploadReport(filename, contenttype, bytes, (int)HttpContext.Session.GetInt32("UserId"), HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString(), reportUploadVM.BankId.Value, reportUploadVM.report_file_type_id.Value, isPhqe, periodstartdate, periodenddate, out int inserted_id, out string errorMessage);
                        if (uploaded)
                        {
                            DBLog.AddInfo("UploadReportFileByCBAR", HttpContext, reports_file: inserted_id.ToString(), data: String.Format("Filename:{0}, BankId:{1}", filename, reportUploadVM.BankId.Value));
                            ViewBag.SuccessMessage = "\"" + filename + "\" faylı göndərildi,ilkin yoxlamanın nəticəsini gözləyin.";
                        }
                        else
                        {
                            DBLog.AddError("UploadReportFileByCBAR", HttpContext, data: String.Format("Filename:, BankId:{1}", filename, reportUploadVM.BankId.Value));
                            ViewBag.ErrorMessage = "Səhv! \"" + filename + "\" faylı yüklənmədi";
                        }
                    }

                }

            }

            ViewBag.ReportFileTypeList = DBCBAR.GetReportFileTypeList(userId);
            int bank_id = reportUploadVM.BankId.HasValue ? reportUploadVM.BankId.Value : -1;
            var RecentUploadedReportFiles = DBCBAR.GetRecentUploadedReportFiles(userId, bank_id);
            ViewBag.RecentUploadedReportFiles = RecentUploadedReportFiles;

            if (reportUploadVM.bank_types != null)
                SetFilterViewBags(reportUploadVM.bank_types);
            else
                SetFilterViewBags(null);

            reportUploadVM.ShowPhqe = reportUploadVM.ReportVersionId == 1 ? false : true;
            ViewBag.ReportVersions = DBCBAR.GetReportVersions();
            return View(reportUploadVM);
        }

        [HttpGet]
        public ActionResult ReportErrors(int reportId)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return new EmptyResult();

            var ReportErrors = DBCBAR.GetReportFileErrors(reportId);

            var ActionLog = DBCBAR.GetActionLogs(reportId).FirstOrDefault();
            ViewBag.ErrorBankName = ActionLog.BANK;
            ViewBag.ErrorTypeName = ActionLog.REPORT_FILE_TYPE_NAME;

            ViewBag.ReportErrors = ReportErrors;
            return View();
        }

        [HttpGet]
        public ActionResult ReportXlsCellWithData(int reportFileId, int ruleId, bool isMultiRow)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return new EmptyResult();
            ViewBag.IsMultiRow = isMultiRow;
            List<XlsCellWithData> result = DBCBAR.GetXlsCellWithData(reportFileId, ruleId);
            return View(result);
        }

        [HttpGet]
        public ActionResult ReportFileErrors(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return new EmptyResult();

            var ReportErrors = DBCBAR.GetReportFileErrors(id);
            ViewBag.ReportErrors = ReportErrors;

            var ActionLog = DBCBAR.GetActionLogs(id).FirstOrDefault();
            ViewBag.ErrorBankName = ActionLog.BANK;
            ViewBag.ErrorTypeName = ActionLog.REPORT_FILE_TYPE_NAME;

            return View();


        }

        [HttpGet]
        public ActionResult DownloadReport(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });


            int userId = (int)HttpContext.Session.GetInt32("UserId");
            DBFileInfo fi = DBCBAR.GetReportUploadedFile(id, userId);

            if (fi.NotFound)
                return new EmptyResult();

            MemoryStream ms = new MemoryStream(fi.UPLOAD_FILE_DATA);
            string contentType = "binary/octet-stream";
            if (!String.IsNullOrEmpty(fi.UPLOAD_FILE_CONTENT_TYPE))
                contentType = fi.UPLOAD_FILE_CONTENT_TYPE;
            var fsr = new FileStreamResult(ms, contentType);
            fsr.FileDownloadName = fi.UPLOAD_FILE_NAME;
            return fsr;
        }

        [HttpGet]
        public ActionResult DownloadRecentlyUploadedReport(int id)
        {
            DBFileInfo fi = DBCBAR.GetReportUploadedFile(id, (int)HttpContext.Session.GetInt32("UserId"));

            if (fi.NotFound)
                return new EmptyResult();

            MemoryStream ms = new MemoryStream(fi.UPLOAD_FILE_DATA);
            string contentType = "binary/octet-stream";
            if (!String.IsNullOrEmpty(fi.UPLOAD_FILE_CONTENT_TYPE))
                contentType = fi.UPLOAD_FILE_CONTENT_TYPE;
            var fsr = new FileStreamResult(ms, contentType);
            fsr.FileDownloadName = fi.UPLOAD_FILE_NAME;
            return fsr;
        }

        [HttpGet]
        public ActionResult RecentFilesTable(int bankId)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return new EmptyResult();

            int userId = (int)HttpContext.Session.GetInt32("UserId");
            var recentFiles = DBCBAR.GetRecentUploadedReportFiles(userId, bankId);
            ViewBag.RecentUploadedReportFiles = recentFiles;
            return View();
        }

        [HttpGet]
        public ActionResult ActionLogs(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return new EmptyResult();

            var ActionLogs = DBCBAR.GetActionLogs(id);

            ViewBag.ReportBankName = ActionLogs.FirstOrDefault().BANK;
            ViewBag.ReportTypeName = ActionLogs.FirstOrDefault().REPORT_FILE_TYPE_NAME;
            return View(ActionLogs);
        }

        [HttpGet]
        public ActionResult ActionReportFileLogs(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return new EmptyResult();

            var ActionLogs = DBCBAR.GetActionLogs(id);

            ViewBag.ReportBankName = ActionLogs.FirstOrDefault().BANK;
            ViewBag.ReportTypeName = ActionLogs.FirstOrDefault().REPORT_FILE_TYPE_NAME;
            return View("ActionLogs", ActionLogs);
        }

        public ActionResult GetPeriodType(int id)
        {
            var model = DBCBAR.GetPeriodTypeByReportFileType(id);

            return Json(model, new JsonSerializerOptions
            {
            });

        }

        #region private methods
        private void SetFilterViewBags(string[] bankTypes, params int[] selectedBanks)
        {
            var DaysList = new List<SelectListItem>();
            DaysList.Add(new SelectListItem { Text = "yarımaylıq", Value = "1" });
            DaysList.Add(new SelectListItem { Text = "aylıq", Value = "2" });

            ViewBag.DaysList = DaysList;
            SetBankTypeList();
            SetBankList(bankTypes, selectedBanks);
        }
        private void SetBankTypeList()
        {

            int userId = (int)HttpContext.Session.GetInt32("UserId");
            var bankTypeList = DBCBAR.GetBankTypeList(userId);
            var openedBankTypeList = bankTypeList.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
            var closedBankTypeList = bankTypeList
                .Select(x => new SelectListItem { Value = $"0{x.Id}", Text = $"{x.Name} (Bağlanmış)" });
            openedBankTypeList.AddRange(closedBankTypeList);
            ViewBag.BankTypeList = openedBankTypeList.OrderBy(x => x.Text).ToList();
        }
        private void SetBankList(string[] bankTypes, params int[] selectedBanks)
        {
            List<SelectListItem> result;
            var allBankList = DBCBAR.GetBankList((int)HttpContext.Session.GetInt32("UserId"), bankTypes);

            if (bankTypes == null || bankTypes.Length == 0)
            {
                result = allBankList.Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();
            }
            else
            {
                result = new List<SelectListItem>();
                foreach (var item in ConvertToBankTypeFilter(bankTypes))
                {
                    var filterResult = allBankList
                        .Where(x => x.BANK_TYPE_ID == item.Id && x.IS_CLOSED == item.IsClosed)
                        .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
                        .ToList();
                    result.AddRange(filterResult);
                }
            }

            if (selectedBanks != null && selectedBanks.Length > 0)
                result.ForEach(x => x.Selected = selectedBanks.Contains(Convert.ToInt32(x.Value)));

            ViewBag.BankList = result.OrderBy(x => x.Text).ToList();
        }
        private List<BankTypeFilter> ConvertToBankTypeFilter(string[] bankTypes)
        {
            if (bankTypes == null || bankTypes.Length == 0) return null;

            var bankTypeFilters = bankTypes.Where(x => !string.IsNullOrEmpty(x)).Select(x => new BankTypeFilter
            {
                IsClosed = x.StartsWith("0"),
                Id = Convert.ToInt32(x.StartsWith("0") ? x.Substring(1) : x)
            });

            return bankTypeFilters.ToList();
        }
        private bool IfParamsIsEmpty(ref string yearmonth, int[] bank_ids, string days)
        {
            bool changed = false;
            if (yearmonth == "" && (bank_ids == null || bank_ids.Length == 0) && days == "")
            {
                yearmonth = String.Format("{0}-{1:00}", DateTime.Now.Year, DateTime.Now.Month);
                changed = true;
            }
            return !changed;
        }
        #endregion
    }
}
