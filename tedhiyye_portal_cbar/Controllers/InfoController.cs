﻿using tedhiyye_portal_cbar.Models;
using tedhiyye_portal_shared;
using tedhiyye_portal_shared.Domains;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using tedhiyye_portal_cbar;

namespace tedhiyye_portal_cbar.Controllers
{
    public class InfoController : Controller
    {
        public ActionResult Downloads(int id, int? department)
        {
            int? userId = HttpContext.Session.GetInt32("UserId") as int?;
            if (userId == null)
            {
                var path = HttpContext.Request.Path;
                var query = HttpContext.Request.QueryString;
                var pathAndQuery = path + query;
                return RedirectToAction("Login", "Home", new { ReturnUrl = pathAndQuery });
            }
            int group_id = id;
            ViewBag.Title = "Yükləmələr";
            //if (group_id == 2)
            //    ViewBag.Title = "Hesabat Formaları";
            //else
            //if (group_id == 3)
            //    ViewBag.Title = "Təlimatlar normativ-hüquqi baza";
            //else
            if (group_id == 4)
                ViewBag.Title = "İstifadəçi təlimatı";

            var DepartmentItems = DBCBAR.GetDepartments();
            ViewBag.DepartmentsList = new SelectList(
                DepartmentItems,
                "Value",
                "Text",
                department);
            var downloads = DBCBAR.GetDownloads(group_id, department);
            ViewBag.ShowDepartments = (group_id != 4);
            return View(downloads);
        }

        public ActionResult CheckRules(int? reportFileType, int? Status)
        {
            int? userId = HttpContext.Session.GetInt32("UserId") as int?;
            if (userId == null)
            {
                return RedirectToAction("Login", "Home", new { ReturnUrl = HttpContext.Request.Path + HttpContext.Request.QueryString });
            }

            var reportFileTypeList = DBCBAR.GetReportFileTypeList(userId);
            ViewBag.ReportFileTypeList = reportFileTypeList;

            if (reportFileType == null && reportFileTypeList.Count > 0)
                return RedirectToAction("CheckRules", new { reportFileType = reportFileTypeList[0].Value });

            var checkRulesList = new List<CheckRule>();
            if (reportFileTypeList.Count > 0)

                checkRulesList = DBCBAR.GetCheckRules(reportFileType, Status);
            ViewBag.Status = Status == null ? 1 : Status;
            return View(checkRulesList);

        }

        [HttpGet]
        public ActionResult Download(int id)
        {
            int? userId = HttpContext.Session.GetInt32("UserId") as int?;
            if (userId == null)
                return new EmptyResult();

            var downloadFileInfo = DBCBAR.GetDownload(id);

            if (downloadFileInfo.UPLOAD_FILE_DATA.Length == 0)
                return new EmptyResult();


            MemoryStream ms = new MemoryStream(downloadFileInfo.UPLOAD_FILE_DATA);
            var fsr = new FileStreamResult(ms, "application/vnd.ms-excel.sheet.macroEnabled.12");
            fsr.FileDownloadName = downloadFileInfo.Description;
            return fsr;
        }

        public ActionResult ReportTemplates()
        {
            var userId = HttpContext.Session.GetInt32("UserId");
            if (userId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            ViewBag.Title = "Hesabat Formaları";
            var reportTemplates = DBCBAR.GetReportTemplates((int)userId);
            return View(reportTemplates);
        }

        public ActionResult DownloadReportTemplate(int id)
        {
            var userId = HttpContext.Session.GetInt32("UserId");
            if (userId == null)
                return new EmptyResult();

            Response.Headers.Add("Cache-Control", " no-store, no-cache");
            Response.Headers.Append("Pragma", "no-cache"); // HTTP 1.0.
            Response.Headers.Append("Expires", "0"); // Proxies.
            var rf = DBCBAR.GetReportTemplate(id);

            if (rf.NotFound)
                return new EmptyResult();

            MemoryStream ms = new MemoryStream(rf.Report_Template);
            string contentType = "application/vnd.ms-excel.sheet.macroEnabled.12";
            var fsr = new FileStreamResult(ms, contentType);
            fsr.FileDownloadName = rf.UPLOAD_FILE_NAME.EndsWith(".xls") || rf.UPLOAD_FILE_NAME.EndsWith(".xlsx") || rf.UPLOAD_FILE_NAME.EndsWith(".xlsm") ? rf.UPLOAD_FILE_NAME : rf.UPLOAD_FILE_NAME + ".xls";
            return fsr;
        }

        public ActionResult AddCheckRule(int? id)
        {
            int? userId = HttpContext.Session.GetInt32("UserId") as int?;
            if (userId == null)
            {
                var path = HttpContext.Request.Path;
                var query = HttpContext.Request.QueryString;
                var pathAndQuery = path + query;
                return RedirectToAction("Login", "Home", new { ReturnUrl = pathAndQuery });
            }

            var reportFileTypeList = DBCBAR.GetReportFileTypeList(userId);
            var reportFileTypeListForView = new List<SelectListItem>();
            reportFileTypeListForView.Add(new SelectListItem { Value = "0", Text = "Siyahıdan seçin" });
            foreach (var reportFileType in reportFileTypeList)
            {
                var reportFileTypeForView = new SelectListItem
                {
                    Text = reportFileType.Text,
                    Value = reportFileType.Value
                };
                reportFileTypeListForView.Add(reportFileTypeForView);
            }
            ViewBag.XlRuleOperators = new SelectList(Constants.XlRuleOperators, "Key", "Value");
            ViewBag.ReportFileTypes = reportFileTypeListForView;
            var reportSheets = new List<SelectListItem>();
            reportSheets.Add(new SelectListItem() { Text = "Siyahıdan seçin", Value = "0" });
            ViewBag.ReportSheets = reportSheets;
            ViewBag.RoundSize = "";
            var isEdit = 0;
            if (id != null)
            {
                isEdit = 1;
                var rule = new XlRule();
                rule.Id = int.Parse(id.ToString());
                var formula = DBCBAR.GetXlFormula(rule);
                ViewBag.FormulaCodeForView = formula.FormulaCodeForView;
                ViewBag.FormulaCode = formula.FormulaCode;
                ViewBag.FormulaText = rule.Formula;
                ViewBag.SplittedFormula = formula.splittedFormulaCodeForView;
                ViewBag.SplittedFormulaCode = formula.splittedFormulaCode;
                ViewBag.SplitOperators = formula.splitOperators;
                ViewBag.Status = formula.Status;
                ViewBag.RoundSize = rule.RoundSize;
                ViewBag.GroupName = formula.groupName;
            }
            ViewBag.IsEdit = isEdit;
            return View();
        }

        public JsonResult GetReportSheets(int id)
        {
            var reportTypes = DBCBAR.GetReportSheets(id);
            return Json(reportTypes);
        }

        public JsonResult GetReportCols(int id)
        {
            var reportCols = DBCBAR.GetReportColumns(id);
            return Json(reportCols);
        }

        public JsonResult GetReportRows(int id)
        {
            var reportRows = DBCBAR.GetReportRows(id);
            return Json(reportRows);
        }
        public JsonResult GetDataBySelectedReport(string reportFormula)
        {
            reportFormula = HttpUtility.UrlDecode(reportFormula.Substring(reportFormula.IndexOf('~') + 1));
            var reportData = DBCBAR.GetDataBySelectedReport(reportFormula);
            return Json(reportData);
        }

        public ActionResult AddOrUpdateFormula(int? id, XlRulesFormula formula)
        {
            var result = DBCBAR.AddOrUpdateXlRuleAndFormula(id, formula);
            if (result == 1)
            {
                string addText = "əlavə olundu";
                string updateText = "yeniləndi";
                if (id == null)
                {
                    TempData["SuccessMessage"] = "Yoxlama " + addText;
                }
                else
                {
                    TempData["SuccessMessage"] = "Yoxlama " + updateText;
                }

            }
            else
            {
                TempData["ErrorMessage"] = "Səhv baş verdi";
            }
            if (id == null)
            {
                return RedirectToAction("AddCheckRule");
            }
            else
            {
                return RedirectToAction("AddCheckRule", new { id = id });
            }
        }
    }
}
