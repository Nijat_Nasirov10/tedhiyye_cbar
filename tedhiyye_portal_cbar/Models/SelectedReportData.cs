﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar.Models
{
    public class SelectedReportData
    {
        public int XlsSheetId { get; set; }
        public string XlsSheetName { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportFileTypeId { get; set; }
        public string ReportFileTypeName { get; set; }
        public string XlsColumnIds { get; set; }
        public string XlsColumns { get; set; }
        public string XlsRows { get; set; }
        public string XlsRowIds { get; set; }
        public int PreviousMonth { get; set; }
    }
}
