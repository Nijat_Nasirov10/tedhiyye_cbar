﻿var answerReportOTable;
$(document).ready(function () {

    var url_report_answer = $("#url-report").data('answer');
    var url_report_export = $("#url-report").data('export');

    answerReportOTable = $("#answer_report_datatable").DataTable({
        "processing": datatables_options.processing,
        "serverSide": true,
        "ajax": {
            "url": url_report_answer,
            "type": "post",
            "data": function (data) {
                var year = $("#years").val();
                var month = $("#months").val();

                data.MonthOfYearSearch = { Year: year, Month: month };
            }
        },
        "dom": "bfrtip",
        "ordering": false,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "autoWidth": false,
        "pageLength": 15,
        "columns": [
            { "data": "CompanyClass" },
            { "data": "CId" },
            { "data": "Company" },
            { "data": "Area" },
            { "data": "SequenceNo" },
            { "data": "Ifnt" },
            { "data": "CompanyClassId" },
            { "data": "PrivateAnswer" },
            { "data": "Question" },
            { "data": "Answer" },
            { "data": "Point" },
        ]
    })

    $("#export-excel").click(function () {
        

        var year = $("#years").val();
        var month = $("#months").val();

        $.get(url_report_export, { year: year, month: month }, function (data) {
           
            $("#report-container").excelexportjs({
                containerid: "report-container",
                datatype: 'json',
                dataset: data,
                columns: getColumns(data),
                encoding: "utf-8"
            });
        })
    })
});


function reloadAnswerReportTable() {
    answerReportOTable.ajax.reload();
}

