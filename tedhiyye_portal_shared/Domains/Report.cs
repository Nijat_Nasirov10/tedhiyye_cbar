﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class Report
    {
        public int ID { get; set; }
        public int REPORTTYPEID { get; set; }
        public int USERID { get; set; }
        public DateTime UPLOAD_TIME { get; set; }
        public DateTime SENDDATE { get; set; }
        public DateTime PERIODSTART { get; set; }
        public DateTime PERIODEND { get; set; }
        public int STATUSID { get; set; }
        public int CONFIRMSTATUS { get; set; }
        public int REPORT_FILE_STATUS_ID { get; set; }
        public int? CHECKID { get; set; }
        public string DESCRIPTION { get; set; }
        public string BANKNAME { get; set; }
        public string BANK { get; set; }
        public int? BANKID { get; set; }
        public DateTime? STATUS_CHAGE_DATE { get; set; }
        public int? STATUS_CROSS_CHECK_ID { get; set; }
        public DateTime? STATUS_CROSS_CHECK_CHANGE_DATE { get; set; }
        public int READY_FOR_BI { get; set; }
        public string REPORTTYPETEXT { get; set; }
        public string REPORT_FILE_TYPE_NAME { get; set; }
        public string STATUSTEXT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_CROSS_CHECK_TEXT { get; set; }
        public int? REPORTS_FILE_ID { get; set; }
        public DateTime? DissiminateDate { get; set; }
        public int CountInFileGroup { get; set; }
        public int IS_PHQE { get; set; }
        public int CheckSimilarLimit { get; set; }
    }
}
