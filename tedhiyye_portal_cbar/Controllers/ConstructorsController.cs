﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar.Controllers
{
    public class ConstructorsController : Controller
    {
        public IActionResult Index()
        {
            var reportConstructors = DBCBAR.GetReportConstructors();
            return View(reportConstructors);
        }
        

    }
}
