﻿using tedhiyye_portal_cbar.Models;
using tedhiyye_portal_shared;
using tedhiyye_portal_shared.Domains.RealSector.Models;
using System.Collections.Generic;


namespace insurance_system_cbar.ViewModels.RealSector.Archive
{
    public class QuestionnaireHistoryVM
    {
        public PaginatedList<QuestionnaireHistoryItem> Items { get; set; }
        public List<CompanyClass> CompanyClasses { get; set; }
        public List<CustomCompany> Companies { get; set; }

        public int Page { get; set; } = 1;
        public string SendDate { get; set; } = "";
        public string CompanyId { get; set; } = "";
        public string CompanyClassId { get; set; } = "";

        public string SearchUrl
        {
            get
            {
                return $"?Page={Page}&SendDate={SendDate}&CompanyId={CompanyId}&CompanyClassId={CompanyClassId}";
            }
        }


    }
}