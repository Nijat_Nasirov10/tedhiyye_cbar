﻿using System.Collections.Generic;

namespace tedhiyye_portal_shared
{
    public class Constants
    {
        public static string[] MonthNamesAz = { "Yanvar", "Fevral", "Mart", "Aprel", "May", "İyun", "İyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr" };

        public static Dictionary<string, List<ReportStatus>> AllowedStatusListForActionCBAR = new Dictionary<string, List<ReportStatus>>
        {
            { "confirm", new List<ReportStatus>{ ReportStatus.Gecikdirilib } },
            { "cancel", new List<ReportStatus>{  ReportStatus.Duzdur, ReportStatus.Yoxlama_yoxdur, ReportStatus.Serti_duzdur } },
            { "confirmascorrect", new List<ReportStatus>{ ReportStatus.Sehvdir_Qebul_olunmadi } },
            { "delete", new List<ReportStatus>{ ReportStatus.Imtina_edilib } }
        };

        public static Dictionary<string, List<ReportStatus>> AllowedStatusListForActionBanks = new Dictionary<string, List<ReportStatus>>
        {
            { "confirm", new List<ReportStatus>{ ReportStatus.Yuklenib, ReportStatus.Imtina_edilib } },
			{ "cancel", new List<ReportStatus>{ ReportStatus.Duzdur, ReportStatus.Yoxlama_yoxdur, ReportStatus.Serti_duzdur } },
        };

        public static Dictionary<string, string> ReportActionLogDescription = new Dictionary<string, string>
        {
            { "UploadReportFile", "Yükləndi" },
            { "UploadReportFileByCBAR", "Yükləndi" },
            { "ConfirmReport", "Təsdiqləndi" },
            { "CancelReport", "İmtina edildi"},
            { "ConfirmAsCorrectReport", "Düz kimi qəbul edildi"},
            { "DeleteReport", "Silindi" },
            { "CheckReport", "Yoxlanıldı" },
        };

        public static HashSet<ReportStatus> ErrorListShownForStatuses = new HashSet<ReportStatus>
        {
            //ReportStatus.Duzdur,
            ReportStatus.Imtina_edilib,
            ReportStatus.Sehvdir_Qebul_olunmadi,
            ReportStatus.Serti_duzdur,
            ReportStatus.Yoxlama_yoxdur
            //ReportStatus.Tesdiq_edilib
        };

        public static HashSet<ReportUploadFileStatus> ErrorListShownForUploadStatuses = new HashSet<ReportUploadFileStatus>
        {
            ReportUploadFileStatus.ErrorConvertingToXML,
            ReportUploadFileStatus.ErrorInsertingToDB,
           // ReportUploadFileStatus.InsertedToDB,
            ReportUploadFileStatus.ErrorValidating,
            ReportUploadFileStatus.ConfirmAsCorrect
        };

        public static Dictionary<int, string> BankUserRegistrationStatusDescription = new Dictionary<int, string>
        {
            { 0, "Baxılmayıb" },
            { 1, "Təsdiqlənib" },
            { 2, "İmtina edilib" },
        };

        public static Dictionary<int, string> XlRuleOperators = new Dictionary<int, string>
        {
            { 0, "=" },
            { 1, "<>" },
            { 2, "<=" },
            { 3, ">=" },
            { 4, "<" },
            { 5, ">" },
            { 6, "+" },
            { 7, "-" },
            { 8, "*" },
            { 9, "/" },
            { 10, "MAX" },
            { 11, "MIN" },
            { 12, "ABS" },
            { 13, "(" },
            { 14, ")" },
            { 15, " AND " },
            { 16, " OR " }
        };

    }
}
