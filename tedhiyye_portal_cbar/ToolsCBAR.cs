﻿using tedhiyye_portal_shared;
using tedhiyye_portal_shared.Domains;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar
{
    public class ToolsCBAR
    {
        public static AuthResponse CheckLoginUsername(string username, string password)
        {
            AuthResponse ar = new AuthResponse();
            ar = DBCBAR.AuthUsername(username, password);
            return ar;
        }

        public static List<KeyValuePair<int, string>> ConvertSelectListToKeyValuePairs(List<SelectListItem> listItems)
        {
            return listItems.Select(i => new KeyValuePair<int, string>(Convert.ToInt32(i.Value), i.Text)).ToList<KeyValuePair<int, string>>();
        }

        public static List<SelectListItem> ConvertKeyValuePairsToSelectList(List<KeyValuePair<int, string>> keyValuePairs)
        {
            return keyValuePairs.Select(i => new SelectListItem() { Value = i.Key.ToString(), Text = i.Value }).ToList<SelectListItem>();
        }

        public static bool HasRole(CBARUserRole userRole)
        {
            //List<int> roles = (List<int>)HttpContext.Session.GetInt32("Roles");
            //if (roles == null || roles.Count == 0) return false;

            //bool hasRole = false;
            //foreach (int item in roles)
            //{
            //    if ((CBARUserRole)item == userRole)
            //    {
            //        hasRole = true;
            //        break;
            //    }
            //}

            //return hasRole;  
            return true;
        }

        public static IReadOnlyCollection<int> RealSectorCompanyTypes
        {
            get
            {
                //var companyTypes = HttpContext.Current.Session["RealSectorCompanyTypes"] as List<int>;

                //return companyTypes != null ? new ReadOnlyCollection<int>(companyTypes) : null;
                return null;
            }
        }

        public static byte[] ExportToCsv<T>(IEnumerable<T> records)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                using (var csvWriter = new CsvHelper.CsvWriter(streamWriter, CultureInfo.CurrentCulture))
                {
                    csvWriter.WriteField("sep=,", false);
                    csvWriter.NextRecord();
                    csvWriter.WriteRecords<T>(records);
                }

                return memoryStream.ToArray();
            }
        }

    }
}
