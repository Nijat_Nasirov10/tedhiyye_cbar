﻿
namespace tedhiyye_portal_cbar.ViewModels.RealSector.Archive
{
    public class AnswerReportItemVM
    {
        public string CompanyClass { get; set; }
        public int? CId { get; set; }
        public string Company { get; set; }
        public string Area { get; set; }
        public string SequenceNo { get; set; }
        public string Ifnt { get; set; }
        public int? CompanyClassId { get; set; }
        public string PrivateAnswer { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int? Point { get; set; }
    }
}