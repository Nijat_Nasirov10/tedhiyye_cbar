﻿using tedhiyye_portal_shared;
using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_shared.Domains
{
    public class BankUser
    {
        public int Id { get; set; }
        public int BankId { get; set; }

        [Display(Name = "Tip")]
        public int RoleId { get; set; }

        [Display(Name = "Tip")]
        public string RoleName { get; set; }

        //[Required(ErrorMessage = "{0} daxil edilməyib")]
        [Display(Name = "İstifadəçi adı")]
        public string Username { get; set; }

        //[Required(ErrorMessage = "{0} daxil edilməyib")]
        [DataType(DataType.Password)]
        [Display(Name = "Parol")]
        public string Password { get; set; }

        [Display(Name = "Parol (təkrar)")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Parol və təkrarı uyğun gəlmir")]
        public string PasswordConfirm { get; set; }

        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [Display(Name = "Ad")]
        public string Name { get; set; }

        [Display(Name = "Soyad")]
        public string Surname { get; set; }

        [Display(Name = "Ata adı")]
        public string FatherName { get; set; }

        [Display(Name = "Struktur bölmə")]
        public string Department { get; set; }

        [Display(Name = "Vəzifə")]
        public string Position { get; set; }

        [Display(Name = "E-poçt")]
        [EmailAddress(ErrorMessage = "{0} düzgün daxil edilməyib")]
        public string Email { get; set; }

        [Display(Name = "Telefon")]
        public string Phone { get; set; }

        [Display(Name = "Telefon (İş)")]
        public string Office_Phone { get; set; }

        [Display(Name = "Telefon (Mobil)")]
        public string Mobile_Phone { get; set; }

        [Display(Name = "Autentifikasiya")]
        public int AuthType { get; set; }

        public string Cert { get; set; }

        [Display(Name = "Aktiv")]
        public bool IsActive
        {
            get
            {
                return AuthType != (int)AllowedAuthType.Deactivated;
            }
            set
            {
                if (value)
                    AuthType = (int)AllowedAuthType.ESignature;
                else
                    AuthType = (int)AllowedAuthType.Deactivated;
            }
        }
    }
}
