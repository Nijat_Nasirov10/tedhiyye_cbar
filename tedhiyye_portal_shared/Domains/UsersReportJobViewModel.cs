﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class UsersReportJobViewModel
    {
        public string UserName { get; set; }
        public string Job { get; set; }
        public string ReportFileType { get; set; }

    }
}
