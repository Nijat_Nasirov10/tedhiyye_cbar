﻿using Microsoft.AspNetCore.Mvc.Rendering;
using tedhiyye_portal_shared.Domains;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
//using System.Web.Mvc;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.CompanyManagement
{
    public class CompanyVM
    {
        public int Id { get; set; }
        public int? CompanyTypeId { get; set; }
        public string CompanyType { get; set; }

        [DisplayName("Fəaliyyət növü")]
        public string CompanyClass { get; set; }

        [DisplayName("Şirkət")]
        public string Name { get; set; }

        [DisplayName("Kod")]
        public string Code { get; set; }

        [DisplayName("Ünvan")]
        public string Address { get; set; }

        [DisplayName("Telefon")]
        public string Phone { get; set; }

        [DisplayName("Fax")]
        public string Fax { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Müdir")]
        public string Boss { get; set; }

        [DisplayName("Web-site")]
        public string WebSite { get; set; }

        [DisplayName("Qeydlər")]
        public string Description { get; set; }
    }

    public class CreateEditCompanyVM
    {
        public int? Id { get; set; }
        public int? CompanyTypeId { get; set; }

        [DisplayName("Fəaliyyət növü")]
        public int? CompanyClassId { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        [DisplayName("Şirkət")]
        public string Name { get; set; }

        [DisplayName("Kod")]
        public string Code { get; set; }

        [DisplayName("Ünvan")]
        public string Address { get; set; }

        [DisplayName("Telefon")]
        public string Phone { get; set; }

        [DisplayName("Fax")]
        public string Fax { get; set; }

        [EmailAddress(ErrorMessage = ValidationMessage.Email)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Müdir")]
        public string Boss { get; set; }

        [DisplayName("Web-site")]
        public string WebSite { get; set; }

        [DisplayName("Qeydlər")]
        public string Description { get; set; }

        public IEnumerable<SelectListItem> CompanyClasses { get; set; }
    }
}