﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("QUESTION")]
    public class Question
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("QUESTION")]
        public string QuestionText { get; set; }

        [Column("FORMID")]
        [ForeignKey("Form")]
        public int FormId { get; set; }

        [Column("ISPRIVATE")]
        public bool IsPrivate { get; set; }

        [Column("ISREQUIRED")]
        public bool IsRequired { get; set; }

        [Column("ORDERID")]
        public int OrderId { get; set; }

        [Column("ISACTIVE")]
        public bool IsActive { get; set; }

        [Column("BIICOEFFICIENT")]
        public int? BiiCoefficient { get; set; }

        [Column("ISNUMBER")]
        public int IsNumber { get; set; }

        public virtual Form Form { get; set; }
        public List<UserAnswerDetail> UserAnswerDetails { get; set; }
        public List<QuestionAnswer> QuestionAnswers { get; set; }
    }
}