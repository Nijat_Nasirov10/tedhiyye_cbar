﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.ViewModels
{
    public class UsernamePasswordVM
    {
        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [Display(Name = "İstifadəçi adı")]
        public string Username { get; set; }
        [Required(ErrorMessage = "{0} daxil edilməyib")]
        [Display(Name = "Parol")]
        public string Password { get; set; }
    }
}
