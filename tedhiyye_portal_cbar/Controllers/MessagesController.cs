﻿using tedhiyye_portal_cbar.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar.Controllers
{
    public class MessagesController : Controller
    {
        public ActionResult Inbox(int? page, int? message_id)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            if (message_id != null)
            {
                DBCBAR.MarkInboxMessageAsRead(message_id.Value);
                var message = DBCBAR.GetInboxMessage(message_id.Value);
                ViewBag.Message = message;
            }

            var messagesInbox = DBCBAR.GetInboxMessages((int)HttpContext.Session.GetInt32("UserId"), page ?? 1, 5);
            return View(messagesInbox);
        }

        public ActionResult Sent(int? page, int? message_id)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            if (message_id != null)
            {
                var message = DBCBAR.GetSentMessage(message_id.Value);
                ViewBag.Message = message;
            }

            var messagesSent = DBCBAR.GetSentMessages(0, (int)HttpContext.Session.GetInt32("DepartmentId"), page ?? 1, 5);
            return View(messagesSent);
        }

        [HttpGet]
        public ActionResult Compose()
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            ViewBag.DepartmentList = DBCBAR.GetBankListForSelectBox((int)HttpContext.Session.GetInt32("UserId"));

            var composeMessageVM = new ComposeMessageVM();
            return View(composeMessageVM);
        }

        [HttpPost]
        public ActionResult Compose(ComposeMessageVM composeMessageVM)
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            bool sent = false;

            if (ModelState.IsValid)
            {
                sent = DBCBAR.SendMessage(0, composeMessageVM);
            }

            if (sent)
            {
                TempData["SuccessMessage"] = "Mesaj göndərildi";
                return RedirectToAction("Sent");
            }
            else
            {
                ViewBag.DepartmentList = DBCBAR.GetBankListForSelectBox((int)HttpContext.Session.GetInt32("UserId"));

                return View(composeMessageVM);
            }
        }
    }
}
