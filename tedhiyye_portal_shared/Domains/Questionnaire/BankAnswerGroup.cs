﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankAnswerGroup
    {

        public BankAnswerGroup()
        {
            BankAnswers = new List<BankAnswer>();
        }
        public int ID { get; set; }
        public List<BankAnswer> BankAnswers { get; set; }
        public List<BankQuestionDetail> BankQuestionAnswers { get; set; }

    }
}
