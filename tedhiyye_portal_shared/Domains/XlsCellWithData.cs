﻿using System.ComponentModel.DataAnnotations;

namespace tedhiyye_portal_shared.Domains
{
    public class XlsCellWithData
    {
        public XlsCellWithData(string cell, string data, string groupName, string sheetName)
        {
            Cell = cell;
            Data = data;
            GroupName = groupName;
            SheetName = sheetName;
        }

        [Display(Name = "Sahə (xana)")]
        public string Cell { get; }

        [Display(Name = "Dəyər")]
        public string Data { get; }

        [Display(Name = "Qrup")]
        public string GroupName { get; }
        [Display(Name = "Hesabat")]
        public string SheetName { get; }
    }
}
