﻿using tedhiyye_portal_cbar.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using tedhiyye_portal_shared;
using tedhiyye_portal_shared.ViewModels;
using tedhiyye_portal_cbar;

namespace insurance_system_cbar.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (HttpContext.Session.GetInt32("UserId") == null)
                return RedirectToAction("Login", new
                {
                    ReturnUrl = HttpContext.Request.Path + HttpContext.Request.QueryString
                });

            int userID = (int)HttpContext.Session.GetInt32("UserId");
            int departmentID = (int)HttpContext.Session.GetInt32("DepartmentId");

            if (!ToolsCBAR.HasRole(CBARUserRole.Bank_prudential))
            {
                if (ToolsCBAR.HasRole(CBARUserRole.Real_sector))
                    return RedirectToAction("Questionnaire", "RealSector");
                else
                    return RedirectToAction("ChangePassword", "Home");

            }

            var Reports = DBCBAR.GetRecentUploadedReportFiles(userID, -1, 10);
            ViewBag.Reports = Reports;

            var UnreadCount = DBCBAR.GetUnreadInboxMessagesCount(userID);
            ViewBag.UnreadCount = UnreadCount;

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            UsernamePasswordVM up = new UsernamePasswordVM();
            return View(up);
        }

        [HttpPost]
        public ActionResult Login(UsernamePasswordVM up)
        {
            // For debug
            bool isDebug = false;
            if (HttpContext.Request.Path.Value.StartsWith("http://localhost"))
            {
                isDebug = true;
            }

            if (ModelState.IsValid || isDebug)
            {
                bool userExistsInDb = DBCBAR.CheckUserName(up.Username, out int userId, out int departmentid);

                if (!userExistsInDb)
                {
                    ViewBag.LoginMessage = "İstifadəçi adı sistemdə tapılmadı";
                    return View(up);
                }

                LdapLocal ldap = new LdapLocal();

                if (!ldap.NovellLogin(up.Username, up.Password, out LdapLocal.LDAPUser ldapUser))
                {
                    ViewBag.LoginMessage = "İstifadəçi məlumatları səhvdir və ya tapılmadı";
                    return View(up);
                }

                HttpContext.Session.SetInt32("DepartmentId", departmentid);
                HttpContext.Session.SetString("DepartmentName", ldapUser.Department);
                HttpContext.Session.SetInt32("UserId", userId);
                HttpContext.Session.SetString("Username", ldapUser.Username);
                HttpContext.Session.SetString("FullName", ldapUser.Displayname);
                HttpContext.Session.SetString("Roles", "");
                HttpContext.Session.SetString("RealSectorCompanyTypes", "");

                Log.Add("Login", string.Format("UserId: {0}, Username: {1}", userId, ldapUser.Username));

                if (HttpContext.Request.Query["ReturnUrl"].ToString() != null && Url.IsLocalUrl(HttpContext.Request.Query["ReturnUrl"].ToString()))
                    return Redirect(HttpContext.Request.Query["ReturnUrl"].ToString());
                else
                    return RedirectToAction("Index", "Home");
            }
            return View(up);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Log.Add("Logout", string.Format("UserId: {0}, Username: {1}", HttpContext.Session.GetInt32("UserId"), HttpContext.Session.GetInt32("Username")));

            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult ConvertAllToHash()
        {
            DBCBAR.ConvertAllToHash();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [HttpPost]
        public ActionResult Error()
        {
            return View();
        }

        public class LDAPUser
        {
            public string LastName { get; set; }
            public string Firstname { get; set; }
            public string Email { get; set; }
            public string Username { get; set; }
            public string Department { get; set; }
            public string Position { get; set; }
        }
    }
}
