﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains
{
    public class CheckRule
    {
        public int Id { get; set; }
        public int ReportFileTypeId { get; set; }
        public string ReportFileTypeName { get; set; }
        public string Formula { get; set; }
        public string ShortName { get; set; }
        public string CheckValue { get; set; }
        public string Status { get; set; }
    }
}
