﻿using tedhiyye_portal_shared.Domains;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tedhiyye_portal_cbar;

namespace insurance_system_cbar.Controllers
{
    public class ResponsiblesController : Controller
    {
        public ActionResult ResponsiblesStatUnits(int? su, string[] bankType)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            if (bankType == null)
            {
                bankType = new string[] { "1" };
            }
            var bankTypeList = DBCBAR.GetBankTypeList((int)sessionUserId);
            var openedBankTypeList = bankTypeList.Where(x => x.Id != 5).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            ViewBag.BankTypeList = openedBankTypeList.OrderBy(x => x.Text).ToList();
            var BankList = DBCBAR.GetBankList((int)sessionUserId, bankType).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            ViewBag.BankList = BankList;
            int firstBank = BankList.Count > 0 ? Convert.ToInt32(BankList[0].Value) : -1;
            int bankID = su.HasValue ? su.Value : firstBank;
            ViewBag.BankId = bankID;
            var respList = DBCBAR.GetResponsiblesBank((int)sessionUserId, bankID, string.Empty);
            ViewBag.ReportFileTypes = DBCBAR.GetReportFileTypeList(sessionUserId);
            return View(respList);
        }

        [HttpPost]
        public ActionResult ResponsiblesStatUnits(int? su, string[] bankType, string reportFileTypes)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            if (bankType == null)
            {
                bankType = new string[] { "1" };
            }
            var bankTypeList = DBCBAR.GetBankTypeList((int)sessionUserId);
            var openedBankTypeList = bankTypeList.Where(x => x.Id != 5).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            ViewBag.BankTypeList = openedBankTypeList.OrderBy(x => x.Text).ToList();
            var BankList = DBCBAR.GetBankList((int)sessionUserId, bankType).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            ViewBag.BankList = BankList;
            int firstBank = BankList.Count > 0 ? Convert.ToInt32(BankList[0].Value) : -1;
            int bankID = su.HasValue ? su.Value : firstBank;
            ViewBag.BankId = bankID;
            var respList = DBCBAR.GetResponsiblesBank((int)sessionUserId, bankID, reportFileTypes);
            ViewBag.ReportFileTypes = DBCBAR.GetReportFileTypeList(sessionUserId);
            return View("ResponsiblesStatUnits", respList);
        }

        public ActionResult ResponsiblesCBAR()
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });

            var respList = DBCBAR.GetResponsiblesCBAR((int)sessionUserId);

            return View(respList);
        }

        public ActionResult UpdateResponsiblesCBAR(int id)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            var hasAccessReportFileType = DBCBAR.GetModeratorReportFileTypes((int)sessionUserId).Exists(x => x.Value == id.ToString());
            if (!hasAccessReportFileType)
            {
                return RedirectToAction("ResponsiblesCbar", "Responsibles", new { ReturnUrl = Request.Path + Request.QueryString });
            }
            var cbarUsers = DBCBAR.GetCbarUsers();
            ViewBag.CbarUsersList = cbarUsers;
            var selectedUsers = DBCBAR.GetResponsiblesByReportFileTypeId(id, 0, (int)sessionUserId);
            return View(selectedUsers);
        }

        [HttpPost]
        public ActionResult UpdateResponsiblesCBAR(int reportFileTypeId, string executorId, string curatorId, string substituteId)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            var status = DBCBAR.ChangeReportEmployees(reportFileTypeId, executorId, curatorId, substituteId, 0);
            if (status != 0)
            {
                TempData["SuccessMessage"] = "Məlumatlar uğurla yeniləndi!";
            }
            return RedirectToAction("UpdateResponsiblesCbar", "Responsibles", new { id = reportFileTypeId });
        }

        public ActionResult UpdateResponsiblesSU(int id, int bankId)
        {
            try
            {
                var sessionUserId = HttpContext.Session.GetInt32("UserId");
                if (sessionUserId == null)
                {
                    return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
                }

                var hasAccessReportFileType = DBCBAR.GetModeratorReportFileTypes((int)sessionUserId).Exists(x => x.Value == id.ToString());
                if (!hasAccessReportFileType)
                {
                    return RedirectToAction("ResponsiblesStatUnits", "Responsibles", new { ReturnUrl = Request.Path + Request.QueryString });
                }

                List<SelectListItem> UserList = new List<SelectListItem>();

                var bankUsers = DBCBAR.GetBankUsersForResponsibles(bankId);
                foreach (var item in bankUsers)
                {
                    UserList.Add(new SelectListItem()
                    {
                        Text = item.Name,
                        Value = item.Id.ToString()
                    });
                }
                ViewBag.BankUsersList = UserList;

                var selectedUsers = DBCBAR.GetResponsiblesByReportFileTypeId(id, bankId, (int)sessionUserId);

                return View(selectedUsers);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return View(new ResponsiblesInfo());
            }
        }

        [HttpPost]
        public ActionResult UpdateResponsiblesSU(int reportFileTypeId, string executorId, string curatorId, string substituteId, int bankId)
        {
            try
            {
                var sessionUserId = HttpContext.Session.GetInt32("UserId");
                if (sessionUserId == null)
                    return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
                var status = DBCBAR.ChangeReportEmployees(reportFileTypeId, executorId, curatorId, substituteId, bankId);
                if (status != 0)
                {
                    TempData["SuccessMessage"] = "Məlumatlar uğurla yeniləndi!";
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }

            return RedirectToAction("UpdateResponsiblesSU", "Responsibles", new { id = reportFileTypeId, bankid = bankId });
        }

        [HttpPost]
        public ActionResult UpdateResponsiblesCbarUser(string fullName, string department, string position, string phone, string email, string eID)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            DBCBAR.UpdateCbarUser(fullName, department, position, phone, email, eID);

            return RedirectToAction("ResponsiblesCBAR", "Responsibles");
        }


        public ActionResult DeleteUserJob(int reportFileTypeId, int userId, int jobId, int roleId, int bankId)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            var status = DBCBAR.UpdateReportEmployee(reportFileTypeId, userId, jobId, bankId, 0);
            if (status != 0)
            {
                TempData["SuccessMessage"] = "Məlumatlar uğurla yeniləndi!";
            }
            if (bankId == 0)
            {
                return RedirectToAction("UpdateResponsiblesCBAR", "Responsibles", new { id = reportFileTypeId });
            }
            else
            {
                return RedirectToAction("UpdateResponsiblesSU", "Responsibles", new { id = reportFileTypeId, bankId = bankId });
            }

        }

        [HttpPost]
        public ActionResult UpdateResponsiblesBankUser(string fullName, string department, string position, string oPhone, string mPhone, string email, string eID, int bankId)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            DBCBAR.UpdateBankUser(fullName, department, position, oPhone, mPhone, email, eID);
            return RedirectToAction("ResponsiblesStatUnits", "Responsibles", new { su = bankId });
        }

        [HttpPost]
        public ActionResult AddResponsiblesBankUser(string fullName, string department, string position, string oPhone, string mPhone, string email, int reportFileTypeId, int jID, int bankId)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            var addedUserID = DBCBAR.AddBankUser(bankId, fullName, department, position, oPhone, mPhone, email);
            if (addedUserID > 0)
            {
                if (DBCBAR.CheckReportFileTypeHasUserByJobId(reportFileTypeId, jID, bankId) > 0)
                {
                    if (jID != 2)
                    {
                        DBCBAR.UpdateReportEmployee(reportFileTypeId, addedUserID, jID, bankId, 1);
                    }
                    else
                    {
                        DBCBAR.AddReportEmployee(reportFileTypeId, addedUserID, jID, bankId);
                    }
                }
                else
                {
                    DBCBAR.AddReportEmployee(reportFileTypeId, addedUserID, jID, bankId);
                }
                TempData["SuccessMessage"] = "Məlumatlar uğurla yeniləndi!";
            }
            else
            {
                TempData["ErrorMessage"] = "Bu adda istifadəçi artıq mövcuddur!";
            }
            return RedirectToAction("UpdateResponsiblesSU", "Responsibles", new { id = reportFileTypeId, bankid = bankId });
        }

        [HttpGet]
        public ActionResult CertUsers(int? cu, string[] bankType)
        {
            var sessionUserId = HttpContext.Session.GetInt32("UserId");
            if (sessionUserId == null)
                return RedirectToAction("Login", "Home", new { ReturnUrl = Request.Path + Request.QueryString });
            if (bankType == null)
            {
                bankType = new string[] { "1" }
;
            }
            var bankTypeList = DBCBAR.GetBankTypeList((int)sessionUserId);
            var openedBankTypeList = bankTypeList.Where(x => x.Id != 5).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
            ViewBag.BankTypeList = openedBankTypeList.OrderBy(x => x.Text).ToList();
            var BankList = DBCBAR.GetBankList((int)sessionUserId, bankType).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            ViewBag.BankList = BankList;

            int firstBank = BankList.Count > 0 ? Convert.ToInt32(BankList[0].Value) : -1;
            int bankID = cu.HasValue ? cu.Value : firstBank;
            var users = DBCBAR.GetCertUsers(bankID);

            return View(users);
        }
    }
}
