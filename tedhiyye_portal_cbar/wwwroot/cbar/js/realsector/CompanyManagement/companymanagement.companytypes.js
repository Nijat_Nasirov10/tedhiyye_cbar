﻿$(document).ready(function () {
    var url_companyType_table = $("#url-companyType").data('table');
    var url_companyType_edit = $("#url-companyType").data('edit');
    var url_companyType_delete = $("#url-companyType").data('delete');
    var url_companyType_companyClass = $("#url-companyType").data('companyclass');

    var companyTypeOTable = $('#companytypes_datatable').DataTable({
        "ajax": {
            "url": url_companyType_table,
            "type": "get",
            "datatype": "json"
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "sPaginationType": "numbers",
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [{ "orderable": false, "targets": [1, 2] }],
        "columns": [
            { "data": "Type" },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="edit btn btn-primary btn-block" href="' + url_companyType_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>';
                },
                "width": "30px"
            },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="delete btn btn-danger btn-block" data-id="' + data + '" href="' + url_companyType_delete + '"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                "width": "30px"
            }
        ]
    })


    $('.companyType-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpencompanyTypePopup($(this).attr('href'));
    })

    $('.companyType-table-container').on('click', 'a.delete', function (e) {
        e.preventDefault();
        if (confirm("Məlumat ləğv edilsin?")) {
            e.preventDefault();

            $.post($(this).attr('href'), { companyTypeId: $(this).attr('data-id') }, function (data) {
                companyTypeOTable.ajax.reload(null, false);
            });
        }
        else {
            return false;
        }
    })

    $('#companytypes_datatable tbody').on('click', 'td', function (e) {
        if (!$(this).has('a, input, button').length) {
            var tr = $(this).closest('tr');

            if (tr.hasClass('selected')) {
                tr.removeClass('selected');
                ClearChildContent()

            }
            else {
                companyTypeOTable.$('tr.selected').removeClass('selected');
                tr.addClass('selected');
                LoadCompanyClasses(url_companyType_companyClass + '?companyTypeId=' + companyTypeOTable.row(tr).data().Id)
            }
        }
    });

    function LoadCompanyClasses(url) {
        $.get(url, function (data) {
            $("#companyclass-holder").html(data);

            $(document).ready(OnLoadCompanyClasses);

            goToByScroll('companyclass-holder');
        });
    }

    var companyTypeFormId = '#companyType-content';
    function OpencompanyTypePopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $(companyTypeFormId, $pageContent).removeData('validator');
            $(companyTypeFormId, $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });

        $(".dialog").dialog("close");
        $dialog_companyType = $('<div class="dialog-companyType dialog"></div>')
            .html($pageContent)
            .dialog({
                draggable: dialog_options.draggable,
                autoOpen: dialog_options.autoOpen,
                resizable: dialog_options.resizable,
                modal: dialog_options.modal,
                height: dialog_options.height,
                width: 700,
                position: dialog_options.position,
                title: 'Fəaliyyət növü',
                buttons: {
                    "Yadda saxla": function () {
                        if (!$(companyTypeFormId).valid()) {
                            return false;
                        }
                        var url = $(companyTypeFormId)[0].action;
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $(companyTypeFormId).serialize(),
                            success: function (data) {
                                if (data.status) {
                                    $dialog_companyType.dialog('destroy').remove();
                                    companyTypeOTable.ajax.reload(null, false);
                                    ClearChildContent();
                                }
                            }
                        })
                    },
                    "Cancel": function () {
                        $dialog_companyType.dialog('destroy').remove();
                    }
                }
            })

        $dialog_companyType.dialog('open');
    }
})
