﻿namespace tedhiyye_portal_shared.Domains
{
    public class Bank
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BANK_TYPE_ID { get; set; }
        public bool IS_CLOSED { get; set; }
    }
}
