﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankQuestionDetail
    {

        public BankQuestionDetail()
        {
            BankAnswers = new List<BankAnswer>();
        }
        public int ID { get; set; }
        public int BANK_QUESTION_ID { get; set; }
        public int TIME_PERIOD_ID { get; set; }
        public int ANSWER_GROUP_ID { get; set; }

        public BankQuestion BankQuestion { get; set; }
        public BankTimePeriod BankTimePeriod { get; set; }

        public List<BankAnswer> BankAnswers { get; set; }

        public int AnswerId { get; set; }
        public int QuestionAnswerId { get; set; }
    }
}
