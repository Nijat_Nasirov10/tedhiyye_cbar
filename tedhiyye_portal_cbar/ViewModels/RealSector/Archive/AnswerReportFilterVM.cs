﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;

namespace tedhiyye_portal_cbar.ViewModels.RealSector.Archive
{
    public class AnswerReportFilterVM
    {
        public IEnumerable<SelectListItem> Years{ get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
    }
}