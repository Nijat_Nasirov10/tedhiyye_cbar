﻿using System;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("COMPANIES")]
    public class Company
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("COMPANYTYPEID")]
        
        public int? CompanyTypeId { get; set; }

        [Column("COMPANYCLASSID")]
        public int? CompanyClassId { get; set; }

        [Column("COMPANY")]
        public string Name { get; set; }

        [Column("CODE")]
        public string Code { get; set; }

        [Column("ADDRESS")]
        public string Address { get; set; }

        [Column("PHONE")]
        public string Phone { get; set; }

        [Column("FAX")]
        public string Fax { get; set; }

        [Column("EMAIL")]
        public string Email { get; set; }

        [Column("DESCRIPTION")]
        public string Description { get; set; }

        [Column("BOSS")]
        public string Boss { get; set; }

        [Column("WEBSITE")]
        public string WebSite { get; set; }

        public CompanyType CompanyType { get; set; }
        public CompanyClass CompanyClass { get; set; }
        public ICollection<User> Users { get; set; }
    }
}