﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
//using System.Data.Entity;

using tedhiyye_portal_shared.Domains.RealSector.DatabaseContext;
using tedhiyye_portal_shared.Domains.RealSector.Models;

namespace tedhiyye_portal_shared.Domains.RealSector.Services
{
    public class CompanyManagementService : BaseService
    {
        public CompanyManagementService(ApplicationContext context)
            : base(context)
        {

        }

        #region Companies

        public object GetCompany(int? companyId)
        {
            if (!companyId.HasValue)
                throw new ArgumentNullException();

            return _context.Companies
                .Include(src => src.CompanyType)
                .Include(src => src.CompanyClass.CompanyType)
                .SingleOrDefault(src => src.Id == companyId);
        }

        public Company GetCompany(int? companyId, IReadOnlyCollection<int> accessedCompanyTypes)
        {
            if (!companyId.HasValue)
                throw new ArgumentNullException();

            return _context.Companies
                .Where(src => accessedCompanyTypes.Contains(src.CompanyTypeId.Value))
                .Include(src => src.CompanyType)
                .Include(src => src.CompanyClass.CompanyType)
                .SingleOrDefault(src => src.Id == companyId);
        }

        public IEnumerable<Company> GetCompanies(IReadOnlyCollection<int> accessedCompanyTypes)
        {
            return _context.Companies
                .Where(src => accessedCompanyTypes.Contains(src.CompanyTypeId.Value))
                .Include(src => src.CompanyType)
                .Include(src => src.CompanyClass.CompanyType)
                .Include(src => src.CompanyClass.Parent)
                .ToList();
        }

        public bool CreateCompany(Company company)
        {
            if (company == null)
                throw new ArgumentNullException();

            var companies = _context.Companies;
            if (companies.Any(src => src.Name == company.Name))
                return false;

            if (company.CompanyClassId != null)
            {
                company.CompanyTypeId = _context.CompanyClasses
                .SingleOrDefault(src => src.Id == company.CompanyClassId)
                .CompanyTypeId;
            }

            companies.Add(company);

            return _context.SaveChanges() > 0;
        }

        public bool EditCompany(Company company)
        {
            if (company == null)
                throw new ArgumentNullException();

            var companies = _context.Companies;
            if (companies.Any(src => src.Id == company.Id))
                if (!companies.Any(src => src.Id != company.Id && src.Name == company.Name))
                {
                    if (company.CompanyClassId != null)
                    {
                        company.CompanyTypeId = _context.CompanyClasses
                        .SingleOrDefault(src => src.Id == company.CompanyClassId)
                        .CompanyTypeId;
                    }

                    companies.Attach(company);
                    var companyEntry = _context.Entry(company);
                    companyEntry.State = EntityState.Modified;

                    return _context.SaveChanges() > 0;
                }

            return false;
        }

        public bool RemoveCompany(int? companyId)
        {
            if (!companyId.HasValue)
                throw new ArgumentNullException();

            var company = _context.Companies.Find(companyId);
            if (company == null)
                return false;

            _context.Companies.Remove(company);
            return _context.SaveChanges() > 0;
        }

        public IEnumerable<CompanyClass> GetCompanyClasses()
        {
            return _context.CompanyClasses
                .Include(src => src.CompanyType)
                .Include(src => src.Parent)
                .AsEnumerable();
        }

        public IEnumerable<CompanyClass> GetCompanyClasses(IReadOnlyCollection<int> accessedCompanyTypes)
        {
            return _context.CompanyClasses
                .Where(src => accessedCompanyTypes.Contains(src.CompanyTypeId))
                .Include(src => src.CompanyType)
                .Include(src => src.Parent)
                .AsEnumerable();
        }

        #endregion

        #region Users

        public User GetUser(int? userId)
        {
            if (!userId.HasValue)
                throw new ArgumentNullException();

            return _context.Users
                .Include(src => src.Company)
                .SingleOrDefault(src => src.Id == userId);
        }

        public IEnumerable<User> GetUsers(int? companyId)
        {
            var users = _context.Users.AsQueryable();

            if (companyId.HasValue)
                users = users.Where(src => src.CompanyId == companyId);

            return users.AsEnumerable();
        }

        public bool CreateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException();

            var users = _context.Users;
            if (users.Any(src => src.Username == user.Username))
                return false;

            user.Password = Tools.CalculateSHA512Hash(user.Password);
            users.Add(user);

            return _context.SaveChanges() > 0;
        }

        public bool EditUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException();

            var users = _context.Users;
            var dbUser = users.Find(user.Id);

            var a = users.Any(src => src.Id != user.Id && src.Username == user.Username);
            if (dbUser != null && dbUser.CompanyId == user.CompanyId)
                if (!users.Any(src => src.Id != user.Id && src.Username == user.Username))
                {
                    if (user.Password == null)
                        user.Password = dbUser.Password;
                    else
                        user.Password = Tools.CalculateSHA512Hash(user.Password);

                    _context.Entry(dbUser).CurrentValues.SetValues(user);
                    return _context.SaveChanges() > 0;
                }

            return false;
        }

        public bool RemoveUser(int? userId)
        {
            if (!userId.HasValue)
                throw new ArgumentNullException();

            var user = _context.Users.Find(userId);
            if (user == null)
                return false;

            _context.Users.Remove(user);
            return _context.SaveChanges() > 0;
        }

        #region Helpers

        #endregion
        #endregion

        #region CompanyTypes

        public CompanyType GetCompanyType(int? companyTypeId)
        {
            if (!companyTypeId.HasValue)
                throw new ArgumentNullException();

            return _context.CompanyTypes
                .Include(src => src.CompanyClasses.Select(cc => cc.Parent))
                .SingleOrDefault(src => src.Id == companyTypeId);
        }

        public IEnumerable<CompanyType> GetCompanyTypes()
        {
            return _context.CompanyTypes
                .Include(src => src.CompanyClasses.Select(cc => cc.Parent))
                .ToList();
        }

        public bool CreateCompanyType(CompanyType companyType)
        {
            if (companyType == null)
                throw new ArgumentNullException();

            var companyTypes = _context.CompanyTypes;
            if (companyTypes.Any(src => src.Type == companyType.Type))
                return false;

            companyTypes.Add(companyType);

            return _context.SaveChanges() > 0;
        }

        public bool EditCompanyType(CompanyType companyType)
        {
            if (companyType == null)
                throw new ArgumentNullException();

            var companyTypes = _context.CompanyTypes;
            if (companyTypes.Any(src => src.Id == companyType.Id))
                if (!companyTypes.Any(src => src.Id != companyType.Id && src.Type == companyType.Type))
                {
                    companyTypes.Attach(companyType);
                    var companyEntry = _context.Entry(companyType);
                    companyEntry.State = EntityState.Modified;

                    return _context.SaveChanges() > 0;
                }

            return false;
        }

        public bool RemoveCompanyType(int? companyTypeId)
        {
            if (!companyTypeId.HasValue)
                throw new ArgumentNullException();

            var companyType = _context.CompanyTypes.Find(companyTypeId);
            if (companyType == null)
                return false;

            _context.CompanyTypes.Remove(companyType);
            return _context.SaveChanges() > 0;
        }

        #endregion

        #region CompanyClasses
        public CompanyClass GetCompanyClass(int? companyClassId)
        {
            if (!companyClassId.HasValue)
                throw new ArgumentNullException();

            return _context.CompanyClasses
                .Where(src => src.Id == companyClassId)
                .SingleOrDefault(src => src.Id == companyClassId);
        }

        public IEnumerable<CompanyClass> GetCompanyClassessList(int? companyTypeId)
        {
            if (!companyTypeId.HasValue)
                throw new ArgumentNullException();

            return _context.CompanyClasses
                   .Include(src => src.Parent)
                   .Where(src => src.CompanyTypeId == companyTypeId)
                   .AsEnumerable();
        }

        public bool CreateCompanyClass(CompanyClass companyClass)
        {
            if (companyClass == null)
                throw new ArgumentNullException();

            var companyClasses = _context.CompanyClasses;
            if (companyClasses.Any(src => src.Name == companyClass.Name))
                return false;

            companyClasses.Add(companyClass);

            return _context.SaveChanges() > 0;
        }

        public bool EditCompanyClass(CompanyClass companyClass)
        {
            if (companyClass == null)
                throw new ArgumentNullException();

            var companyClasses = _context.CompanyClasses;

            if (companyClasses.Any(src => src.Id == companyClass.Id))
                if (!companyClasses.Any(src => src.Id != companyClass.Id && src.Name == companyClass.Name && src.ParentId == companyClass.ParentId))
                {
                    companyClasses.Attach(companyClass);
                    var companyEntry = _context.Entry(companyClass);
                    companyEntry.State = EntityState.Modified;

                    return _context.SaveChanges() > 0;
                }

            return false;
        }

        public bool RemoveCompanyClass(int? companyClassId)
        {
            if (!companyClassId.HasValue)
                throw new ArgumentNullException();

            var companyClass = _context.CompanyClasses.Find(companyClassId);
            if (companyClass == null)
                return false;

            _context.CompanyClasses.Remove(companyClass);
            return _context.SaveChanges() > 0;
        }

        public IEnumerable<CompanyClass> GetCurrentCompanyClasses(int? companyTypeId)
        {            
            return _context.CompanyClasses
                .Where(src => src.CompanyType.Id == companyTypeId)
                .AsEnumerable();
        }
        #endregion
    }


}
