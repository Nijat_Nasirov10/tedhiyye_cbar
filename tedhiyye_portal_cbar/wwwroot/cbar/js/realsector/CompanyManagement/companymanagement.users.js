﻿function OnLoadUsers() {
    var url_user_table = $("#url-user").data('table');
    var url_user_edit = $("#url-user").data('edit');
    var url_user_delete = $("#url-user").data('delete');

    var $users_table = $("#users_datatable")
    var companyId = $users_table.data('companyid');
    var userOTable = $users_table.DataTable({
        "ajax": {
            "url": url_user_table + '?companyId=' + companyId,
            "type": "get",
            "datatype": "json"
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [{ "orderable": false, "targets": [2, 3, 4] }],
        "columns": [
            { "data": "UserName" },
            { "data": "Email" },
            {
                "data": "Password", "render": function (data) {
                    return '********';
                },
            },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="edit btn btn-primary btn-block" href="' + url_user_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>';
                },
                "width": "30px"
            },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="delete btn btn-danger btn-block" data-id="' + data + '" href="' + url_user_delete + '"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                "width": "30px"
            }
        ]
    })

    $('.user-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpenUserPopup($(this).attr('href'));
    })

    $('.user-table-container').on('click', 'a.delete', function (e) {
        e.preventDefault();
        if (confirm("Məlumat ləğv edilsin?")) {
            e.preventDefault();

            $.post($(this).attr('href'), { userId: $(this).attr('data-id') }, function (data) {
                userOTable.ajax.reload(null, false);
            });
        }
        else {
            return false;
        }
    })

    var userFormId = '#user-content';
    function OpenUserPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $('#company-content', $pageContent).removeData('validator');
            $('#company-content', $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse(userFormId);
        });

        $(".dialog").dialog("close");
        $dialog_user = $('<div class="dialog-user dialog"></div>')
                  .html($pageContent)
                  .dialog({
                      draggable: dialog_options.draggable,
                      autoOpen: dialog_options.autoOpen,
                      resizable: dialog_options.resizable,
                      modal: dialog_options.modal,
                      height: dialog_options.height,
                      width: dialog_options.width,
                      position: dialog_options.position,
                      title: 'İstifadəçi',
                      buttons: {
                          "Yadda saxla": function () {
                              if (!$(userFormId).valid()) {
                                  return false;
                              }
                              var url = $(userFormId)[0].action;
                              $.ajax({
                                  type: "POST",
                                  url: url,
                                  data: $(userFormId).serialize(),
                                  success: function (data) {
                                      if (data !== null) {
                                          $dialog_user.dialog('destroy').remove();
                                          userOTable.ajax.reload(null, false);
                                      }
                                  }
                              })
                          },
                          "Cancel": function () {
                              $dialog_user.dialog('destroy').remove();
                          }
                      }
                  })

        $dialog_user.dialog('open');
    }
}
