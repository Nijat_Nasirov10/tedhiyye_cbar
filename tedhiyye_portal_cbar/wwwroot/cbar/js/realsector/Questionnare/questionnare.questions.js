﻿function OnLoadQuestions() {
    var url_question_table = $("#url-question").data('table');
    var url_question_edit = $("#url-question").data('edit');
    var url_question_delete = $("#url-question").data('delete');
    var url_question_status = $("#url-question").data('status')
    var url_question_answers = $("#url-question").data('answers');

    var $questions_table = $("#questions_datatable")
    var formId = $questions_table.data('formid');
    var questionOTable = $questions_table.DataTable({
        "ajax": {
            "url": url_question_table + '?formId=' + formId,
            "type": "get",
            "datatype": "json"
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [{ "orderable": false, "targets": [2, 3, 4] }],
        "columns": [
            {
                "data": "QuestionText",
                "width": "60%"
            },
            { "data": "OrderId", "width": "70px", "className":"text-center"},
            {
                "data": "IsPrivate", "render": function (data) {
                    if (data)
                        return '<span class="glyphicon glyphicon-ok" style="color:#03a9d9"></span>';
                    else
                        return '';
                },
                "width": "90px",
                "className": "text-center"

            },
            {
                "data": "IsRequired", "render": function (data) {
                    if (data)
                        return '<span class="glyphicon glyphicon-ok" style="color:#03a9d9"></span>';
                    else
                        return '';
                },
                "width": "90px",
                "className": "text-center"
            },
            {
                "data": "Id", "render": function (data) {
                    var edit = '<a class="edit btn btn-primary btn-block" href="' + url_question_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>'
                    return edit;
                },
                "width": "30px"
            },
            {
                "data": "Id", "render": function (data, type, row, meta) {
                    var status_active = '<a class="status btn btn-success btn-block" title = "Activ Et"  data-id="' + data + '" href="' + url_question_status + '"><span class="glyphicon glyphicon-ok"></span></a>';
                    var status_deactive = '<a class="status btn btn-danger btn-block" title = "Deactiv Et" data-id="' + data + '" href="' + url_question_status + '"><span class="glyphicon glyphicon-ban-circle"></span></a>';

                    if (row.IsActive)
                        return status_deactive;
                    else {
                        return status_active;
                    }
                },
                "width": "30px"
            }
        ]
    })

    $('#questions_datatable tbody').on('click', 'td', function () {
        if (!$(this).has('a, input, button').length) {
            var tr = $(this).closest('tr');
            if (tr.hasClass('selected')) {
                tr.removeClass('selected');
                ClearChildContent();
            }
            else {
                questionOTable.$('tr.selected').removeClass('selected');
                tr.addClass('selected');
                LoadAnswers(url_question_answers + '?questionId=' + questionOTable.row(tr).data().Id)
            }
        }
    });

    function LoadAnswers(url) {
        $.get(url, function (data) {
            $("#answers-holder").html(data);
            $(document).ready(OnLoadAnswers);
            goToByScroll('answers-holder');
        });
    }

    $('.question-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpenQuestionPopup($(this).attr('href'));
    })

    $('.question-table-container').on('click', 'a.status', function (e) {
        e.preventDefault();

        $.post($(this).attr('href'), { questionId: $(this).attr('data-id') }, function (data) {
            questionOTable.ajax.reload(null, false);
            ClearChildContent();
        });
    })

    function ClearChildContent() {
        $("#answers-holder").html('');
    }

    var questionFormId = '#question-content';
    function OpenQuestionPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $(questionFormId, $pageContent).removeData('validator');
            $(questionFormId, $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });

        $(".dialog").dialog("close");
        $dialog = $('<div class="dialog-question dialog"></div>')
            .html($pageContent)
            .dialog({
                draggable: dialog_options.draggable,
                autoOpen: dialog_options.autoOpen,
                resizable: dialog_options.resizable,
                modal: dialog_options.modal,
                height: dialog_options.height,
                width: dialog_options.width,
                position: dialog_options.position,
                title: 'Sual',
                buttons: {
                    "Yadda saxla": function () {
                        if (!$(questionFormId).valid()) {
                            return false;
                        }
                        var url = $(questionFormId)[0].action;
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $(questionFormId).serialize(),
                            success: function (data) {
                                if (data !== null) {
                                    $dialog.dialog('destroy').remove();
                                    questionOTable.ajax.reload(null, false);
                                }
                            }
                        })
                    },
                    "Cancel": function () {
                        $dialog.dialog('destroy').remove();
                    }
                }
            })

        $dialog.dialog('open');
    }
}
