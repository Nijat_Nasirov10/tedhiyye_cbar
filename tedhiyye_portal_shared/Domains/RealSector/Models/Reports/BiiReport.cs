﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.RealSector.Models.Reports
{
	public class BiiReport
	{
		public string Title { get; set; }
		public string CompanyType { get; set; }
		public string CompanyClass { get; set; }
		public List<BiiData> DataByCompanyType { get; set; }
		public List<BiiData> DataByCompanyClass { get; set; }
	}

	public static class BiiReportSettings
	{
		public static int DefaultMonthsCount = 12;
	}


}
