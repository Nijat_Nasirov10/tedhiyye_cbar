﻿$(document).ready(function () {
    var url_form_table = $("#url-form").data('table');
    var url_form_edit = $("#url-form").data('edit');
    var url_form_delete = $("#url-form").data('delete');
    var url_form_questions = $("#url-form").data('questions');

    var formTable = $('#forms_datatable').DataTable({
        "ajax": {
            "url": url_form_table,
            "type": "get",
            "datatype": "json"
        },
        "processing": datatables_options.processing,
        "lengthMenu": datatables_options.lengthMenu,
        "language": datatables_options.language,
        "dom": datatables_options.dom,
        "autoWidth": false,
        "columnDefs": [ { "orderable": false, "targets": [3,4] }],
        "columns": [
            {
                "data": "Name"
            },
            { "data": "CompanyType", "width": "100px" },
            { "data": "OrderId", "width": "30px" },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="edit btn btn-primary btn-block" href="' + url_form_edit + '/' + data + '"><span class="glyphicon glyphicon-pencil"></span><a>';
                },
                "width": "30px"
            },
            {
                "data": "Id", "render": function (data) {
                    return '<a class="delete btn btn-danger btn-block" data-id="' + data + '" href="' + url_form_delete + '"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                "width": "30px"
            }
        ]
    })

    $('#forms_datatable tbody').on('click', 'td', function (e) {
        if (!$(this).has('a, input, button').length) {
            var tr = $(this).closest('tr');

            if (tr.hasClass('selected')) {
                tr.removeClass('selected');
                ClearChildContent()

            }
            else {
                formTable.$('tr.selected').removeClass('selected');
                tr.addClass('selected');
                LoadQuestions(url_form_questions + '?formId=' + formTable.row(tr).data().Id)
            }
        }
    });

    function LoadQuestions(url) {
        $.get(url, function (data) {
            $("#questions-holder").html(data);
            $("#answers-holder").html('');

            $(document).ready(OnLoadQuestions);

            goToByScroll('questions-holder');
        });
    }

    $('.form-table-container').on('click', 'a.create, a.edit', function (e) {
        e.preventDefault();
        OpenFormPopup($(this).attr('href'));
    })

    $('.form-table-container').on('click', 'a.delete', function (e) {
        e.preventDefault();
        if (confirm("Məlumat ləğv edilsin?")) {
            e.preventDefault();

            $.post($(this).attr('href'), { formId: $(this).attr('data-id') }, function (data) {
                formTable.ajax.reload(null, false);
                ClearChildContent();
            });
        }
        else {
            return false;
        }
    })

    function ClearChildContent() {
        $("#questions-holder").html('');
        $("#answers-holder").html('');
    }

    var formId = '#form-content';
    function OpenFormPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $(formId, $pageContent).removeData('validator');
            $(formId, $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });

        $(".dialog").dialog("close");
        $dialog_form = $('<div class="dialog-form dialog"></div>')
                  .html($pageContent)
                  .dialog({
                      draggable: dialog_options.draggable,
                      autoOpen: dialog_options.autoOpen,
                      resizable: dialog_options.resizable,
                      modal: dialog_options.modal,
                      height: dialog_options.height,
                      width: dialog_options.width,
                      position: dialog_options.position,
                      title: 'Sorğu',
                      buttons: {
                          "Yadda saxla": function () {
                              if (!$(formId).valid()) {
                                  return false;
                              }
                              var url = $(formId)[0].action;
                              $.ajax({
                                  type: "POST",
                                  url: url,
                                  data: $(formId).serialize(),
                                  success: function (data) {
                                      if (data !== null) {
                                          $dialog_form.dialog('destroy').remove();
                                          formTable.ajax.reload(null, false);
                                      }
                                  }
                              })
                          },
                          "Cancel": function () {
                              $dialog_form.dialog('destroy').remove();
                          }
                      }
                  })

        $dialog_form.dialog('open');
    }
})
