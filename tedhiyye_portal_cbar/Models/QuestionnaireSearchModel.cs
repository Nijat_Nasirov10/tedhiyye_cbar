﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tedhiyye_portal_cbar.Models
{
    public class QuestionnaireSearchModel
    {
        public int Page { get; set; }
        public string SendDate { get; set; }
        public string CompanyId { get; set; }
        public string CompanyClassId { get; set; }
    }
}