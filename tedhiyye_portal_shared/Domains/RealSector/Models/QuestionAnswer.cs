﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("ANSWER")]
    public class QuestionAnswer
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("ANSWER")]
        public string Answer { get; set; }

        [Column("QUESTIONID")]
        [ForeignKey("Question")]
        public int QuestionId { get; set; }

        [Column("POINT")]
        public int? Point { get; set; }

        [Column("ORDERID")]
        public int? OrderId { get; set; }

        public virtual Question Question { get; set; }
        public virtual List<UserAnswerDetail> UserAnswerDetails { get; set; }
    }
}