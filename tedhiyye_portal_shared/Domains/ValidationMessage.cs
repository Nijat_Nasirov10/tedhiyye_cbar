﻿
namespace tedhiyye_portal_shared.Domains
{
    public static class ValidationMessage
    {
        public const string Email = "Zəhmət olmasa, düzgün elektron poçt daxil edin";
        public const string Required = "{0} daxil edilməyib";
    }
}
