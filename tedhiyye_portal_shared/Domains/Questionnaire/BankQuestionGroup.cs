﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.Questionnaire
{
    public class BankQuestionGroup
    {

        public BankQuestionGroup()
        {
            BankQuestions = new List<BankQuestion>();
        }
        public int ID { get; set; }
        public string QUESTION_GROUP_TITLE { get; set; }
        public string QUESTION_TITLE_ID { get; set; }
        public bool IS_HIDDEN { get; set; }


        public List<BankQuestion> BankQuestions { get; set; }
    }
}
