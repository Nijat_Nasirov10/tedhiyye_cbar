﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar.Models
{
    public class XlRulesFormula
    {
        public string FormulaCode { get; set; }
        public int Status { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportFileTypeId { get; set; }
        public string RoundSize { get; set; }
        public string groupName { get; set; }
        public string FormulaText { get; set; }//RuleText
        public string FieldFunction { get; set; }
        public string CellText { get; set; }
        public string WhereString { get; set; }
        public int PreviousMonth { get; set; }
        public string FormulaCodeForView { get; set; }
        public Dictionary<string, string> splittedFormulaCode { get; set; }
        public Dictionary<string, string> splittedFormulaCodeForView { get; set; }
        public string[] splitOperators { get; set; }
        public List<int> XlsSheetId { get; set; }
    }
}
