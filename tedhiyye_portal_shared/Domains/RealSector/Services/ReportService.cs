﻿using System.Collections.Generic;
using System.Linq;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using tedhiyye_portal_shared.Domains.RealSector.DatabaseContext;
using tedhiyye_portal_shared.Domains.RealSector.Models.Reports;
using System;
using tedhiyye_portal_shared.Domains.RealSector.Models;
using System.Data.Common;

namespace tedhiyye_portal_shared.Domains.RealSector.Services
{
	public class ReportService : BaseService
	{
		public ReportService(ApplicationContext context)
			: base(context)
		{
			//context.Database.Log = Console.Write;
		}

		#region AnswersReport

		public AnswerReport GetAnswerReports(IReadOnlyCollection<int> accessedCompanyTypes, int? year, int? month, int skip = -1, int take = -1)
		{
			var queryableData = GetAnswerQueryableData(year, month)
				.Where(src => !src.CompanyTypeId.HasValue || accessedCompanyTypes.Contains(src.CompanyTypeId.Value));

			IQueryable<AnswerReportItem> reports = queryableData
			.OrderBy(src => src.CompanyClass)
			.ThenBy(src => src.CId)
			.ThenBy(src => src.QuestionNumber);

			if (skip != -1)
				reports = reports.Skip(skip);
			if (take != -1)
				reports = reports.Take(take);

			var data = queryableData.ToList();


			return new AnswerReport { Data = reports.AsEnumerable(), Count = queryableData.Count() };
		}

		private IQueryable<AnswerReportItem> GetAnswerQueryableData(int? year, int? month)
		{
			return from user in _context.Users
				   join company in _context.Companies on user.CompanyId equals company.Id
				   join userAnswer in _context.UserAnswers on user.Id equals userAnswer.UserId
				   join companyClass in _context.CompanyClasses on company.CompanyClassId equals companyClass.Id
				   join question in _context.Questions on userAnswer.FormId equals question.FormId
				   join userAnswerDetail in _context.UserAnswerDetails on new { p1 = userAnswer.Id, p2 = question.Id } equals
																		  new { p1 = userAnswerDetail.UserAnswerId, p2 = userAnswerDetail.QuestionId }
				   join questionAnswer in _context.QuestionAnswers on userAnswerDetail.QuestionAnswerId equals questionAnswer.Id into form
				   from questionAnswer in form.DefaultIfEmpty()
				   where (userAnswer.AnswerDate.Year == year && userAnswer.AnswerDate.Month == month) ||
						 (year == null && month == null)
				   select new AnswerReportItem
				   {
					   CompanyClass = companyClass.Name,
					   CId = company.Id,
					   Company = company.Name,
					   Area = company.Code.Substring(0, 3),
					   SequenceNo = company.Code.Substring(3, 4),
					   Ifnt = company.Code.Substring(company.Code.Length - 3),
					   CompanyTypeId = companyClass.CompanyTypeId,
					   CompanyClassId = companyClass.Id,
					   PrivateAnswer = userAnswerDetail.PrivateAnswer,
					   QuestionNumber = userAnswerDetail.QuestionId - 11,
					   Answer = questionAnswer.Answer,
					   Point = questionAnswer.Point
				   };
		}

		#endregion

		#region BiiReport

		public BiiReport GetBiiReportForLastMonths(int? companyClassId)
		{
			CompanyClass companyClassInfo = ValidateBiiReport(companyClassId);
			var report = new BiiReport();
			report.Title = "Biznes İnam İndeksi - ";
			report.Title += $"son {BiiReportSettings.DefaultMonthsCount + 1} ay";
			report.CompanyType = $"İcmal({companyClassInfo.CompanyType.Type}) sektoru";
			report.CompanyClass = companyClassInfo.Name;

			DateTime dt = DateTime.Now;
			var endDate = new DateTime(dt.Year, dt.Month, 1);
			var startDate = endDate.AddMonths(-(BiiReportSettings.DefaultMonthsCount + 1));

			var biiDataForCompanyClass = GetBiiByCompanyClassData(companyClassInfo.Id, startDate, endDate);

			report.DataByCompanyClass = biiDataForCompanyClass
				.GroupBy(g => new { g.Year, g.Month },
						(k, v) => new BiiData { Year = k.Year, Month = k.Month, Points = v.Sum(p => p.Points) })
				.OrderBy(x => x.Year)
				.ThenBy(x => x.Month)
				.ToList();

			var cTypeStartDate = biiDataForCompanyClass.Min(x => new DateTime(x.Year, x.Month, 1));
			var cTypeEndDate = biiDataForCompanyClass.Max(x => new DateTime(x.Year, x.Month, 1));
			var biiDataForCompanyType = GetBiiByCompanyTypeData(companyClassInfo.CompanyTypeId, cTypeStartDate, cTypeEndDate);

			report.DataByCompanyType = biiDataForCompanyType
				.GroupBy(g => new { g.Year, g.Month },
						(k, v) => new BiiData { Year = k.Year, Month = k.Month, Points = v.Sum(p => p.Points) })
				.OrderBy(x => x.Year)
				.ThenBy(x => x.Month)
				.ToList();

			return report;
		}

		public BiiReport GetBiiReportForYear(int? companyClassId, int year)
		{
			CompanyClass companyClassInfo = ValidateBiiReport(companyClassId);

			var report = new BiiReport();
			report.Title = "Biznes İnam İndeksi - ";
			report.Title += year.ToString();
			report.CompanyType = $"İcmal({companyClassInfo.CompanyType.Type}) sektoru";
			report.CompanyClass = companyClassInfo.Name;

			var biiDataForCompanyClass = GetBiiByCompanyClassData(companyClassInfo.Id, year);

			report.DataByCompanyClass = biiDataForCompanyClass
				.GroupBy(g => new { g.Year, g.Month },
						(k, v) => new BiiData { Year = k.Year, Month = k.Month, Points = v.Sum(p => p.Points) })
				.OrderBy(x => x.Year)
				.ThenBy(x => x.Month)
				.ToList();

			var biiDataForCompanyType = GetBiiByCompanyTypeData(companyClassInfo.CompanyTypeId, year);

			report.DataByCompanyType = biiDataForCompanyType
				.GroupBy(g => new { g.Year, g.Month },
						(k, v) => new BiiData { Year = k.Year, Month = k.Month, Points = v.Sum(p => p.Points) })
				.OrderBy(x => x.Year)
				.ThenBy(x => x.Month)
				.ToList();

			return report;
		}

		private CompanyClass ValidateBiiReport(int? companyClassId)
		{
			if (!companyClassId.HasValue)
				throw new ArgumentNullException();

			var companyClassInfo = _context.CompanyClasses
				.Include(src => src.CompanyType)
				.Where(src => src.Id == companyClassId)
				.SingleOrDefault();

			if (companyClassInfo == null)
				throw new ArgumentNullException("Company Class can not be null");
			return companyClassInfo;
		}

		private IEnumerable<BiiData> GetBiiByCompanyClassData(int companyClassId, int year)
		{
			var result = (from vwBii in _context.VwBiisForCompanyClass
						  where vwBii.CompanyClassId.Equals(companyClassId) && vwBii.Year.Equals(year)
						  select new BiiData
						  {
							  Month = vwBii.Month,
							  Year = vwBii.Year,
							  Points = vwBii.TotalPoints
						  }).ToList();

			AddEmptyMonthData(year, result);
			return result;
		}

		private IEnumerable<BiiData> GetBiiByCompanyClassData(int companyClassId, DateTime startDate, DateTime endDate)
		{
			var result = (from vwBii in _context.VwBiisForCompanyClass
						  where vwBii.CompanyClassId.Equals(companyClassId) &&
						  vwBii.FirstDateOfMonth >= startDate &&
						  vwBii.FirstDateOfMonth <= endDate
						  select new BiiData
						  {
							  Month = vwBii.Month,
							  Year = vwBii.Year,
							  Points = vwBii.TotalPoints
						  }).ToList();

			AddEmptyMonthData(startDate, endDate, result);
			return result;
		}

		private IEnumerable<BiiData> GetBiiByCompanyTypeData(int companyTypeId, int year)
		{
			var result = (from vwBii in _context.VwBiisForCompanyType
						  where vwBii.CompanyTypeId.Equals(companyTypeId) && vwBii.Year.Equals(year)
						  select new BiiData
						  {
							  Month = vwBii.Month,
							  Year = vwBii.Year,
							  Points = vwBii.TotalPoints
						  }).ToList();

			AddEmptyMonthData(year, result);
			return result;
		}

		private IEnumerable<BiiData> GetBiiByCompanyTypeData(int companyTypeId, DateTime startDate, DateTime endDate)
		{
			var result = (from vwBii in _context.VwBiisForCompanyType
						  where vwBii.CompanyTypeId.Equals(companyTypeId) &&
						  vwBii.FirstDateOfMonth >= startDate &&
						  vwBii.FirstDateOfMonth <= endDate
						  select new BiiData
						  {
							  Month = vwBii.Month,
							  Year = vwBii.Year,
							  Points = vwBii.TotalPoints
						  }).ToList();

			AddEmptyMonthData(startDate, endDate, result);
			return result;
		}


		private static void AddEmptyMonthData(int year, List<BiiData> data)
		{
			var emptyMonthData = GetEmptyMonthDataByYear(data, year, 1, 12);
			data.AddRange(emptyMonthData);
		}

		private static void AddEmptyMonthData(DateTime startDate, DateTime endDate, List<BiiData> data)
		{
			var emptyMinMonthData = GetEmptyMonthDataByYear(data, startDate.Year, startDate.Month, 12);
			data.AddRange(emptyMinMonthData);

			if (startDate.Year != endDate.Year)
			{
				var emptyMaxMonthData = GetEmptyMonthDataByYear(data, endDate.Year, 1, endDate.Month);
				data.AddRange(emptyMaxMonthData);
			}
		}

		private static IEnumerable<BiiData> GetEmptyMonthDataByYear(List<BiiData> data, int year, int startMonth, int endMonth)
		{
			return Enumerable.Range(startMonth, endMonth - startMonth + 1)
					.Where(x => !data.Any(r => r.Month == x))
					.Select(x => new BiiData { Month = x, Year = year, Points = 0 });
		}

		#endregion
	}
}
