﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("COMPANYCLASS")]
    public class CompanyClass
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("COMPANYTYPEID")]
        [ForeignKey("CompanyType")]
        public int CompanyTypeId { get; set; }

        [Column("COMPANYCLASS")]
        public string Name { get; set; }

        [Column("PARENT_ID")]
        public int? ParentId { get; set; }

        public CompanyClass Parent { get; set; }
        public CompanyType CompanyType { get; set; }
        public IEnumerable<Company> Companies { get; set; }
    }
}
