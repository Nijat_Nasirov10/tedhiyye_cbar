﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tedhiyye_portal_cbar.Models
{
    public class CustomSelectListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Is_PHQE { get; set; }
        public int Service_Id { get; set; }
    }
}