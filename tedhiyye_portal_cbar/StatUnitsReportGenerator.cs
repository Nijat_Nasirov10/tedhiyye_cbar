﻿using tedhiyye_portal_shared.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar
{
    public class BankReportTypeInfo
    {
        public List<Report> Reports;
        public int LastReportInd;
    }

    public class StatUnitsReportGenerator
    {
        public Dictionary<string, BankReportTypeInfo> ReportsDict;
        public List<KeyValuePair<int, string>> BankList;
        public List<KeyValuePair<int, string>> ReportTypeList;

        public void Generate(List<Report> reports)
        {
            LoadReportsIntoDict(reports);
        }

        private void LoadReportsIntoDict(List<Report> reports)
        {
            ReportsDict = new Dictionary<string, BankReportTypeInfo>();
            BankList = new List<KeyValuePair<int, string>>();
            var BankSet = new HashSet<int>();
            ReportTypeList = new List<KeyValuePair<int, string>>();
            var ReportTypeSet = new HashSet<int>();

            foreach (var report in reports)
            {
                if (report.BANKID == null) continue;

                int BankId = report.BANKID.Value;
                string BankName = report.BANKNAME;
                int ReportTypeId = report.REPORTTYPEID;
                string ReportTypeName = report.REPORTTYPETEXT;
                string key = BankId.ToString() + "-" + ReportTypeId.ToString();

                // Add to dict
                if (!ReportsDict.ContainsKey(key))
                {
                    var info = new BankReportTypeInfo();
                    info.Reports = new List<Report>();
                    ReportsDict[key] = info;
                }
                ReportsDict[key].Reports.Add(report);

                // Add to bank list
                if (!BankSet.Contains(BankId))
                {
                    BankList.Add(new KeyValuePair<int, string>(BankId, BankName));
                    BankSet.Add(BankId);
                }

                // Add to report type list
                if (!ReportTypeSet.Contains(ReportTypeId))
                {
                    ReportTypeList.Add(new KeyValuePair<int, string>(ReportTypeId, ReportTypeName));
                    ReportTypeSet.Add(ReportTypeId);
                }
            }

            // Sort banks list
            BankList.Sort((b1, b2) => b1.Value.CompareTo(b2.Value));

            //Sort report type 
            ReportTypeList.Sort((b1, b2) => b1.Value.CompareTo(b2.Value));
        }

        /*private void CalculateLastReports()
        {
            foreach (KeyValuePair<string, BankReportTypeInfo> item in ReportsDict)
            {
                if (item.Value.Reports.Count>1)
                {
                    int LastReportInd = 0;
                    int maxid = item.Value.Reports[0].ID;
                    for (int i = 1; i < item.Value.Reports.Count; i++)
                    {
                        int id= item.Value.Reports[i].ID;
                        if (id > maxid)
                        {
                            LastReportInd = i;
                            maxid = id;
                        }
                    }
                    ReportsDict[item.Key].LastReportInd = LastReportInd;
                }
                else
                {
                    ReportsDict[item.Key].LastReportInd = 0;
                }
            }
        }*/
    }
}
