﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tedhiyye_portal_cbar;

namespace tedhiyye_portal_cbar
{
    public class DBLog
    {
        static string ReplaceCRLF(string text)
        {
            text = text.Replace("\r", " ");
            text = text.Replace("\n", " ");
            return text;
        }

        public static void AddInfo(string action, HttpContext context, string report = "", string data = "", string reports_file = "")
        {
            Add(action, context, report, reports_file, data, false);
        }

        public static void AddError(string action, HttpContext context, string report = "", string data = "", string reports_file = "")
        {
            Add(action, context, report, reports_file, data, true);
        }

        private static void Add(string action, HttpContext context, string report = "", string reports_file = "", string data = "", bool iserror = false)
        {
            try
            {
                string organization = (string)context.Session.GetString("BankName");
                string username = (string)context.Session.GetString("Username");
                string log_type = "info";
                if (iserror)
                    log_type = "error";
                string ip = context.Connection.RemoteIpAddress?.MapToIPv4()?.ToString();
                DBCBAR.AddLog(organization, username, report, action, data, log_type, ip, reports_file);
            }
            catch
            {
            }
        }
    }
}
