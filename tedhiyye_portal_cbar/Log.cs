﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace tedhiyye_portal_cbar
{
    public class Log
    {
        public static void AddError(string operation, string message, string data = "")
        {
            Add(operation, message, data, true);
        }

        static string ReplaceCRLF(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            text = text.Replace("\r", " ");
            text = text.Replace("\n", " ");
            return text;
        }

        public static void Add(string operation, string message, string data = "", bool iserror = false)
        {
            if (message == "Thread was being aborted.") return;

            try
            {
                string AppDataFolder = Path.Combine(Directory.GetCurrentDirectory(), "App_Data");

                if (!Directory.Exists(AppDataFolder))
                    Directory.CreateDirectory(AppDataFolder);

                string LogFolder = Path.Combine(AppDataFolder, "Log");

                if (!Directory.Exists(LogFolder))
                    Directory.CreateDirectory(LogFolder);

                string path = Path.Combine(LogFolder, DateTime.Today.ToString("yyyy-MM-dd") + ".txt");

                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }

                using (StreamWriter w = File.AppendText(path))
                {
                    if (!iserror)
                        w.Write("Info\t");
                    else
                        w.Write("Error\t");
                    w.Write("Operation: " + operation + "\t");
                    w.Write("Time: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff") + "\t");
                    w.Write("Message: " + ReplaceCRLF(message) + "\t");
                    if (data != "")
                    {
                        w.Write("Data: " + ReplaceCRLF(data) + "\t");
                    }
                    w.WriteLine(" ");
                    w.Flush();
                    w.Close();
                }
            }
            catch
            {
            }
        }
    }
}
