﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace tedhiyye_portal_shared
{
    public class Tools
	{
		public static string AzTranslit(string s)
		{
			s = s.Replace("ü", "u");
			s = s.Replace("Ü", "U");
			s = s.Replace("ö", "o");
			s = s.Replace("Ö", "O");
			s = s.Replace("ğ", "g");
			s = s.Replace("Ğ", "G");
			s = s.Replace("ı", "i");
			s = s.Replace("İ", "I");
			s = s.Replace("ə", "e");
			s = s.Replace("Ə", "E");
			s = s.Replace("ç", "c");
			s = s.Replace("Ç", "C");
			s = s.Replace("ş", "s");
			s = s.Replace("Ş", "S");

			return s;
		}

		public static string AzUppercase(string s)
		{
			s = s.Replace("i", "İ");
			s = s.Replace("ı", "I");
			return s.ToUpper();
		}

		public static string CalculateSHA512Hash(string input)
		{
			SHA512 sha512 = SHA512.Create();
			byte[] inputBytes = Encoding.UTF8.GetBytes(input + "p2?R=*3eX5g2RUyCYUkh6]khLKJ*jplLG^&N7bilu6965lrJ8mfj494j-3jd");
			byte[] hash = sha512.ComputeHash(inputBytes);

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				sb.Append(hash[i].ToString("X2"));
			}
			return sb.ToString();
		}

		public static string GetReportUploadFileStatusCssClass(ReportUploadFileStatus status)
		{
			string cssClass = "";
			if (status == ReportUploadFileStatus.ErrorValidating || status == ReportUploadFileStatus.ErrorConvertingToXML || status == ReportUploadFileStatus.ErrorInsertingToDB)
			{
				cssClass = "text-danger";
			}
			else
			if (status == ReportUploadFileStatus.InsertedToDB)
			{
				cssClass = "text-success";
			}
			else
			{
				cssClass = "text-primary";
			}
			return cssClass;
		}

		public static bool IsLoadingUploadFileStatus(ReportUploadFileStatus status)
		{
			bool isLoading = false;
			if (status == ReportUploadFileStatus.Uploaded || status == ReportUploadFileStatus.ConvertingToXML || status == ReportUploadFileStatus.ConvertedToXML)
			{
				isLoading = true;
			}
			return isLoading;
		}

		public static string GetStatusCssClassText(ReportStatus reportStatus, ReportStatusCrossCheck? reportStatusCrossCheck)
		{
			string cssClass = "";
			if (reportStatus == ReportStatus.Sehvdir_Qebul_olunmadi ||reportStatus == ReportStatus.Yoxlama_yoxdur || reportStatus == ReportStatus.Sehv_dovr_secilmisdir || reportStatus == ReportStatus.Imtina_edilib || reportStatusCrossCheck == ReportStatusCrossCheck.cross_yoxlama_sehvdir)
			{
				cssClass = "text-danger";
			}
			else
			if (reportStatus == ReportStatus.Duzdur)
			{
				cssClass = "text-success";
			}
			else
			if (reportStatus == ReportStatus.Serti_duzdur || reportStatus == ReportStatus.Gecikdirilib)
			{
				cssClass = "text-warning";
			}
			else
			{
				cssClass = "text-info";
			}
			return cssClass;
		}

		public static string GetStatusCssClassTextInt(int reportStatus, int? reportStatusCrossCheck)
		{
			return GetStatusCssClassText((ReportStatus)reportStatus, (ReportStatusCrossCheck?)reportStatusCrossCheck);
		}

		public static string GetStatusCssClassTdInt(int reportStatus, int? reportStatusCrossCheck)
		{
			string cssClass = GetStatusCssClassText((ReportStatus)reportStatus, (ReportStatusCrossCheck?)reportStatusCrossCheck);

			cssClass = cssClass.Replace("text-", "");

			return cssClass;
		}

		public static string GetReportActionLogDescription(string actionlog)
		{
			string desc = actionlog;
			if (Constants.ReportActionLogDescription.ContainsKey(actionlog))
				desc = Constants.ReportActionLogDescription[actionlog];
			return desc;
		}

		private static DateTime? GetYearMonthEndDate(string yearmonth, string days)
		{
			DateTime? periodend = null;
			if (days == "1")
			{
				// get 15th day of the selected month
				periodend = DateTime.ParseExact(yearmonth + "-15", "yyyy-MM-dd", CultureInfo.InvariantCulture);
			}
			else
			if (days == "2")
			{
				// get the last day of the selected month
				periodend = DateTime.ParseExact(yearmonth + "-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
				periodend = periodend.Value.AddMonths(1).AddDays(-1);
			}

			return periodend;
		}

		public static DateTime? GetPeriodStartDate(PeriodType periodType, DateTime periodEnd)
		{
			switch (periodType)
			{
				case PeriodType.Unknown:
					throw new Exception("PeriodType is Unknown");
				case PeriodType.M:
					return periodEnd.AddDays(1).AddMonths(-1);
				case PeriodType.HM:
					return new DateTime(periodEnd.Year, periodEnd.Month, 1);
				case PeriodType.Q:
					return periodEnd.AddDays(1).AddMonths(-3);
				case PeriodType.W:
					return periodEnd.AddDays(-7);
				case PeriodType.HD:
					return periodEnd.AddHours(-4);
				case PeriodType.D:
					return periodEnd.AddHours(-4);
			}

			return null;
		}

		public static List<DateTime> GetYearMonthEndDates(string yearmonth, string days)
		{
			var dates = new List<DateTime>();
			if (days == "1" || days == "")
			{
				// get 15th day of the selected month
				DateTime periodend = GetYearMonthEndDate(yearmonth, "1").Value;
				dates.Add(periodend);
			}
			if (days == "2" || days == "")
			{
				// get the last day of the selected month
				DateTime periodend = GetYearMonthEndDate(yearmonth, "2").Value;
				dates.Add(periodend);
			}

			return dates;
		}

		public static bool IsLunchBreak(DateTime dateTime)
		{
			var lunchStartTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 13, 0, 0);
			var workStartTimeAfterLunch = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 14, 0, 0);

			return dateTime >= lunchStartTime && dateTime < workStartTimeAfterLunch;
		}

		public static string GetAppUrlDomain(string AbsoluteUri)
		{
			string s = AbsoluteUri;
			s = s.Replace("http://", "");
			s = s.Replace("https://", "");
			int slashPos = s.IndexOf('/');
			s = s.Substring(0, slashPos);
			return s;
		}
		public static string GetFileUrlInServer(string downloadsPath, string link)
		{
			string filePath = Path.Combine(downloadsPath, link);
			filePath = filePath.Replace('/', Path.DirectorySeparatorChar);
			filePath = filePath.Replace('\\', Path.DirectorySeparatorChar);
			return filePath;
		}
	}
}
