﻿using Dapper;
using tedhiyye_portal_cbar.Models;
using tedhiyye_portal_shared;
using tedhiyye_portal_shared.Domains;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using insurance_system_cbar;

namespace tedhiyye_portal_cbar
{
    public class DBCBAR
    {
        public static IConfiguration _configuration;
        public DBCBAR(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private static object recentFilesLocker = new object();

        public static string ConnectionString
        {
            get
            {
                return AppSettings.ConnectionString("prudentialConnectionString");
            }
        }

        public static List<ResponsiblesInfo> GetResponsiblesCBAR(int user_id)
        {
            var respList = new List<ResponsiblesInfo>();
            var IsUserAdmin = IsAdmin(user_id);
            var moderatorReportFileTypes = GetModeratorReportFileTypes(user_id);
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_responsibles_cbar",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                    for (int j = 0; j < moderatorReportFileTypes.Count; j++)
                    {
                        for (int i = 0; i < dbResult.Count; i++)
                        {
                            if (moderatorReportFileTypes[j].Value == dbResult[i].REPORT_FILE_TYPE_ID.ToString())
                            {
                                string[] subIds = dbResult[i].EId != null ? dbResult[i].EId.ToString().Split(',') : new string[1];
                                var ri = new ResponsiblesInfo(subIds.Length);
                                ri.ReportFileTypeId = (int)dbResult[i].REPORT_FILE_TYPE_ID;
                                ri.ReportFileTypeName = dbResult[i].REPORT_FILE_TYPE_NAME.ToString();
                                ri.IsAdmin = IsUserAdmin;
                                ri.Executor = new ResponsiblePerson()
                                {
                                    Name = dbResult[i].INAME == null || dbResult[i].INAME == "-" ? "" : dbResult[i].INAME,
                                    Department = dbResult[i].IDEPARTMENT == null || dbResult[i].IDEPARTMENT == "-" ? "" : dbResult[i].IDEPARTMENT,
                                    Position = dbResult[i].IPOSITION == null || dbResult[i].IPOSITION == "-" ? "" : dbResult[i].IPOSITION,
                                    Phone = dbResult[i].IPHONE == null || dbResult[i].IPHONE == "-" ? "" : dbResult[i].IPHONE,
                                    Email = dbResult[i].IEMAIL == null || dbResult[i].IEMAIL == "-" ? "" : dbResult[i].IEMAIL,
                                    EmployeeId = dbResult[i].IID == null || dbResult[i].IID == "-" ? "" : dbResult[i].IID
                                };
                                ri.Curator = new ResponsiblePerson()
                                {
                                    Name = dbResult[i].KNAME == null || dbResult[i].KNAME == "-" ? "" : dbResult[i].KNAME,
                                    Department = dbResult[i].KDEPARTMENT == null || dbResult[i].KDEPARTMENT == "-" ? "" : dbResult[i].KDEPARTMENT,
                                    Position = dbResult[i].KPOSITION == null || dbResult[i].KPOSITION == "-" ? "" : dbResult[i].KPOSITION,
                                    Phone = dbResult[i].KPHONE == null || dbResult[i].KPHONE == "-" ? "" : dbResult[i].KPHONE,
                                    Email = dbResult[i].KEMAIL == null || dbResult[i].KEMAIL == "-" ? "" : dbResult[i].KEMAIL,
                                    EmployeeId = dbResult[i].KID == null || dbResult[i].KID == "-" ? "" : dbResult[i].KID
                                };
                                for (int k = 0; k < subIds.Length; k++)
                                {
                                    ri.SubstituteNames[k] = dbResult[k].ENAME == null || dbResult[k].ENAME == "-" ? "" : dbResult[k].ENAME.Split(',');
                                    ri.SubstituteDepartment = dbResult[k].EDEPARTMENT == null || dbResult[k].EDEPARTMENT == "-" ? "" : dbResult[k].EDEPARTMENT;
                                    ri.SubstitutePosition[k] = dbResult[k].EPOSITION == null || dbResult[k].EPOSITION == "-" ? "" : dbResult[k].EPOSITION.Split(',');
                                    ri.SubstitutePhone[k] = dbResult[k].EPHONE == null || dbResult[k].EPHONE == "-" ? "" : dbResult[k].EPHONE.Split(',');
                                    ri.SubstituteEmails[k] = dbResult[k].EEMAIL == null || dbResult[k].EEMAIL == "-" ? "" : dbResult[k].EEMAIL.Split(',');
                                    ri.SubstituteUserIds[k] = dbResult[k].EID == null || dbResult[k].EID == "-" ? "" : dbResult[k].EID.Split(',');
                                }
                                respList.Add(ri);
                            }
                        }
                    }
                }
                return respList;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<CheckRule> GetCheckRules(int? reporttype, int? status)
        {
            var list = new List<CheckRule>();
            try
            {
                status = status == null ? 1 : status;
                var statusChar = status == 1 ? "\'A\'" : "\'P\'";
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_report_file_type_id", value: reporttype, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_status", value: status == 1 ? 'A' : 'P', dbType: OracleDbType.Char, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<CheckRule>(sql: "pkg_portal_info.fnc_get_check_rules",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetReportFileTypeList()
        {
            var list = new List<SelectListItem>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<SelectListItem>(sql: "pkg_portal_reports.fnc_get_report_file_type_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }


        public static List<ReportType> GetReportConstructors()
        {
            var list = new List<ReportType>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<ReportType>(sql: "pkg_portal_reports.fnc_get_report_file_type_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<CustomSelectListItem> GetReportFileTypeList(int? userId)
        {
            var list = new List<CustomSelectListItem>();
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_userid", value: userId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    list = conn.Query<CustomSelectListItem>(sql: "pkg_portal_reports.fnc_get_user_report_type_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();


                    return list;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static ReportPeriod GetPeriodTypeByReportFileType(int reportFileTypeId)
        {
            ReportPeriod result = new ReportPeriod();
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    OracleDynamicParameters parameters = new OracleDynamicParameters();
                    parameters.Add("p_report_file_type_id", value: reportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.Varchar2, size: 8000, direction: ParameterDirection.ReturnValue); ;

                    var affectedRows = (string)conn.ExecuteScalar<string>(sql: "pkg_portal_reports.fnc_get_period_type",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           );

                    var returnVal = parameters.Get<OracleString>("rv").ToString();

                    result = new ReportPeriod(returnVal, "");

                }
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<ResponsiblesInfo> GetResponsiblesBank(int user_id, int bank_id, string reportFileType)
        {
            reportFileType = string.IsNullOrEmpty(reportFileType) ? "-1" : reportFileType;
            var respList = new List<ResponsiblesInfo>();
            var IsUserAdmin = IsAdmin(user_id);
            var moderatorReportFileTypes = GetModeratorReportFileTypes(user_id);
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_bankid", value: bank_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_report_file_type_id", value: reportFileType, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_responsibles_bank",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                    for (int j = 0; j < moderatorReportFileTypes.Count; j++)
                    {
                        for (int i = 0; i < dbResult.Count; i++)
                        {
                            if (moderatorReportFileTypes[j].Value == dbResult[i].REPORT_FILE_TYPE_ID.ToString())
                            {
                                string[] subIds = dbResult[i].EId != null ? dbResult[i].EId.ToString().Split(',') : new string[1];
                                var ri = new ResponsiblesInfo(subIds.Length);
                                ri.ReportFileTypeId = (int)dbResult[i].REPORT_FILE_TYPE_ID;
                                ri.ReportFileTypeName = dbResult[i].REPORT_FILE_TYPE_NAME.ToString();
                                ri.IsAdmin = IsUserAdmin;
                                ri.Executor = new ResponsiblePerson()
                                {
                                    Name = dbResult[i].INAME == null || dbResult[i].INAME == "-" ? "" : dbResult[i].INAME,
                                    Department = dbResult[i].IDEPARTMENT == null || dbResult[i].IDEPARTMENT == "-" ? "" : dbResult[i].IDEPARTMENT,
                                    Position = dbResult[i].IPOSITION == null || dbResult[i].IPOSITION == "-" ? "" : dbResult[i].IPOSITION,
                                    OfficePhone = dbResult[i].IOFFICE_PHONE == null || dbResult[i].IOFFICE_PHONE == "-" ? "" : dbResult[i].IOFFICE_PHONE,
                                    MobilePhone = dbResult[i].IMOBILE_PHONE == null || dbResult[i].IMOBILE_PHONE == "-" ? "" : dbResult[i].IMOBILE_PHONE,
                                    Email = dbResult[i].IEMAIL == null || dbResult[i].IEMAIL == "-" ? "" : dbResult[i].IEMAIL,
                                    EmployeeId = dbResult[i].IID == null || dbResult[i].IID == "-" ? "" : dbResult[i].IID
                                };
                                ri.Curator = new ResponsiblePerson()
                                {
                                    Name = dbResult[i].KNAME == null || dbResult[i].KNAME == "-" ? "" : dbResult[i].KNAME,
                                    Department = dbResult[i].KDEPARTMENT == null || dbResult[i].KDEPARTMENT == "-" ? "" : dbResult[i].KDEPARTMENT,
                                    Position = dbResult[i].KPOSITION == null || dbResult[i].KPOSITION == "-" ? "" : dbResult[i].KPOSITION,
                                    OfficePhone = dbResult[i].KOFFICE_PHONE == null || dbResult[i].KOFFICE_PHONE == "-" ? "" : dbResult[i].KOFFICE_PHONE,
                                    MobilePhone = dbResult[i].KMOBILE_PHONE == null || dbResult[i].KMOBILE_PHONE == "-" ? "" : dbResult[i].KMOBILE_PHONE,
                                    Email = dbResult[i].KEMAIL == null || dbResult[i].KEMAIL == "-" ? "" : dbResult[i].KEMAIL,
                                    EmployeeId = dbResult[i].KID == null || dbResult[i].KID == "-" ? "" : dbResult[i].KID
                                };
                                ri.SubstituteNames = dbResult[i].ENAME == null || dbResult[i].ENAME == "-" ? new string[] { "" } : dbResult[i].ENAME.Split(',');
                                ri.SubstituteDepartment = dbResult[i].EDEPARTMENT == null || dbResult[i].EDEPARTMENT == "-" ? "" : dbResult[i].EDEPARTMENT;
                                ri.SubstitutePosition = dbResult[i].EPOSITION == null || dbResult[i].EPOSITION == "-" ? new string[] { "" } : dbResult[i].EPOSITION.Split(',');
                                ri.SubstituteOfficePhone = dbResult[i].EOFFICE_PHONE == null || dbResult[i].EOFFICE_PHONE == "-" ? new string[] { "" } : dbResult[i].EOFFICE_PHONE.Split(',');
                                ri.SubstituteMobilePhone = dbResult[i].EMOBILE_PHONE == null || dbResult[i].EMOBILE_PHONE == "-" ? new string[] { "" } : dbResult[i].EMOBILE_PHONE.Split(',');
                                ri.SubstituteEmails = dbResult[i].EEMAIL == null || dbResult[i].EEMAIL == "-" ? new string[] { "" } : dbResult[i].EEMAIL.Split(',');
                                ri.SubstituteUserIds = dbResult[i].EID == null || dbResult[i].EID == "-" ? new string[] { "" } : dbResult[i].EID.Split(',');
                                respList.Add(ri);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
            return respList;
        }

        internal static List<ReportVersion> GetReportVersions()
        {

            List<ReportVersion> result = new List<ReportVersion>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);

                    result = conn.Query<ReportVersion>(sql: "pkg_portal_reports.fnc_get_report_version",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                }
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<XlsCellWithData> GetXlsCellWithData(int reportFileId, int ruleId)
        {
            List<XlsCellWithData> result = new List<XlsCellWithData>();
            try
            {
                var sql = "PKG_HELPER_REPORTS.GET_CELL_AND_VALUE";
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.BindByName = true;

                        OracleParameter pReturn = new OracleParameter
                        {
                            OracleDbType = OracleDbType.RefCursor,
                            Direction = ParameterDirection.ReturnValue
                        };


                        OracleParameter pReportId = new OracleParameter
                        {
                            ParameterName = "P_REPORTS_FILE_ID",
                            DbType = System.Data.DbType.Int32,
                            Direction = ParameterDirection.Input,
                            Value = reportFileId
                        };

                        OracleParameter pRuleId = new OracleParameter
                        {
                            ParameterName = "P_XLRULEID",
                            DbType = System.Data.DbType.Int32,
                            Direction = ParameterDirection.Input,
                            Value = ruleId
                        };

                        cmd.Parameters.Add(pReturn);
                        cmd.Parameters.Add(pReportId);
                        cmd.Parameters.Add(pRuleId);

                        var r = cmd.ExecuteNonQuery();

                        var crs = (cmd.Parameters[0].Value) as OracleRefCursor;

                        if (!crs.IsNull)
                        {
                            using (var reader = crs.GetDataReader())
                            {
                                while (reader.Read())
                                {
                                    string cell = reader.GetOracleString(0).IsNull ? "-" : reader.GetOracleString(0).Value;
                                    var data = reader.GetOracleString(1).IsNull ? "-" : reader.GetOracleString(1).Value;
                                    var groupName = reader.GetOracleString(2).IsNull ? "-" : reader.GetOracleString(2).Value;
                                    var sheetName = reader.GetOracleString(3).IsNull ? "-" : reader.GetOracleString(3).Value;
                                    result.Add(new XlsCellWithData(cell, data, groupName, sheetName));
                                }
                            }
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool PeriodDateFileExists(int bank_id, int report_file_type_id, int isPhqe, DateTime periodend, out bool p_result, out string p_result_message)
        {

            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "pkg_portal_reports.prc_check_report_exist";
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_bankid", type: OracleDbType.Int32, obj: bank_id, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_report_file_id", type: OracleDbType.Int32, obj: report_file_type_id, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_is_phqe", type: OracleDbType.Int32, obj: isPhqe, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_periodend", type: OracleDbType.Date, obj: periodend, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result", type: OracleDbType.Boolean, obj: null, direction: ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result_message", type: OracleDbType.Varchar2, size: 8000, obj: null, direction: ParameterDirection.Output));


                        cmd.ExecuteScalar();


                        p_result = ((OracleBoolean)cmd.Parameters["p_result"].Value).Value;
                        p_result_message = ((OracleString)cmd.Parameters["p_result_message"].Value).Value;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }

            return p_result;
        }

        public static bool MarkInboxMessageAsRead(int message_id)
        {
            bool res = false;
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "PKG_PORTAL_MESSAGES.prc_mark_inbox_message_as_read";
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_message_id", type: OracleDbType.Int32, obj: message_id, direction: ParameterDirection.Input));
                        var r = cmd.ExecuteNonQuery();
                        res = true;
                    }
                }


                return res;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static PaginatedList<NotificationMessage> GetSentMessages(int bank_id, int department_id, int page_no = 1, int page_size = 10)
        {
            var list = new List<NotificationMessage>();
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_bankid", value: bank_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_department", value: department_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    var dbResult = conn.Query<NotificationMessage>(sql: "PKG_PORTAL_MESSAGES.fnc_get_sent_messages",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                    int rowNo = 0;
                    int rowMin = (page_no - 1) * page_size + 1;
                    int rowMax = rowMin + page_size - 1;

                    foreach (var item in dbResult)
                    {
                        rowNo++;
                        if (rowNo < rowMin || rowNo > rowMax)
                            continue;
                        var message = new NotificationMessage();
                        message.ID = item.ID;
                        message.SENDDATE = item.SENDDATE;
                        message.BANKID = item.BANKID;
                        message.DEPARTMENTID = item.DEPARTMENTID;
                        message.DEPARTMENT = item.DEPARTMENT;
                        message.SUBJECT = item.SUBJECT;
                        message.HASREAD = item.HASREAD;
                        message.READDATE = item.READDATE;

                        list.Add(message);
                    }
                }

                return new PaginatedList<NotificationMessage>(list, list.Count, page_no, page_size);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static NotificationMessage GetSentMessage(int message_id)
        {
            var message = new NotificationMessage();
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_message_id", value: message_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    message = conn.Query<NotificationMessage>(sql: "PKG_PORTAL_MESSAGES.fnc_get_sent_message",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .FirstOrDefault();

                }


                return message;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool SendMessage(int bank_id, ComposeMessageVM composeMessageVM)
        {
            bool res = false;
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "PKG_PORTAL_MESSAGES.prc_send_message";
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_bankid", type: OracleDbType.Int32, obj: bank_id, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_departamentid", type: OracleDbType.Int32, obj: (int)composeMessageVM.DepartmentId.Value, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_subject", type: OracleDbType.Varchar2, size: 8000, obj: composeMessageVM.Subject, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_message", type: OracleDbType.Varchar2, size: 8000, obj: composeMessageVM.Body, direction: ParameterDirection.Input));

                        var r = cmd.ExecuteNonQuery();
                        res = true;


                    }
                    return res;
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static AuthResponse AuthUsername(string username, string password)
        {
            AuthResponse ar = new AuthResponse();
            ar.ErrorCode = 1;
            ar.Roles = new List<int>();
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                string sql =
    @"select e.ID, DEPARTMENTID, d.DEPARTMENT, EMPLOYEE, USERNAME, PASSWORD, NAME, SURNAME, fathername, e.DEPARTMENT, POSITION, PHONE, EMAIL, MISSIONED, SYSTEMPERMISSION, PIN from employees e 
left join DEPARTMENTS d on e.DEPARTMENTID = d.ID 
where lower(USERNAME) = lower(:USERNAME) and PASSWORD = :PASSWORD and e.is_active=1";
                using (OracleCommand c = new OracleCommand(sql, conn))
                {
                    c.Parameters.Add("USERNAME", username);
                    c.Parameters.Add("PASSWORD", Tools.CalculateSHA512Hash(password));

                    using (OracleDataReader r = c.ExecuteReader())
                    {
                        if (r.Read())
                        {
                            ar.ErrorCode = 0;
                            ar.AuthenticationType = AuthType.UsernamePassword;

                            ar.Username = username;
                            ar.UserId = Convert.ToInt32(r["ID"].ToString());

                            ar.Name = "";
                            if (r["NAME"] != DBNull.Value)
                                ar.Name += r["NAME"].ToString();
                            if (r["SURNAME"] != DBNull.Value)
                                ar.Name += " " + r["SURNAME"].ToString();
                            if (r["fathername"] != DBNull.Value)
                                ar.Name += " " + r["fathername"].ToString();

                            ar.DepartmentId = Convert.ToInt32(r["DEPARTMENTID"].ToString());
                            ar.DepartmentName = r["DEPARTMENT"].ToString();
                            ar.Roles = new List<int>();
                        }
                    }
                }

                using (OracleCommand c = new OracleCommand("select role_id from employee_roles where employee_id = :employee_id", conn))
                {
                    c.Parameters.Add("employee_id", ar.UserId);

                    using (OracleDataReader r = c.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            ar.Roles.Add(Convert.ToInt32(r["role_id"].ToString()));
                        }
                    }
                }

                if (ar.Roles != null && ar.Roles.Contains((int)CBARUserRole.Real_sector))
                {
                    ar.RealSectorCompanyTypes = new List<int>();

                    using (OracleCommand c = new OracleCommand("select companytype_id from employee_companytypes where employee_id = :employee_id", conn))
                    {
                        c.Parameters.Add("employee_id", ar.UserId);

                        using (OracleDataReader r = c.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                ar.RealSectorCompanyTypes.Add(Convert.ToInt32(r["companytype_id"].ToString()));
                            }
                        }
                    }
                }
            }

            return ar;
        }

        public static void ConvertAllToHash()
        {
            ConvertTableToHash("users");
            ConvertTableToHash("employees");
            ConvertTableToHash("userscompanies");
        }

        private static void ConvertTableToHash(string tablename)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    DataTable dt = new DataTable();
                    using (OracleDataAdapter adapter = new OracleDataAdapter("select ID, PASSWORD from " + tablename + " where length(password)<100", conn))
                    {
                        dt = new DataTable();
                        adapter.Fill(dt);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        using (OracleCommand c = new OracleCommand("update " + tablename + " set password=:password where id=:id", conn))
                        {
                            c.Parameters.Add("password", Tools.CalculateSHA512Hash(row["password"].ToString()));
                            c.Parameters.Add("id", row["id"]);
                            c.ExecuteNonQuery();
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetBankListForSelectBox(int user_id)
        {
            var list = new List<SelectListItem>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_banktypes", value: "", dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);

                    var dbResult = conn.Query<Bank>(sql: "pkg_portal_info.fnc_get_bank_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                    foreach (var bank in dbResult)
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = bank.Name,
                            Value = bank.Id.ToString()
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<BankType> GetBankTypeList(int employeeid)
        {
            var list = new List<BankType>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_employeeid", value: employeeid, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<BankType>(sql: "pkg_portal_info.fnc_get_bank_type_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }


        public static List<Report> GetReports(List<BankTypeFilter> bankTypeFilters, int[] bank_ids, int user_id, int year, int month, string days, string status, string report_file_type_id)
        {
            var list = new List<Report>();

            try
            {
                using (IDbConnection connection = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

                    var bankIDs = string.Join(",", bank_ids);
                    parameters.Add("p_bankid", value: bankIDs.ToString(), dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("p_year", value: year, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_month", value: month, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

                    int daysCount = -1;
                    if (!string.IsNullOrEmpty(days)) daysCount = Convert.ToInt32(days);
                    parameters.Add("p_days", value: daysCount, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

                    int file_id = -1;
                    if (!string.IsNullOrEmpty(report_file_type_id)) file_id = Convert.ToInt32(report_file_type_id);

                    parameters.Add("p_report_file_type_id", value: file_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

                    int statusid = -1;
                    if (!string.IsNullOrEmpty(status))
                        statusid = Convert.ToInt32(status);
                    parameters.Add("p_status", value: statusid, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    list = connection.Query<Report>(sql: "pkg_portal_reports.fnc_get_reports",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }

            return list;
        }

        public static List<BaseReportError> GetReportFileErrors(int report_file_id)
        {
            var list = new List<BaseReportError>();
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_report_file_id", value: report_file_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    list = conn.Query<BaseReportError>(sql: "pkg_portal_reports.fnc_get_report_file_errors",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();


                    return list;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static DBFileInfo GetReportUploadedFile(int report_file_id, int user_id)
        {
            DBFileInfo fi = new DBFileInfo() { NotFound = true };
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_report_file_id", value: report_file_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    fi = conn.Query<DBFileInfo>(sql: "pkg_portal_reports.fnc_get_report_uploaded_file",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           ).FirstOrDefault();

                    fi.NotFound = fi.UPLOAD_FILE_DATA.Length != 0 ? false : true;
                }
                return fi;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool AddLog(string organization, string username, string report_id, string action, string data, string log_type, string ip, string reports_file_id = "")
        {
            bool res = false;
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    try
                    {
                        using (OracleCommand cmd = new OracleCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "pkg_portal_reports.prc_add_log";
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_organization", type: OracleDbType.Varchar2, obj: organization, direction: ParameterDirection.Input));
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_username", type: OracleDbType.Varchar2, obj: username, direction: ParameterDirection.Input));
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_action", type: OracleDbType.Varchar2, obj: action, direction: ParameterDirection.Input));
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_data", type: OracleDbType.Varchar2, obj: data, direction: ParameterDirection.Input));
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_log_type", type: OracleDbType.Varchar2, obj: log_type, direction: ParameterDirection.Input));
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_ip", type: OracleDbType.Varchar2, obj: ip, direction: ParameterDirection.Input));
                            cmd.Parameters.Add(new OracleParameter(parameterName: "p_reports_file_id", type: OracleDbType.Varchar2, obj: reports_file_id, direction: ParameterDirection.Input));
                            var r = cmd.ExecuteNonQuery();
                            res = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex);
                        throw;
                    }

                }
                return res;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool ConfirmAsCorrect(int report_file_id, out bool p_result, out string p_result_message)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "pkg_portal_reports.prc_confirmascorrect";
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_report_file_id", type: OracleDbType.Int32, obj: report_file_id, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result", type: OracleDbType.Boolean, obj: null, direction: ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result_message", type: OracleDbType.NVarchar2, size: 8000, obj: null, direction: ParameterDirection.Output));

                        cmd.ExecuteScalar();


                        p_result = ((OracleBoolean)cmd.Parameters["p_result"].Value).Value;
                        p_result_message = ((OracleString)cmd.Parameters["p_result_message"].Value).Value;

                        return p_result;
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }


        public static bool ConfirmCorrect(int report_file_id, out bool p_result, out string p_result_message)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "pkg_portal_reports.prc_ConfirmReport";
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_report_file_id", type: OracleDbType.Int32, obj: report_file_id, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result", type: OracleDbType.Boolean, obj: null, direction: ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result_message", type: OracleDbType.NVarchar2, size: 8000, obj: null, direction: ParameterDirection.Output));

                        cmd.ExecuteScalar();


                        p_result = ((OracleBoolean)cmd.Parameters["p_result"].Value).Value;
                        p_result_message = ((OracleString)cmd.Parameters["p_result_message"].Value).Value;

                        return p_result;
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool CancelReport(int report_file_id, out bool p_result, out string p_result_message)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "pkg_portal_reports.prc_cancel_report";
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_report_file_id", type: OracleDbType.Int32, obj: report_file_id, direction: ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result", type: OracleDbType.Boolean, obj: null, direction: ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter(parameterName: "p_result_message", type: OracleDbType.Varchar2, size: 8000, obj: null, direction: ParameterDirection.Output));

                        cmd.ExecuteScalar();


                        p_result = ((OracleBoolean)cmd.Parameters["p_result"].Value).Value;
                        p_result_message = ((OracleString)cmd.Parameters["p_result_message"].Value).Value;

                        return p_result;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static PaginatedList<NotificationMessage> GetInboxMessages(int user_id, int page_no = 1, int page_size = 10)
        {
            var list = new List<NotificationMessage>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_userid ", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    var dbResult = conn.Query<NotificationMessage>(sql: "PKG_PORTAL_MESSAGES.fnc_get_inbox_messages",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                    int rowNo = 0;
                    int rowMin = (page_no - 1) * page_size + 1;
                    int rowMax = rowMin + page_size - 1;

                    foreach (var item in dbResult)
                    {
                        rowNo++;
                        if (rowNo < rowMin || rowNo > rowMax)
                            continue;

                        var message = new NotificationMessage();

                        message.ID = item.ID;
                        message.SENDDATE = item.SENDDATE;
                        message.BANKID = item.BANKID;
                        message.BankName = item.BANKNAME;
                        message.SUBJECT = item.SUBJECT;
                        message.HASREAD = item.HASREAD;
                        message.READDATE = item.READDATE;
                        message.DELETEDATE = item.DELETEDATE;
                        message.USERID = item.USERID;

                        list.Add(message);
                    }
                }

                return new PaginatedList<NotificationMessage>(list, list.Count, page_no, page_size);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static NotificationMessage GetInboxMessage(int message_id)
        {
            var message = new NotificationMessage();
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_message_id ", value: message_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    message = conn.Query<NotificationMessage>(sql: "PKG_PORTAL_MESSAGES.fnc_get_inbox_message",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .FirstOrDefault();

                }

                return message;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }


        public static int GetUnreadInboxMessagesCount(int user_id)
        {
            int count = 0;
            try
            {
                var list = new List<NotificationMessage>();

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);

                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    count = conn.Query<Int32>(sql: "pkg_portal_messages.fnc_get_unread_message_count",
                          param: parameters,
                          commandType: CommandType.StoredProcedure
                          )
                          .First();
                }

                return count;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<DownloadFileInfo> GetDownloads(int group_id, int? department_id)
        {
            var list = new List<DownloadFileInfo>();
            try
            {
                string projectName = group_id == 2 ? @"prudential_banks" : null;
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_groupid", value: group_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_departmentid", value: department_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_projectname", value: projectName, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<DownloadFileInfo>(sql: "pkg_portal_info.fnc_get_downloads",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static DownloadFileInfo GetDownload(int id)
        {
            var di = new DownloadFileInfo();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    di = conn.Query<DownloadFileInfo>(sql: "pkg_portal_info.fnc_get_download",
                          param: parameters,
                          commandType: CommandType.StoredProcedure
                          )
                          .First();
                }

                return di;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetDepartments()
        {
            var list = new List<SelectListItem>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<SelectListItem>(sql: "pkg_portal_info.fnc_get_departments",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool UploadReport(string filename, string contenttype, byte[] data, int userid, string ip, int? bankid, int report_file_type_id, int isPhqe, DateTime periodstart, DateTime periodend, out int insertedReportFileId, out string resultMessage)
        {
            lock (recentFilesLocker)
            {
                try
                {
                    using (var conn = new OracleConnection(ConnectionString))
                    {
                        var parameters = new OracleDynamicParameters();
                        conn.Open();

                        parameters.Add("P_UPLOAD_FILE_NAME", value: filename, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                        parameters.Add("P_UPLOAD_FILE_CONTENT_TYPE", value: contenttype, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                        parameters.Add("P_UPLOAD_USER_ID", value: userid, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                        parameters.Add("P_UPLOAD_USER_IP", value: ip, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                        parameters.Add("P_BANKID", value: bankid, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                        parameters.Add("P_PERIODSTART", value: periodstart, dbType: OracleDbType.Date, direction: ParameterDirection.Input);
                        parameters.Add("P_PERIODEND", value: periodend, dbType: OracleDbType.Date, direction: ParameterDirection.Input);
                        parameters.Add("P_REPORT_FILE_TYPE_ID", value: report_file_type_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                        parameters.Add("P_UPLOAD_FILE_DATA", value: data, dbType: OracleDbType.Blob, direction: ParameterDirection.Input);
                        parameters.Add("P_IS_PHQE", value: isPhqe, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                        parameters.Add("P_RESULT", dbType: OracleDbType.Boolean, direction: ParameterDirection.Output);
                        parameters.Add("P_RESULT_MESSAGE", dbType: OracleDbType.Varchar2, size: 600, direction: ParameterDirection.Output);
                        parameters.Add("P_REPORT_FILE_ID", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);


                        var dbResult = conn.Execute(sql: "pkg_portal_reports.prc_upload_report",
                               param: parameters,
                               commandType: CommandType.StoredProcedure
                               );

                        var result = parameters.Get<OracleBoolean>("P_RESULT").Value;
                        var oracleResultMessage = parameters.Get<OracleString>("P_RESULT_MESSAGE");
                        resultMessage = oracleResultMessage.IsNull ? string.Empty : oracleResultMessage.Value;

                        insertedReportFileId = parameters.Get<OracleDecimal>("P_REPORT_FILE_ID").ToInt32();

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex);
                    resultMessage = string.Format("Baza xətası (pkg_portal_reports.prc_upload_report): {0}", ex.Message);
                    insertedReportFileId = -1;
                    return false;
                }
            }
        }

        public static List<UploadedReportFile> GetRecentUploadedReportFiles(int user_id, int bank_id, int count = 0)
        {

            var list = new List<UploadedReportFile>();
            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_bankid", value: bank_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    list = conn.Query<UploadedReportFile>(sql: "pkg_portal_reports.fnc_get_recent_report_files",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }

                if (count == 10)
                {
                    list = list.Take(count).ToList();
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<DBLogItem> GetActionLogs(int report_id)
        {
            var list = new List<DBLogItem>();

            try
            {
                using (IDbConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_report_file_id", value: report_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);


                    list = conn.Query<DBLogItem>(sql: "pkg_portal_reports.fnc_get_report_action_logs",
                          param: parameters,
                          commandType: CommandType.StoredProcedure
                          )
                          .AsList();
                }

                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool CheckConfirmedAsCorrectSimilarReports(int report_file_id)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_report_file_id", value: report_file_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.Boolean, direction: ParameterDirection.ReturnValue);
                    var affectedRows = conn.ExecuteScalar<bool>(sql: "pkg_portal_reports.fnc_check_confirmed_exist",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           );

                    var returnVal = parameters.Get<OracleBoolean>("rv");

                    return returnVal.Value;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetCbarUsers()
        {
            List<SelectListItem> employeeList = new List<SelectListItem>();
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    employeeList = conn.Query<SelectListItem>(sql: "pkg_portal_info.fnc_get_cbar_users",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return employeeList;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static int AddReportEmployee(int reportFileTypeId, int employeeId, int jobId, int bankId)
        {
            int rows = 0;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rftID", value: reportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("eID", value: employeeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rjID", value: jobId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("bankID", value: bankId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    rows = rows = conn.Execute("pkg_portal_info.prc_add_report_employee", parameters, commandType: CommandType.StoredProcedure);
                }
                return rows;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetModeratorReportFileTypes(int userId)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            try
            {

                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_userid", value: userId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<SelectListItem>(sql: "pkg_portal_reports.fnc_get_user_report_type_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }

            return list;
        }
        public static int UpdateReportEmployee(int reportFileTypeId, int employeeId, int jobId, int bankId, int is_active)
        {
            int rows = 0;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rjID", value: jobId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("eID", value: employeeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rftID", value: reportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("bID", value: bankId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rjID2", value: jobId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_isactive", value: is_active, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    rows = conn.Execute("pkg_portal_info.prc_update_report_employee", parameters, commandType: CommandType.StoredProcedure);
                }
                return rows;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static bool IsAdmin(int userId)
        {
            bool result = false;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_userid", value: userId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_is_admin",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .First();
                    result = dbResult.ID.ToString() == "2" ? true : false;
                }
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static int UpdateCbarUser(string fullName, string department, string position, string phone, string email, string eID)
        {
            string name = "", surname = "";
            bool hasWhiteSpace = fullName.Contains(" ");
            if (hasWhiteSpace)
            {
                var nameSurname = fullName.Split(' ');
                name = nameSurname[0];
                surname = nameSurname[1];
            }
            else
            {
                name = fullName;
            }
            int rows = 0;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("name", value: name, dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Input);
                    parameters.Add("dep", value: department, dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Input);
                    parameters.Add("pos", value: position, dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Input);
                    parameters.Add("phone", value: phone, dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Input);
                    parameters.Add("email", value: email, dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Input);
                    parameters.Add("eID", value: eID, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    rows = conn.Execute("pkg_portal_info.prc_update_cbar_user", parameters, commandType: CommandType.StoredProcedure);
                }
                return rows;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static ResponsiblesInfo GetResponsiblesByReportFileTypeId(int reportFileTypeId, int bankId, int userId)
        {
            var reportFileTypeList = GetReportFileTypeList();
            var bankList = GetBankList(userId, null);
            ResponsiblesInfo ri = null;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rftID", value: reportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("bID", value: bankId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_responsibles_by_report_file_type",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                    var subIds = 1;
                    subIds = dbResult.Count == 0 ? 1 : dbResult[0].EID.ToString().Split(',').Length;
                    ri = new ResponsiblesInfo(subIds);
                    ri.Executor = new ResponsiblePerson();
                    ri.Curator = new ResponsiblePerson();
                    ri.ReportFileTypeName = reportFileTypeList.FirstOrDefault(x => x.Value == reportFileTypeId.ToString()).Text;
                    ri.ReportFileTypeId = Convert.ToInt32(reportFileTypeList.FirstOrDefault(x => x.Value == reportFileTypeId.ToString()).Value);
                    ri.BankName = bankList.First(x => Convert.ToInt32(x.Id) == bankId).Name;
                    ri.BankId = Convert.ToInt32(bankList.First(x => x.Id == bankId).Id);
                    if (dbResult.Count > 0)
                    {
                        ri.Executor = new ResponsiblePerson()
                        {
                            Name = dbResult[0].INAME,
                            EmployeeId = dbResult[0].IID
                        };
                        ri.Curator = new ResponsiblePerson()
                        {
                            Name = dbResult[0].KNAME,
                            EmployeeId = dbResult[0].KID
                        };
                        for (int i = 0; i < subIds; i++)
                        {
                            ri.SubstituteNames = dbResult[0].ENAME == null || dbResult[0].ENAME == "-" ? new string[] { "" } : dbResult[0].ENAME.Split(',');
                            ri.SubstituteDepartment = dbResult[0].EDEPARTMENT == null || dbResult[0].EDEPARTMENT == "-" ? "" : dbResult[0].EDEPARTMENT;
                            ri.SubstitutePosition = dbResult[0].EPOSITION == null || dbResult[0].EPOSITION == "-" ? new string[] { "" } : dbResult[0].EPOSITION.Split(',');
                            ri.SubstituteOfficePhone = dbResult[0].EOFFICE_PHONE == null || dbResult[0].EOFFICE_PHONE == "-" ? new string[] { "" } : dbResult[0].EOFFICE_PHONE.Split(',');
                            ri.SubstituteMobilePhone = dbResult[0].EMOBILE_PHONE == null || dbResult[0].EMOBILE_PHONE == "-" ? new string[] { "" } : dbResult[0].EMOBILE_PHONE.Split(',');
                            ri.SubstituteEmails = dbResult[0].EEMAIL == null || dbResult[0].EEMAIL == "-" ? new string[] { "" } : dbResult[0].EEMAIL.Split(',');
                            ri.SubstituteUserIds = dbResult[0].EID == null || dbResult[0].EID == "-" ? new string[] { "" } : dbResult[0].EID.Split(',');
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }

            return ri;
        }

        public static int CheckReportFileTypeHasUserByJobId(int reportFileTypeId, int jobId, int bankId)
        {
            int result = 0;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rft_ID", value: reportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("jobId", value: jobId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("bankId", value: bankId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    result = conn.Query<Int32>(sql: "pkg_portal_info.fnc_get_report_file_type_user",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .FirstOrDefault();
                }
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static int ChangeReportEmployees(int reportFileTypeId, string executorId, string curatorId, string substituteId, int bankId)
        {
            int result = 0;
            if (!(String.IsNullOrEmpty(executorId)))
            {
                if (CheckReportFileTypeHasUserByJobId(reportFileTypeId, 1, bankId) > 0)
                {
                    result = UpdateReportEmployee(reportFileTypeId, Convert.ToInt32(executorId), 1, bankId, 1);
                }
                else
                {
                    result = AddReportEmployee(reportFileTypeId, Convert.ToInt32(executorId), 1, bankId);
                }
            }
            if (!(String.IsNullOrEmpty(curatorId)))
            {
                if (CheckReportFileTypeHasUserByJobId(reportFileTypeId, 3, bankId) > 0)
                {
                    result = UpdateReportEmployee(reportFileTypeId, Convert.ToInt32(curatorId), 3, bankId, 1);
                }
                else
                {
                    result = AddReportEmployee(reportFileTypeId, Convert.ToInt32(curatorId), 3, bankId);
                }
            }
            if (!(String.IsNullOrEmpty(substituteId)))
            {
                result = AddReportEmployee(reportFileTypeId, Convert.ToInt32(substituteId), 2, bankId);
            }
            return result;
        }

        public static int UpdateBankUser(string fullName, string department, string position, string oPhone, string mPhone, string email, string eID)
        {
            int rows = 0;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("name", value: fullName, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("dep", value: department, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("pos", value: position, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("oPhone", value: oPhone, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("mPhone", value: mPhone, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("email", value: email, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("eID", value: eID, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    rows = conn.Execute("pkg_portal_info.prc_update_cbar_user", parameters, commandType: CommandType.StoredProcedure);
                }
                return rows;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static int AddBankUser(int bankid, string fullName, string department, string position, string oPhone, string mPhone, string email)
        {
            int userId = 0;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    int rows = 0;
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("bankid", value: bankid, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("name", value: fullName, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("department", value: department, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("position", value: position, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("oPhone", value: oPhone, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("mPhone", value: mPhone, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("email", value: email, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    rows = conn.Execute("pkg_portal_info.prc_add_bank_user", parameters, commandType: CommandType.StoredProcedure);
                    userId = GetBankUserMaxId(bankid);
                }
                return userId;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return userId;
            }
        }

        public static List<BankUser> GetBankUsersForResponsibles(int bank_id)
        {
            var list = new List<BankUser>();

            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_bankid", value: bank_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);

                    list = conn.Query<BankUser>(sql: "pkg_portal_info.fnc_get_responsible_users",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }

            return list;
        }

        public static List<Bank> GetBankList(int user_id, string[] bankType)
        {
            var list = new List<Bank>();
            string bankTypeInQuery = "";
            if (bankType != null)
            {
                for (int i = 0; i < bankType.Length; i++)
                {
                    bankTypeInQuery += bankType[i];
                    if (i != bankType.Length - 1)
                    {
                        bankTypeInQuery += ",";
                    }
                }
            }
            try
            {

                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_userid", value: user_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_banktypes", value: bankTypeInQuery, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);

                    list = conn.Query<Bank>(sql: "pkg_portal_info.fnc_get_bank_list",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }

            return list;
        }

        public static int GetReportFileTypeId(string reportFileTypeName, string reportTypeId)
        {
            int reportFileTypeId = 0;
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                string sql = "";
                conn.Open();
                if (!string.IsNullOrEmpty(reportFileTypeName))
                {
                    sql = @"SELECT REPORT_FILE_TYPE_ID,REPORT_FILE_TYPE_NAME  FROM REPORT_FILE_TYPES WHERE REPORT_FILE_TYPE_NAME=:reportFileTypeName";
                }
                if (!string.IsNullOrEmpty(reportTypeId))
                {
                    sql = @"select rft.report_file_type_id,rft.report_file_type_name
from reporttype rt join report_file_types rft on rt.report_file_type_id=rft.report_file_type_id
where rt.id=:rt_id";
                }
                using (OracleCommand c = new OracleCommand(sql, conn))
                {
                    if (!string.IsNullOrEmpty(reportFileTypeName))
                    {
                        c.Parameters.Add("reportFileTypeName", reportFileTypeName);
                    }
                    if (!string.IsNullOrEmpty(reportTypeId))
                    {
                        c.Parameters.Add("rt_id", reportTypeId);
                    }
                    using (OracleDataReader r = c.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            reportFileTypeId = Convert.ToInt32(r["REPORT_FILE_TYPE_ID"].ToString());
                            break;
                        }
                    }
                }
            }
            return reportFileTypeId;
        }

        public static List<CertUserReportFileTypeDetail> GetCertUsers(int bankId)
        {
            CertUserReportFileTypeDetail certUserReportFileTypeDetail = null;
            List<CertUserReportFileTypeDetail> certUserReportFileTypeDetailList = new List<CertUserReportFileTypeDetail>();
            List<int> userIds = new List<int>();
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();
                var parameters = new OracleDynamicParameters();
                parameters.Add("p_bankid", value: bankId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_cert_users",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .AsList();
                foreach (var item in dbResult)
                {
                    certUserReportFileTypeDetail = new CertUserReportFileTypeDetail()
                    {
                        ReportFileTypeId = (int)item.REPORT_FILE_TYPE_ID,
                        ReportFileTypeName = item.REPORT_FILE_TYPE_NAME,
                        User = new CertUser()
                        {
                            FullName = item.NAME,
                            Phone = item.PHONE,
                            Position = item.POSITION,
                            Email = item.EMAIL
                        },
                    };
                    certUserReportFileTypeDetailList.Add(certUserReportFileTypeDetail);
                }
            }
            return certUserReportFileTypeDetailList;
        }

        public static bool IsBankHaveAccessForReportFileType(int? bank_id, int? report_file_type_id)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_bankid ", value: bank_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("p_report_file_type_id ", value: report_file_type_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.Boolean, direction: ParameterDirection.ReturnValue);

                    var affectedRows = conn.ExecuteScalar<int>(sql: "pkg_portal_reports.fnc_is_bank_access_report",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           );

                    var returnVal = parameters.Get<OracleBoolean>("rv");

                    return returnVal.Value;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static int GetReportTypeByReportSheet(int reportSheetId)
        {
            int reportTypeId = 0;
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                var parameters = new OracleDynamicParameters();
                parameters.Add("sheet_id", value: reportSheetId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<int>(sql: "pkg_portal_info.fnc_get_report_type_by_report_sheet",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                reportTypeId = dbResult;
            }
            return reportTypeId;
        }

        public static int GetReportSheetIdByReportTypeId(int reportTypeId)
        {
            int reportSheetId = 0;
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                var parameters = new OracleDynamicParameters();
                parameters.Add("rt_id", value: reportTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<int>(sql: "pkg_portal_info.fnc_get_report_sheet_id_by_report_type_id",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                reportSheetId = dbResult;
            }
            return reportSheetId;
        }

        public static List<SelectListItem> GetReportSheets(int id)
        {
            var list = new List<SelectListItem>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rft_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<SelectListItem>(sql: "pkg_portal_info.fnc_get_report_sheets",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetReportColumns(int id)
        {
            var list = new List<SelectListItem>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("sheet_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<SelectListItem>(sql: "pkg_portal_info.fnc_get_report_columns",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<SelectListItem> GetReportRows(int id)
        {
            var list = new List<SelectListItem>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("sheet_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<SelectListItem>(sql: "pkg_portal_info.fnc_get_report_rows",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static string GetReportColumnNamesByIds(string columnIds)
        {
            string columnText = "";
            if (!string.IsNullOrEmpty(columnIds))
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("column_ids", value: columnIds, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_report_column_names_by_ids",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                    foreach (var column in dbResult)
                    {
                        columnText += "\'" + column.COL_CODE + "\',";
                    }
                }
                columnText = columnText.Remove(columnText.LastIndexOf(','));
            }
            return columnText;
        }

        public static string GetReportColumns(OracleConnection conn, string columnNames, int sheetId)
        {
            string columnIds = "";
            var colArray = columnNames.Trim('[').Trim(']').Split(',');
            columnNames = "";
            foreach (var column in colArray)
            {
                columnNames += column + ",";
            }
            columnNames = columnNames.Remove(columnNames.LastIndexOf(','));
            var parameters = new OracleDynamicParameters();
            parameters.Add("columnNames", value: columnNames, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
            parameters.Add("sheet_id", value: sheetId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
            parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
            var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_report_columns_by_sheet",
                   param: parameters,
                   commandType: CommandType.StoredProcedure
                   ).AsList();
            foreach (var id in dbResult)
            {
                columnIds += id.ColumnIds + ",";
            }
            columnIds = columnIds.Remove(columnIds.LastIndexOf(','));
            return columnIds;
        }

        public static SelectedReportData GetDataBySelectedReport(string reportFormula)
        {
            List<SelectListItem> reportColumns = null;
            List<SelectListItem> reportRows = null;
            SelectedReportData reportData = null;
            var columnIds = "";
            var rowTexts = "";
            var sheetAndCell = reportFormula.Split('$');
            var colAndRow = sheetAndCell[1];
            var columns = colAndRow.Contains('[') ? colAndRow.Substring(colAndRow.IndexOf('[') + 1, (colAndRow.IndexOf(']') - colAndRow.IndexOf('[')) - 1) : "";
            string[] splittedColumns = (!string.IsNullOrEmpty(columns) && columns.Contains(',')) ? columns.Split(',') : (new string[] { columns });
            string rows = colAndRow[colAndRow.IndexOf("!") + 1] == '(' ? "" : colAndRow.Substring(colAndRow.IndexOf('!') + 1, colAndRow.IndexOf('(') - (colAndRow.IndexOf('!') + 1));
            string[] splittedRows = (!string.IsNullOrEmpty(rows) && rows.Contains(',')) ? rows.Split(',') : (new string[] { rows });
            string previousMonth = colAndRow.Substring(colAndRow.IndexOf('(') + 1, colAndRow.IndexOf(')') - (colAndRow.IndexOf('(') + 1));
            int sheetId = Convert.ToInt32(reportFormula.Substring(reportFormula.IndexOf('{') + 1, reportFormula.IndexOf('}') - (reportFormula.IndexOf('{') + 1)));
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                var parameters = new OracleDynamicParameters();
                parameters.Add("sheet_id", value: sheetId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<SelectedReportData>(sql: "pkg_portal_info.fnc_get_data_by_selected_report",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                reportData = new SelectedReportData()
                {
                    XlsSheetId = sheetId,
                    XlsSheetName = dbResult.XlsSheetName,
                    ReportTypeId = dbResult.ReportTypeId,
                    ReportFileTypeId = dbResult.ReportFileTypeId,
                    ReportFileTypeName = dbResult.ReportFileTypeName,
                    PreviousMonth = string.IsNullOrEmpty(previousMonth) ? 0 : Convert.ToInt32(previousMonth),
                    XlsColumns = columns,
                    XlsRows = rows
                };
            }
            if (splittedColumns.Length > 0)
            {
                foreach (var id in splittedColumns)
                {
                    columnIds += id + ",";
                }
                columnIds = columnIds.Remove(columnIds.LastIndexOf(","));
            }
            if (!string.IsNullOrEmpty(columns))
            {
                reportColumns = GetReportColumns(reportData.XlsSheetId);
                foreach (var column in reportColumns)
                {
                    for (int i = 0; i < splittedColumns.Length; i++)
                    {
                        if (column.Value == splittedColumns[i])
                        {
                            columnIds += column.Value + ",";
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(rows))
            {
                reportRows = GetReportRows(reportData.XlsSheetId);
                foreach (var row in reportRows)
                {
                    for (int i = 0; i < splittedRows.Length; i++)
                    {
                        if (row.Text == splittedRows[i])
                        {
                            rowTexts += row.Text + ",";
                        }
                    }
                }
                rowTexts = rowTexts.Remove(rowTexts.LastIndexOf(","));
            }
            reportData.XlsColumnIds = columnIds;
            reportData.XlsRows = rowTexts;
            return reportData;
        }

        public static int AddOrUpdateXlRuleAndFormula(int? id, XlRulesFormula formula)
        {
            string[] splitOperators = SplitXlRuleOperators();
            List<int> reportFileTypeIds = new List<int>();
            XlRule rule = new XlRule()
            {
                CheckValue = null,
                ErrorMessageSuffix = "] yoxlamasında səhv var! Yoxlama : (",
                Formula = formula.FormulaCode,
                RoundSize = formula.RoundSize,
                State = formula.Status == 1 ? "Y" : "N",
                Status = formula.Status == 1 ? "A" : "P"
            };
            string[] cellAddress = formula.FormulaCode.Split(splitOperators, StringSplitOptions.None);
            int result = 1;
            formula.groupName = formula.groupName == "0" ? "" : formula.groupName;
            OracleTransaction transaction = null;
            try
            {
                using (var conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    try
                    {
                        if (id == null)
                        {
                            rule.Id = GetXlRuleMaxId(conn);
                            int ruleRow = AddXlRule(conn, rule, formula);
                            if (ruleRow == 0) return ruleRow;
                        }
                        else
                        {
                            rule.Id = (int)id;
                            DeleteXlFormula(conn, rule.Id);
                            DeleteReportTypeRule(conn, rule.Id);
                        }
                        cellAddress = cellAddress.Distinct().ToArray();
                        for (int i = 0; i < cellAddress.Length; i++)
                        {
                            if (!cellAddress[i].Contains('(') && cellAddress[i].Contains(')'))
                            {
                                cellAddress[i] = cellAddress[i].Remove(cellAddress[i].LastIndexOf(')'));
                            }
                            double number = 0;
                            bool isNumeric = false;
                            if (cellAddress[i].Contains('%'))
                            {
                                isNumeric = double.TryParse(cellAddress[i].Substring(cellAddress[i].IndexOf('%') + 1, cellAddress[i].LastIndexOf('%') - (cellAddress[i].IndexOf('%') + 1)), out number);
                            }
                            string[] sheetAndCell = null;
                            string[] colAndRow = null;
                            int previousMonth = 0;
                            //string cellText = " ";
                            string whereString = "";
                            int xlsSheetId = 0;
                            if (!isNumeric)
                            {
                                rule.IsMultiRowResult = 0;
                                sheetAndCell = cellAddress[i].TrimEnd(' ').Split('$');
                                xlsSheetId = Convert.ToInt32(sheetAndCell[0].Substring(sheetAndCell[0].IndexOf('{') + 1, sheetAndCell[0].IndexOf('}') - (sheetAndCell[0].IndexOf('{') + 1)));
                                formula.ReportTypeId = GetReportTypeByReportSheet(xlsSheetId);
                                formula.ReportFileTypeId = GetReportFileTypeId(null, formula.ReportTypeId.ToString());
                                reportFileTypeIds.Add(formula.ReportFileTypeId);
                                colAndRow = sheetAndCell[1].Split('!');
                                previousMonth = Convert.ToInt32(sheetAndCell[1].Substring(sheetAndCell[1].LastIndexOf('(') + 1, sheetAndCell[1].IndexOf(')') - (sheetAndCell[1].LastIndexOf('(') + 1)));
                                var columns = colAndRow[0].Replace("[", "").Replace("]", "");
                                var rows = colAndRow[1].Remove(colAndRow[1].LastIndexOf('('));
                                var rowText = string.IsNullOrEmpty(rows) ? "" : "\'" + rows + "\'";
                                if (rows.Contains(','))
                                {
                                    rowText = "";
                                    var rowArray = rows.Split(',');
                                    foreach (var row in rowArray)
                                    {
                                        rowText += "\'" + row + "\',";
                                    }
                                    rowText = rowText.Remove(rowText.LastIndexOf(','));
                                }
                                string columnText = !string.IsNullOrEmpty(columns) ? GetReportColumnNamesByIds(columns) : "";
                                if (!string.IsNullOrEmpty(columnText) && !string.IsNullOrEmpty(rowText))
                                {
                                    whereString = "col_code in (" + columnText + ") and row_code ";
                                    if (colAndRow[1].ToLower().Contains("between"))
                                    {
                                        whereString += colAndRow[1].Remove(colAndRow[1].LastIndexOf('(')).ToLower();
                                    }
                                    else
                                    {
                                        whereString += "in(" + rowText + ")";
                                    }
                                }
                                else if (string.IsNullOrEmpty(columnText) && !string.IsNullOrEmpty(rowText))
                                {
                                    whereString = "row_code ";
                                    if (colAndRow[1].ToLower().Contains("between"))
                                    {
                                        whereString += colAndRow[1].Remove(colAndRow[1].LastIndexOf('(')).ToLower(); ;
                                    }
                                    else
                                    {
                                        whereString += "in(" + rowText + ")";
                                    }
                                }
                                else if (!string.IsNullOrEmpty(columnText) && string.IsNullOrEmpty(rowText))
                                {
                                    whereString = "col_code in (" + columnText + ")";
                                }
                                else if (string.IsNullOrEmpty(columnText) && string.IsNullOrEmpty(rowText))
                                {
                                    whereString = " ";
                                }

                                formula.WhereString = whereString;
                                formula.FieldFunction = "sum(value)";
                                if (cellAddress[i].Contains("MAX") || cellAddress[i].Contains("MIN") || cellAddress[i].Contains("ABS"))
                                {
                                    formula.FieldFunction = cellAddress[i].Trim(' ').Substring(0, 3) + "(value)";
                                }
                                formula.PreviousMonth = previousMonth;
                                var formulaRow = AddXlFormula(conn, rule, formula);
                                UpdateFormulaCodeSheetNames(formula, cellAddress, conn, i);
                                AddReportFileTypeRule(formula, rule, conn);
                            }
                            else
                            {
                                if (cellAddress[i].Contains("~(") && cellAddress[i].Contains("~)"))
                                {
                                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i].ToString().Trim(), "(" + number.ToString() + ")");
                                }
                                else if (!cellAddress[i].Contains("~(") && cellAddress[i].Contains("~)"))
                                {
                                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i].ToString().Trim(), number.ToString() + ")");
                                }
                                else if (cellAddress[i].Contains("~(") && !cellAddress[i].Contains("~)"))
                                {
                                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i].ToString().Trim(), "(" + number.ToString());
                                }
                                else
                                {
                                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i].ToString().Trim(), number.ToString());
                                }
                            }
                        }
                        rule.IsMixed = reportFileTypeIds.Distinct().Count();
                        formula.FormulaCode = String.Concat(formula.FormulaCode.Where(c => !Char.IsWhiteSpace(c)));
                        formula.FormulaCode = formula.FormulaCode.Replace("OR", " OR ").Replace("AND", " AND ");
                        if (formula.groupName.Contains("code"))
                        {
                            rule.IsMultiRowResult = 1;
                        }
                        UpdateXlRule(formula, rule, conn);
                        UpdateReportFileTypeRuleIsmixed(rule, conn);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex);
                        transaction.Rollback();
                        return 0;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return result;
            }
        }

        private static void AddReportFileTypeRule(XlRulesFormula formula, XlRule rule, OracleConnection conn)
        {
            try
            {
                if (!IsExistReportTypeRule(conn, rule.Id, formula.ReportFileTypeId))
                {
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("rft_id", value: formula.ReportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("xr_id", value: rule.Id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("is_mixed", value: rule.IsMixed, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    conn.Execute("pkg_portal_info.prc_add_report_file_type_rule", parameters, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static bool IsExistReportTypeRule(OracleConnection conn, int xlRuleId, int reportFileTypeId)
        {
            bool result = false;
            try
            {
                var parameters = new OracleDynamicParameters();
                parameters.Add("xr_id", value: xlRuleId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rft_id", value: reportFileTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<int>(sql: "pkg_portal_info.fnc_is_exist_report_type_rule",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                result = dbResult > 0 ? true : false;
                return result;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static void UpdateXlRule(XlRulesFormula formula, XlRule rule, OracleConnection conn)
        {
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("c_value", value: formula.FormulaCode, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("is_mr", value: rule.IsMultiRowResult, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("is_mx", value: rule.IsMixed, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("frml", value: formula.FormulaText == null ? rule.Formula : formula.FormulaText, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("sts", value: rule.Status, dbType: OracleDbType.Char, direction: ParameterDirection.Input);
                parameters.Add("stt", value: rule.State, dbType: OracleDbType.Char, direction: ParameterDirection.Input);
                parameters.Add("r_size", value: rule.RoundSize, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rule_id", value: rule.Id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                conn.Execute("pkg_portal_info.prc_update_xl_rule", parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static void UpdateReportFileTypeRuleIsmixed(XlRule rule, OracleConnection conn)
        {
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("is_mixed", value: rule.IsMixed, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("xr_id", value: rule.Id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                conn.Execute("pkg_portal_info.prc_update_report_file_type_rule_is_mixed", parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static void UpdateFormulaCodeSheetNames(XlRulesFormula formula, string[] cellAddress, OracleConnection conn, int i)
        {
            int formulaId = 0;
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                formulaId = conn.Query<int>(sql: "pkg_portal_info.fnc_xl_formula_max_id",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                if (cellAddress[i].Contains("~(") && cellAddress[i].Contains("~)"))
                {
                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i], "(ID" + formulaId + ")").Replace("<>", "!=").Replace("|", " ");
                }
                else if (cellAddress[i].Contains("~(") && !cellAddress[i].Contains("~)"))
                {
                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i], "(ID" + formulaId).Replace("<>", "!=").Replace("|", " ");
                }
                else if (!cellAddress[i].Contains("~(") && cellAddress[i].Contains("~)"))
                {
                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i], "ID" + formulaId + ")").Replace("<>", "!=").Replace("|", " ");
                }
                else
                {
                    formula.FormulaCode = formula.FormulaCode.Replace(cellAddress[i], "ID" + formulaId).Replace("<>", "!=").Replace("|", " ");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static int GetXlRuleMaxId(OracleConnection conn)
        {
            int id = 0;
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                id = conn.Query<int>(sql: "pkg_portal_info.fnc_xl_rule_max_id",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                return id + 1;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static int AddXlRule(OracleConnection conn, XlRule rule, XlRulesFormula formula)
        {
            int ruleRow = 0;
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("id", value: rule.Id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("formula", value: formula.FormulaText == null ? rule.Formula : formula.FormulaText, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("chk_value", value: rule.CheckValue, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("status", value: rule.Status, dbType: OracleDbType.Char, direction: ParameterDirection.Input);
                parameters.Add("state", value: rule.State, dbType: OracleDbType.Char, direction: ParameterDirection.Input);
                parameters.Add("ismixed", value: rule.IsMixed, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("multi_row", value: rule.IsMultiRowResult, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("round_size", value: rule.RoundSize, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("err_mes", value: rule.ErrorMessageSuffix, dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Input);
                parameters.Add("hide_data", value: rule.HideDataValue, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                ruleRow = conn.Execute("pkg_portal_info.prc_add_xl_rule", parameters, commandType: CommandType.StoredProcedure);
                return ruleRow;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return ruleRow;
            }

        }

        private static int AddXlFormula(OracleConnection conn, XlRule rule, XlRulesFormula formula)
        {
            int formulaRow = 0;
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("rule_id", value: rule.Id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rt_id", value: formula.ReportTypeId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("f_func", value: formula.FieldFunction, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("where_string", value: formula.WhereString, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("prev_month", value: formula.PreviousMonth, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("group_name", value: formula.groupName, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                formulaRow = conn.Execute("pkg_portal_info.prc_add_xl_formula", parameters, commandType: CommandType.StoredProcedure);
                return formulaRow;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return formulaRow;
            }
        }

        public static XlRulesFormula GetXlFormula(XlRule rule)
        {
            var formula = new XlRulesFormula();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    try
                    {
                        rule.CheckValue = GetCheckValue(conn, rule.Id).Replace("AND", "|AND|");
                        string[] splitOperators = SplitXlRuleOperators();
                        string[] xlFormulaIds = rule.CheckValue.Split(splitOperators, StringSplitOptions.None);
                        FillFormulaForView(formula, rule, xlFormulaIds);

                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                return formula;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static string[] SplitXlRuleOperators()
        {
            string[] splitOperators = new string[Constants.XlRuleOperators.Count];
            try
            {
                for (int i = 0; i < splitOperators.Length; i++)
                {
                    if (Constants.XlRuleOperators[i].Contains("(") || Constants.XlRuleOperators[i].Contains(")") || Constants.XlRuleOperators[i].Contains("MAX") || Constants.XlRuleOperators[i].Contains("MIN") || Constants.XlRuleOperators[i].Contains("ABS"))
                    {
                        continue;
                    }
                    if (Constants.XlRuleOperators[i].Trim() == "AND")
                    {
                        splitOperators[i] = "|AND|";
                        continue;
                    }
                    splitOperators[i] = Constants.XlRuleOperators[i];
                }
                splitOperators = splitOperators.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                return splitOperators;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static string GetCheckValue(OracleConnection conn, int xlRuleId)
        {
            string checkValue = "";
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("xr_id", value: xlRuleId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                checkValue = conn.Query<string>(sql: "pkg_portal_info.fnc_get_checkvalue",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                return checkValue;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static void GetXlRuleAndXlFormulaDataByXlRuleId(XlRulesFormula formula, XlRule rule)
        {
            formula.XlsSheetId = new List<int>();
            rule.PreviousMonthList = new List<int>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("xr_id", value: rule.Id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_xl_rule_and_xl_formula_data",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();
                    foreach (var item in dbResult)
                    {
                        formula.XlsSheetId.Add(GetReportSheetIdByReportTypeId((int)item.ReportTypeId));
                        rule.PreviousMonthList.Add((int)item.IsPrevMonth);
                        formula.groupName = item.GroupName;
                        rule.Formula = item.Formula;
                        formula.Status = item.Status == "A" ? 1 : 0;
                        rule.IsMixed = (int)item.IsMixed;
                        rule.IsMultiRowResult = item.IsMultiRow;
                        rule.RoundSize = item.RoundSize != null ? item.RoundSize.ToString() : "";
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static void FillFormulaForView(XlRulesFormula formula, XlRule rule, string[] xlFormulaIds)
        {
            List<string> values = new List<string>();
            List<string> hidden_values = new List<string>();
            Dictionary<string, string> splittedFormula = new Dictionary<string, string>();
            Dictionary<string, string> splittedHidenFormula = new Dictionary<string, string>();
            var splitOperators = SplitXlRuleOperators();
            var operators = rule.CheckValue.Split(xlFormulaIds, StringSplitOptions.None).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            foreach (var item in operators)
            {
                rule.CheckValue = rule.CheckValue.Replace(item.Trim(), " " + item.Trim() + " ");
            }
            string checkValueCode = rule.CheckValue;
            rule.CheckValue = string.Join(" ", rule.CheckValue.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            checkValueCode = string.Join(" ", checkValueCode.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            string ids = "";
            string whereString = "";
            int valueCounter = 0;
            int operatorCounter = 0;
            var temp_valueStartIndex = 0;
            var temp_valueEndIndex = 0;
            var temp_hidden_valueStartIndex = 0;
            var temp_hidden_valueEndIndex = 0;
            var operatorStartIndex = 0;
            var operatorEndIndex = 0;
            var hidden_operatorStartIndex = 0;
            var hidden_operatorEndIndex = 0;
            var addedXlFormulaIds = new List<string>();
            foreach (var id in xlFormulaIds)
            {

                if (id.Contains("ID"))
                {
                    var numberFromString = Int32.Parse(Regex.Match(id, @"\d+").Value);
                    ids += numberFromString + ",";
                }
                else
                {
                    checkValueCode = checkValueCode.Replace(id, "%" + id + "%");
                }
            }
            if (ids.LastIndexOf(',') == ids.Length - 1)
            {
                ids = ids.Remove(ids.LastIndexOf(','));
            }
            GetXlRuleAndXlFormulaDataByXlRuleId(formula, rule);
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();
                var parameters = new OracleDynamicParameters();
                parameters.Add("formulaIds", value: ids, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_fill_formula_for_view",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .ToList();
                while (xlFormulaIds.Length > valueCounter)
                {
                    string value = "";
                    string hidden_value = "";
                    string column = "";
                    string rowString = "";
                    double number;
                    var isNumeric = double.TryParse(xlFormulaIds[valueCounter].Trim(), out number);
                    if (!isNumeric)
                    {
                        if (!addedXlFormulaIds.Contains(xlFormulaIds[valueCounter]))
                        {
                            addedXlFormulaIds.Add(xlFormulaIds[valueCounter]);
                            xlFormulaIds[valueCounter] = xlFormulaIds[valueCounter].Trim().Remove(0, 2);
                            whereString = dbResult[valueCounter].WHERESTRING;
                            if (whereString.ToLower().Contains("col_code"))
                            {
                                column = whereString.Substring(whereString.IndexOf("col_code") + 13, whereString.IndexOf(')') - 13).Replace("\'", "");
                                column = "[" + column + "]";
                            }

                            if (whereString.ToLower().Contains("row_code"))
                            {
                                if (whereString.ToLower().Contains("between"))
                                {
                                    rowString = whereString.Substring(whereString.IndexOf("row_code") + 9, whereString.Length - (whereString.IndexOf("row_code") + 9));
                                }
                                else
                                {
                                    rowString = whereString.Substring(whereString.IndexOf("row_code") + 12, whereString.Length - (whereString.IndexOf("row_code") + 13));
                                }

                            }
                            value = dbResult[valueCounter].SHEET_NAME + "$" + column.Replace("\'", "") + rowString.Replace("\'", "") + "{" + rule.PreviousMonthList[valueCounter] + "}";
                            hidden_value = dbResult[valueCounter].SHEET_NAME.Replace("-", "_") + "{" + formula.XlsSheetId[valueCounter] + "}" + "$" + (!string.IsNullOrEmpty(column) ? ("[" + GetReportColumns(conn, column, (int)dbResult[valueCounter].ID) + "]") : "") + "!" + (string.IsNullOrEmpty(rowString) ? "" : rowString.Replace("\'", "")) + "(" + rule.PreviousMonthList[valueCounter] + ")";
                        }
                        else
                        {
                            var sameValueIndex = xlFormulaIds.ToList().IndexOf(xlFormulaIds[valueCounter].Trim().Remove(0, 2));
                            value = values[sameValueIndex];
                            hidden_value = hidden_values[sameValueIndex];
                        }
                    }
                    else
                    {
                        value = number.ToString().Trim();
                        hidden_value = "%" + number + "%";
                    }
                    values.Add(value);
                    hidden_values.Add(hidden_value);
                    var valueStartIndex = temp_valueEndIndex == 0 ? temp_valueEndIndex : temp_valueEndIndex + operators[operatorCounter].Trim().Length;
                    var valueEndIndex = valueStartIndex + (value.Length - 1);
                    var hidden_valueStartIndex = temp_hidden_valueEndIndex == 0 ? temp_hidden_valueEndIndex : temp_hidden_valueEndIndex + operators[operatorCounter].Trim().Length;
                    var hidden_valueEndIndex = hidden_valueStartIndex + (hidden_value.Length - 1);
                    if (valueStartIndex - temp_valueEndIndex > 0)
                    {
                        operatorStartIndex = temp_valueEndIndex + 2;
                        operatorEndIndex = operatorStartIndex + (operators[operatorCounter].Trim().Length - 1);
                        hidden_operatorStartIndex = temp_hidden_valueEndIndex + 2;
                        hidden_operatorEndIndex = hidden_operatorStartIndex + (operators[operatorCounter].Trim().Length - 1);
                        splittedFormula.Add("\"" + operatorStartIndex + ", " + operatorEndIndex + "\"", operatorCounter + "~" + rule.CheckValue.Substring(operatorStartIndex, operators[operatorCounter].Trim(' ').Length));
                        splittedHidenFormula.Add("\"" + hidden_operatorStartIndex + ", " + hidden_operatorEndIndex + "\"", operatorCounter + "~" + checkValueCode.Substring(hidden_operatorStartIndex, operators[operatorCounter].Trim(' ').Length));
                        valueStartIndex = operatorEndIndex + 2;
                        hidden_valueStartIndex = hidden_operatorEndIndex + 2;
                        valueEndIndex = valueStartIndex + (value.Length - 1);
                        hidden_valueEndIndex = hidden_valueStartIndex + (hidden_value.Length - 1);
                        operatorCounter++;
                    }
                    splittedFormula.Add("\"" + valueStartIndex + ", " + valueEndIndex + "\"", valueCounter + "~" + value.ToString());
                    splittedHidenFormula.Add("\"" + hidden_valueStartIndex + ", " + hidden_valueEndIndex + "\"", valueCounter + "~" + hidden_value.ToString());
                    temp_valueStartIndex = valueStartIndex;
                    temp_valueEndIndex = valueEndIndex;
                    temp_hidden_valueStartIndex = hidden_valueStartIndex;
                    temp_hidden_valueEndIndex = hidden_valueEndIndex;
                    rule.CheckValue = rule.CheckValue.Replace("ID" + xlFormulaIds[valueCounter], value).Replace("|AND|", "AND");
                    checkValueCode = checkValueCode.Replace("ID" + xlFormulaIds[valueCounter].Trim(), valueCounter + "~" + hidden_value.ToString());
                    valueCounter++;
                }
            }
            formula.FormulaCodeForView = rule.CheckValue;
            formula.FormulaCode = checkValueCode;
            formula.splittedFormulaCode = splittedHidenFormula;
            formula.splittedFormulaCodeForView = splittedFormula;
            formula.splitOperators = splitOperators.Select(x => x.Replace("|", "")).ToArray();
        }

        private static void DeleteXlFormula(OracleConnection conn, int id)
        {
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("xr_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                conn.Execute(sql: "pkg_portal_info.prc_delete_xl_formula",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       );
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        private static void DeleteReportTypeRule(OracleConnection conn, int id)
        {
            var parameters = new OracleDynamicParameters();
            try
            {
                parameters.Add("xr_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                conn.Execute(sql: "pkg_portal_info.prc_delete_report_type_rule",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       );
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static int GetBankUserMaxId(int bank_id)
        {
            int userId = 0;
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_bankid", value: bank_id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    var dbResult = conn.Query<dynamic>(sql: "pkg_portal_info.fnc_get_bank_user_max_id",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .First();
                    userId = (int)dbResult.ID;
                }
                return userId;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static List<ReportFile> GetReportTemplates(int userId)
        {
            var list = new List<ReportFile>();
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    var parameters = new OracleDynamicParameters();
                    parameters.Add("p_userid", value: userId, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                    parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                    list = conn.Query<ReportFile>(sql: "pkg_portal_info.fnc_get_report_tmplates",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           )
                           .AsList();

                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                throw;
            }
        }

        public static ReportFile GetReportTemplate(int id)
        {
            var rf = new ReportFile();
            rf.NotFound = false;
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();
                var parameters = new OracleDynamicParameters();
                parameters.Add("p_id", value: id, dbType: OracleDbType.Int32, direction: ParameterDirection.Input);
                parameters.Add("rv", dbType: OracleDbType.RefCursor, direction: ParameterDirection.ReturnValue);
                var dbResult = conn.Query<ReportFile>(sql: "pkg_portal_info.fnc_get_report_tmplate",
                       param: parameters,
                       commandType: CommandType.StoredProcedure
                       )
                       .First();
                rf.ID = dbResult.ID;
                rf.UPLOAD_FILE_NAME = dbResult.UPLOAD_FILE_NAME;
                rf.Report_Template = dbResult.Report_Template;
                if (dbResult == null)
                {
                    rf.NotFound = true;
                }
            }
            return rf;
        }

        public static bool CheckUserName(string userName, out int userId, out int deparmentid)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();

                    var parameters = new OracleDynamicParameters();

                    parameters.Add("p_username", value: userName, dbType: OracleDbType.Varchar2, direction: ParameterDirection.Input);
                    parameters.Add("p_userid", value: null, dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
                    parameters.Add("p_departmentid", value: null, dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
                    parameters.Add("rv", dbType: OracleDbType.Boolean, direction: ParameterDirection.ReturnValue);

                    var affectedRows = conn.ExecuteScalar<int>(sql: "pkg_portal_home.fnc_check_username",
                           param: parameters,
                           commandType: CommandType.StoredProcedure
                           );

                    var returnVal = parameters.Get<OracleBoolean>("rv");
                    userId = parameters.Get<OracleDecimal>("p_userid").ToInt32();
                    deparmentid = parameters.Get<OracleDecimal>("p_departmentid").ToInt32();
                    return returnVal.Value;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                userId = -1;
                deparmentid = -1;
                return false;
            }
        }
    }
}
