﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models
{
    [Table("USERANSWER")]
    public class UserAnswer
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("USERID")]
        public int UserId { get; set; }

        [Column("FORMID")]
        public int FormId { get; set; }

        [Column("ANSWERDATE")]
        public DateTime AnswerDate { get; set; }

        public virtual Form Form { get; set; }
        public virtual List<UserAnswerDetail> UserAnswersDetail { get; set; } = new List<UserAnswerDetail>();
    }
}