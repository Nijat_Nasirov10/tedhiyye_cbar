﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tedhiyye_portal_shared.Domains.RealSector.Models.Reports
{
	[Table("VW_BII_FOR_COMPANYTYPE")]
	public class VwBiiForCompanyType
	{
		[Key]
		[Column("COMPANYTYPEID")]
		public int CompanyTypeId { get; set; }

		[Column("YEAR")]
		public int Year { get; set; }

		[Column("MONTH")]
		public int Month { get; set; }

		[Column("FIRSTDATEOFMONTH")]
		public DateTime FirstDateOfMonth { get; set; }

		[Column("TOTALPOINT")]
		public double TotalPoints { get; set; }
	}
}